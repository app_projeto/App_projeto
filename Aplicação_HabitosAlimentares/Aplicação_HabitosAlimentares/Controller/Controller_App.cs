﻿using Aplicação_HabitosAlimentares.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_HabitosAlimentares.Controller
{
    public class Controller_App
    {
        public Controller_App()
        {
            Program.V_Registar.Registo += V_Registar_Registo;
            Program.V_AdcCriança.RegistoCriança += V_AdcCriança_RegistoCriança;
            Program.V_Login.Login += V_Login_Login;
            Program.V_Menu.PreencherPerfilUtilizador += V_Menu_PreencherPerfilUtilizador;
            Program.V_AltCriança.Preencheracriança += V_Pesquisar_PreencherPerfilCrianca;
            Program.V_AlterarPerfil.alterarperfil += V_AlterarPerfil_AlterarPerfil;

        }

        private void V_AlterarPerfil_AlterarPerfil(string nome, string password, string imagem, string email, int idade, int telemovel)
        {
            Program.M_App.AlterarPerfil(nome, password, imagem, email, idade, telemovel);
        }
        private void V_Pesquisar_PreencherPerfilCrianca(string nomecrianca_)
        {
            Program.M_App.PreencheraCrianca(nomecrianca_);
        }

        private void V_AdcCriança_RegistoCriança(string nome_, string imagem_, float altura_, int idade_, int peso_, string atividade_)
        {
            Program.M_App.Registar_criança(nome_, imagem_, altura_, idade_, peso_, atividade_);
        }

        private void V_Menu_PreencherPerfilUtilizador()
        {
            Program.M_App.PerfilUtilizador();
        }
        
        private void V_Login_Login(string email, string password)
        {
            Program.M_App.LoginControlo(email, password);
        }

        private void V_Registar_Registo(string nome, string password, string imagem, string email, int idade, int telemovel)
        {
            Program.M_App.Registar_utilizador (nome, password,imagem, email, idade, telemovel);         
        }
    }

}