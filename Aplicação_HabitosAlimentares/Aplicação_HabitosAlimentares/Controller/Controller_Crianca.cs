﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Aplicação_HabitosAlimentares.Controller
{
    public class Controller_Crianca
    {
        public DataTable Listar()
        {
            try
            {
                Crianca_Model crianca1 = new Crianca_Model();
                DataTable dt = new DataTable();

                dt = crianca1.Listar();

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable Pesquisar(Crianca crianca)
        {
            try
            {
                Crianca_Model crianca1 = new Crianca_Model();
                DataTable dt = new DataTable();

                dt = crianca1.Pesquisar(crianca);

                return dt;
            }
            catch (Exception){

                throw;
            }
        }

    }
}
