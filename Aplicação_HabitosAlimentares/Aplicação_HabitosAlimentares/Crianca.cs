﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_HabitosAlimentares
{
    public class Crianca
    {
        public string nome { get; set; }
        public string foto { get; set; }
        public float altura { get; set; }
        public int idade { get; set; }
        public float peso { get; set; }
        public bool atividade { get; set; }
        public string email_pai { get; set; }
        public float pesoideal { get; set; }
        public Crianca(string nome_, string foto_, float altura_, int idade_, float peso_, bool atividade_, string emailp)
        {
            nome = nome_;
            foto = foto_;
            altura = altura_;
            idade = idade_;
            peso = peso_;
            atividade = atividade_;
            email_pai = emailp;
            pesoideal = 0;
        }
        public Crianca()
        {

        }
        public float CalculaPesoIdeal(float altura_, int idade_)
        {
            float pesoideal = 0;
            if (idade_ <= 5)
            {
                pesoideal = 20 * (altura_ / 100);
            }
            if (idade_>5 && idade_<=15)
            {
                pesoideal = 22 * (altura_ / 100);
            }
            if (idade_ > 15)
            {
                pesoideal = 23 * (altura_ / 100);
            }
            return pesoideal;
        }
        public /*Crianca*/ void CalculaPesoIdeal(Crianca c)
        {
            if (c.idade <= 5)
            {
                c.pesoideal = 20 * (c.altura/ 100);
            }
            if (c.idade > 5 && c.idade <= 15)
            {
                c.pesoideal = 22 * (c.altura / 100);
            }
            if (c.idade > 15)
            {
                c.pesoideal = 23 * (c.altura / 100);
            }
            //return c;
        }

    }
}
    