﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_HabitosAlimentares
{
        //public delegate float MetodosComFloats(float altura_, int idade_);
        public delegate void MetodosComDuasStrings(string email, string password);
        public delegate void MetodosSemParametros();
        public delegate void MetodoComString(string nomecrianca);
        public delegate void MetodosComIntStrings(string nome,string password,string imagem, string email,  int idade,  int telemovel);
        public delegate void MetodosComTresIntTresStrings(string nome_, string imagem_, float altura_, int idade_, int peso_, string atividade_);
    //public delegate void MetodosComSeisStrings(string a, string b, string c, string d, string e, string f);
    //public delegate void MetodosComDama(string x, string y, string cor, bool dama);
    //public delegate void MetodosComDoisIntUmaString(int x, int y, string d);
    //public delegate void MetodosComUmaString(string user);
    //public delegate void MetodosComTresInts(int a, int b, int c);
    //public delegate void MetodosComSeisInts(int a, int b, int c, int d, int e, int f);

}
