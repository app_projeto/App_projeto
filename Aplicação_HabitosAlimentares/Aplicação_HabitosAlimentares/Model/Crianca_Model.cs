﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Aplicação_HabitosAlimentares.Model;

namespace Aplicação_HabitosAlimentares
{
    public class Crianca_Model
    {
        SqlConnection conexao = null;
        SqlCommand comando;

        //Listar Dados da DB
        public DataTable Listar()
        {

            var folder1 = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
            string conecta = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder1 + "Utilizadores.mdf" +
                ";Trusted_Connection=True;";

            try
            {
                conexao = new SqlConnection(conecta);
                string cmdText = string.Format("SELECT Nome FROM Criança Where Id_P = '{0}'", /*conexao*/Program.M_App.ID_UU.ToString());
                comando = new SqlCommand(/*"SELECT Nome FROM Criança Where ID = '{0}'", /*conexao*/cmdText,conexao);

                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();

                da.SelectCommand = comando;

                da.Fill(dt);

                return dt;
            }
            catch(Exception)
            {

                throw;
            }
        }

        public DataTable Pesquisar(Crianca crianca)
        {
            var folder1 = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
            string conecta = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder1 + "Utilizadores.mdf" +
                ";Trusted_Connection=True;";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter();

                DataTable dt = new DataTable();

                conexao = new SqlConnection(conecta);
                

                comando = new SqlCommand("SELECT * FROM Criança WHERE ID_P = '{0}'", conexao);

                comando.Parameters.AddWithValue("@nome", crianca.nome);

                da.SelectCommand = comando;
                da.Fill(dt);

                return dt;
            }
            catch(Exception)
            {
                throw;
            }

        }
    }
}
