﻿using Aplicação_HabitosAlimentares.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_HabitosAlimentares.Model
{
    class Model_App
    {
        public event MetodosSemParametros RegistoSucedido;
        public event MetodosSemParametros RegistoFalhado;
        public event MetodosSemParametros RegistoSucedidoCriança;
        public event MetodosSemParametros RegistoFalhadoCriança;
        public event MetodosSemParametros RegistoMesmoEmail;
        public event MetodosSemParametros RegistoMesmaCriança;
        public event MetodosSemParametros PerfilAlteradoComSucesso;
        public event MetodosSemParametros PerfilAlteradoFalhado;
        public event MetodosSemParametros LoginSucedido;
        public event MetodosSemParametros LoginFalhado;
        public event MetodosSemParametros Preencher_Utilizador;
        public event MetodosSemParametros MesmoLogin;
        public event MetodoComString Preencher_Crianca;
       
        public string password_ { get; private set; }
        public string password_desincriptada { get; private set; }
        public Utilizador Pais { get; private set; }
        public int vez { get; private set; }
        public string comando2 { get; private set; }

        public string comando3 { get; private set; }
        public Perfil perfil { get; set; }
        public int ID_UU { get; set; }
        public Crianca Menor { get; set; }

        //public Model_App()
        //{
        //    perfil = new Perfil();

        //}

        public Model_App()
        {
            Pais = new Utilizador();
            Menor = new Crianca();
        }

        public void PerfilUtilizador()
        {
            if (Preencher_Utilizador != null)
                Preencher_Utilizador();
        }
        public void PerfilCrianca()
        {
            //if (Preencher_Crianca != null) ;
                //Preencher_aCrianca();
        }

        public void PreencheraCrianca(string nomecrianca)
        {
            var folder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
            string sqlConnection = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder + "Utilizadores.mdf" +
                ";Trusted_Connection=True;";
            SqlConnection server = new SqlConnection(sqlConnection);
            server.Open();
            string cmdText = string.Format("SELECT * FROM Criança WHERE Nome  = '{0}'", nomecrianca);
            SqlCommand command = new SqlCommand(cmdText, server);
            SqlDataReader dadosc = command.ExecuteReader();
            if (dadosc.HasRows)
            {
                dadosc.Read();
                Menor.nome = nomecrianca;
                Menor.idade = Int32.Parse(dadosc["Idade"].ToString());
                Menor.foto = dadosc["Foto"].ToString();
                Menor.altura = float.Parse(dadosc["Altura"].ToString());
                Menor.peso = float.Parse(dadosc["Peso"].ToString());
                //Preencher_aCrianca();
            }
            server.Close();

        }
        public void LoginControlo(string email, string password)
        {
            if ( email == "al66518@utad.com")
            {
                if (LoginFalhado != null) LoginFalhado();
            }
            else
            {
                var folder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
                string sqlConnection = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder + "Utilizadores.mdf" +
                    ";Trusted_Connection=True;";
                SqlConnection server = new SqlConnection(sqlConnection);
                server.Open();
                string cmdText = string.Format("SELECT * FROM Utilizador WHERE email = '{0}'", email);
                SqlCommand command = new SqlCommand(cmdText, server);
                SqlDataReader dados = command.ExecuteReader();
                if (dados.HasRows)
                {
                    dados.Read();
                    string password1 = dados["Senha"].ToString();
                    Desincriptar(password1);
                    if (password_desincriptada == password)
                    {
                        if (vez == 0)
                        {
                            Pais.Email = dados["Email"].ToString();
                            Pais.Localizacao_imagem = dados["Foto"].ToString();
                            Pais.Nome= dados["Nome"].ToString();
                            Pais.Idade = Int32.Parse(dados["Idade"].ToString());
                            Pais.telemovel= Int32.Parse(dados["Telefone"].ToString());
                           

                            if (LoginSucedido != null)
                                ID_UU = Int32.Parse(dados["Id"].ToString());
                                 Preencher_Utilizador();
                                LoginSucedido();
                        }
                        else
                        {
                            if (Pais.Email == dados["Username"].ToString())
                            {
                                if (MesmoLogin != null) 
                                    MesmoLogin();
                            }
                            else
                            {
                                Pais.Email = dados["Email"].ToString();
                                Pais.Localizacao_imagem = dados["Foto"].ToString();
                                Preencher_Utilizador();
                                if (LoginSucedido != null) 
                                    LoginSucedido();
                            }
                        }
                        vez = 1;
                    }
                    else
                    {
                        if (LoginFalhado != null) LoginFalhado();
                    }
                }
                else
                {
                    if (LoginFalhado != null) LoginFalhado();
                }
                server.Close();
            }
        }


        public void Registar_utilizador(string nome, string password, string imagem, string email, int idade, int telemovel)
        {
            if (nome != "" && email != "" && password != "" && idade != 0 && imagem != "")
            {
                var folder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");

                string sqlConnection = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder + "Utilizadores.mdf" +
                                         ";Trusted_Connection=True;";

                SqlConnection server = new SqlConnection(sqlConnection);
                server.Open();
                string filename = System.IO.Path.GetFileName(imagem);
                filename = DateTime.Now.ToString("yyyy_MM_dd_HH_mm-") + filename;
                string comando2 = String.Format("SELECT * FROM Utilizador WHERE email = '{0}'", email);
                SqlCommand command2 = new SqlCommand(comando2, server);
                SqlDataReader dados = command2.ExecuteReader();
                if (!dados.HasRows)
                {
                    dados.Close();
                    Encriptar(password);
                    string comando = String.Format("INSERT INTO Utilizador (Nome, senha, Foto, Email, Idade, telefone) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}' )", nome, password_, filename, email, idade, telemovel);
                    SqlCommand command = new SqlCommand(comando, server);
                    int resultado = command.ExecuteNonQuery();

                    if (resultado == 1)
                    {
                        System.IO.File.Copy(imagem, "Imagens\\" + filename, true);
                        if (RegistoSucedido != null)
                        {
                            RegistoSucedido();
                        }
                    }
                }
                else
                {
                    if (RegistoMesmoEmail != null)
                    {
                        RegistoMesmoEmail();
                    }
                }
                server.Close();
            }
            else
            {
                if (RegistoFalhado != null)
                {
                    RegistoFalhado();
                }
            }
        }

        public void Registar_criança(string nome_, string imagem_, float altura_, int idade_, int peso_, string atividade_)
        {
            
            //bool ativ;
            if (nome_ != "" && imagem_ != "" && altura_ != 0 && idade_ != 0 && peso_ != 0 )
            {
                var folder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");

                string sqlConnection = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder + "Utilizadores.mdf" +
                                         ";Trusted_Connection=True;";

                SqlConnection server = new SqlConnection(sqlConnection);
                server.Open();
                string filename1 = System.IO.Path.GetFileName(imagem_);
                filename1 = DateTime.Now.ToString("yyyy_MM_dd_HH_mm-") + filename1;
                string comando3 = String.Format("SELECT * FROM Criança WHERE nome = '{0}'", nome_);
                SqlCommand command3 = new SqlCommand(comando3, server);
                SqlDataReader dados = command3.ExecuteReader();
                if (!dados.HasRows)
                {
                   
                    dados.Close();
                    string comando1 = String.Format("INSERT INTO Criança (Nome, Foto, Altura, Idade, Peso, Atividade, ID_P) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}','{6}')", nome_,filename1, altura_, idade_,peso_,atividade_,ID_UU);
                    SqlCommand command1 = new SqlCommand(comando1, server);
                    int resultado = command1.ExecuteNonQuery();
                  
                    if (resultado == 1)
                    {
                        System.IO.File.Copy(imagem_, "Imagens_Crianças\\" + filename1, true);
                        if (RegistoSucedidoCriança != null)
                        {
                            RegistoSucedidoCriança();
                        }
                    }
                }
                else
                {
                    if (RegistoMesmaCriança != null)
                    {
                        RegistoMesmaCriança();
                    }
                }
                server.Close();
            }
            else
            {
                if (RegistoFalhadoCriança != null)
                {
                    RegistoFalhadoCriança();
                }
            }
        }
        public void AlterarPerfil(string nome, string password, string imagem, string email, int idade, int telemovel)
        {
            if (nome != "" && email != "" && password != "" && idade != 0 && imagem != "")
            {
                var folder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");

                string sqlConnection = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder + "Utilizadores.mdf" +
                                         ";Trusted_Connection=True;";

                SqlConnection server = new SqlConnection(sqlConnection);
                server.Open();
                Encriptar(password);
                string filename = System.IO.Path.GetFileName(imagem);
                filename = DateTime.Now.ToString("yyyy_MM_dd_HH_mm-") + filename;
                string comando2 = String.Format("UPDATE Utilizador (Nome, senha, Foto, Idade, telefone) values ('{1}', '{2}', '{3}', '{4}', '{5}' ) WHERE email = '{0}'", email, nome,password, imagem, idade, telemovel);
                SqlCommand command = new SqlCommand(comando2, server);
                int resultado = command.ExecuteNonQuery();
                if (resultado == 1)
                {

                    if (PerfilAlteradoComSucesso != null) PerfilAlteradoComSucesso();
                }
                else
                {
                    if (PerfilAlteradoFalhado != null) PerfilAlteradoFalhado();
                }
            }
        }
        public void AlterarCrianca(string nome_, string imagem_, float altura_, int idade_, int peso_, string atividade_)
        {
            if (nome_ != "" && peso_ != 0 && altura_ != 0 && idade_ != 0 && imagem_ != "")
            {
                var folder = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");

                string sqlConnection = "Server=(localdb)\\MSSQLLocalDB;AttachDbFilename=" + folder + "Utilizadores.mdf" +
                                         ";Trusted_Connection=True;";

                SqlConnection server = new SqlConnection(sqlConnection);
                server.Open();
                string filename = System.IO.Path.GetFileName(imagem_);
                filename = DateTime.Now.ToString("yyyy_MM_dd_HH_mm-") + filename;
                string comando2 = String.Format("UPDATE Criança SET Nome = '" + nome_ + "', Altura = '" + altura_ + "', Foto = '" + imagem_ + "',Idade='" + idade_ + "',Peso='" + peso_ + "',Atividade='" + atividade_ + "' WHERE Nome = '{0}'", nome_);
                SqlCommand command = new SqlCommand(comando2, server);
                int resultado = command.ExecuteNonQuery();
                if (resultado == 1)
                {

                    if (PerfilAlteradoComSucesso != null) PerfilAlteradoComSucesso();
                }
                else
                {
                    if (PerfilAlteradoFalhado != null) PerfilAlteradoFalhado();
                }
            }
        }
        public void Encriptar(string chave)
        {
            string pass_encriptada = string.Empty;
            Byte[] encriptar = System.Text.Encoding.Unicode.GetBytes(chave);
            password_ = Convert.ToBase64String(encriptar);
        }

        public void Desincriptar(string chave)
        {
            string pass = string.Empty;
            Byte[] desincriptar = Convert.FromBase64String(chave);
            password_desincriptada = System.Text.Encoding.Unicode.GetString(desincriptar);
        }
        public string NomePais()
        {
            return Pais.Nome;
        }

        public string ImagemPais()
        {
            return Pais.Localizacao_imagem;
        }

        //public double CalcularPesoIdeal()
        //{
        //    double.TryParse(textBoxAltura.Text, out double altura);
        //    if (altura == null) return;

        //    double peso_ideal = 0;
        //    // double idade = 0;

        //    if (radioButtonMasculino.Checked)
        //    {
        //        if (textBoxIdade.Text != "")
        //        {
        //            double.TryParse(textBoxIdade.Text, out double idade);

        //            if (idade <= 2)
        //            {
        //                peso_ideal = 18.6 * (altura * altura);
        //            }
        //            if (idade > 2 && idade <= 4)
        //            {
        //                peso_ideal = 19.3 * (altura * altura);
        //            }
        //            if (idade > 4 && idade <= 6)
        //            {
        //                peso_ideal = 19.7 * (altura * altura);
        //            }
        //            if (idade > 6 && idade <= 8)
        //            {
        //                peso_ideal = 20.3 * (altura * altura);
        //            }
        //            if (idade > 8 && idade <= 10)
        //            {
        //                peso_ideal = 20.8 * (altura * altura);
        //            }
        //            if (idade > 10 && idade <= 12)
        //            {
        //                peso_ideal = 21.4 * (altura * altura);
        //            }
        //            if (idade > 12 && idade <= 14)
        //            {
        //                peso_ideal = 21.9 * (altura * altura);
        //            }
        //            if (idade > 14 && idade <= 16)
        //            {
        //                peso_ideal = 22.3 * (altura * altura);
        //            }
        //            if (idade > 16)
        //            {
        //                peso_ideal = 23 * (altura * altura);
        //            }
        //            //homem
        //            // peso_ideal = (72.7 * altura) - 58;
        //            // peso_ideal = 21.9 * (altura * altura) ;

        //        }
        //        else
        //        {
        //            double.TryParse(textBoxIdade.Text, out double idade);

        //            if (idade <= 2)
        //            {
        //                peso_ideal = 18.6 * (altura * altura);
        //            }
        //            if (idade > 2 && idade <= 4)
        //            {
        //                peso_ideal = 19.3 * (altura * altura);
        //            }
        //            if (idade > 4 && idade <= 6)
        //            {
        //                peso_ideal = 19.7 * (altura * altura);
        //            }
        //            if (idade > 6 && idade <= 8)
        //            {
        //                peso_ideal = 20.3 * (altura * altura);
        //            }
        //            if (idade > 8 && idade <= 10)
        //            {
        //                peso_ideal = 20.8 * (altura * altura);
        //            }
        //            if (idade > 10 && idade <= 12)
        //            {
        //                peso_ideal = 21.4 * (altura * altura);
        //            }
        //            if (idade > 12 && idade <= 14)
        //            {
        //                peso_ideal = 21.9 * (altura * altura);
        //            }
        //            if (idade > 14 && idade <= 16)
        //            {
        //                peso_ideal = 22.3 * (altura * altura);
        //            }
        //            if (idade > 16)
        //            {
        //                peso_ideal = 23 * (altura * altura);
        //            }

        //            //mulher
        //            // peso_ideal = (62.7 * altura) - 44.7;

        //        }

        //    }

        //    StringBuilder str = new StringBuilder("Peso Ideal: " + peso_ideal.ToString("0.00") + "Kg");

        //    //comparaçao com o peso atual

        //    if (textBoxPesoAtual.Text != "")
        //    {
        //        double.TryParse(textBoxPesoAtual.Text, out double peso_atual);
        //        if (peso_atual != null)
        //        {
        //            if (peso_ideal < peso_atual)
        //            {
        //                str.Append(Environment.NewLine + "Tem de emagracer " + (peso_atual - peso_ideal).ToString(" 0.00 ") + " Kg");

        //            }
        //            else if (peso_ideal > peso_atual)
        //            {
        //                str.Append(Environment.NewLine + "Esta " + (peso_ideal - peso_atual).ToString("0.00") + " Kg abaixo do peso ideal.");
        //            }
        //            else
        //            {
        //                str.Append(Environment.NewLine + "Esta com o peso ideal!");
        //            }
        //        }
        //    }

        //    MessageBox.Show(str.ToString());
        //}

    }
}
