﻿using Aplicação_HabitosAlimentares.Controller;
using Aplicação_HabitosAlimentares.Model;
using Aplicação_HabitosAlimentares.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares
{
    static class Program
    {
        public static View_Home V_Home { get; set; }
        public static View_Termos_Condiçoes V_Termos_Condiçoes { get; set; }

        public static View_Creditos V_Creditos { get;set; }

        public static View_Login V_Login { get;set; }

        public static View_Registar V_Registar { get; set; }

        public static View_MenuPrincipal V_Menu{ get; set; }

        public static View_AdicionarCriança V_AdcCriança { get;  set; }

        public static View_AlterarDadosCriança V_AltCriança { get; set; }

        public static View_ConsultarDadosCriança V_Consult_Criança { get; set; }

        public static View_CalcularPesoIdeal V_CalcularPesoIdeal { get;  set; }
        public static View_Calorias V_PlanoDeAlimentacao { get; set; }
        public static View_AlterarPerfil V_AlterarPerfil { get; set; }
        public static View_Exercicios V_Exercicio { get; set; }
        public static View_JogoPuzzle V_JogoPuzzle { get; set; }
        public static View_TabCal V_Tabela { get; set; }
       // public static View_Email11 V_Email1 { get; set; }
        public static View_EmailSuporte V_Email_suporte{ get; set; }
        public static View_Pesquisar V_Pesquisar { get; set; }
        public static View_HistoricoGraficos V_HistoricoGraficos { get; set; }
        public static View_Faq_s V_FaQ { get; set; }
        public static Model_App M_App { get; private set; }

        public static Controller_App C_App { get; private set; }


       


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Model
            M_App = new Model_App();

            //Views
            V_Home = new View_Home();
            V_Termos_Condiçoes = new View_Termos_Condiçoes();
            V_Creditos = new View_Creditos();
            V_Login = new View_Login();
            V_Registar = new View_Registar();
            V_Menu = new View_MenuPrincipal();
            V_AdcCriança = new View_AdicionarCriança();
            V_AltCriança = new View_AlterarDadosCriança();
            V_Consult_Criança = new View_ConsultarDadosCriança();
            V_CalcularPesoIdeal = new View_CalcularPesoIdeal();
            V_AlterarPerfil = new View_AlterarPerfil();
            V_PlanoDeAlimentacao = new View_Calorias();
            V_Tabela = new View_TabCal();
            V_Email_suporte = new View_EmailSuporte();
            V_Exercicio = new View_Exercicios();
            V_JogoPuzzle = new View_JogoPuzzle();
            V_Pesquisar = new View_Pesquisar();
            V_HistoricoGraficos = new View_HistoricoGraficos();
            V_FaQ = new View_Faq_s();

            //Controller
            C_App = new Controller_App();

            Application.Run(V_Home);
        }
    }
}
