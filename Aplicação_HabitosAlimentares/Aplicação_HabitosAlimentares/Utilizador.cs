﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_HabitosAlimentares
{
    public class Utilizador
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Password_encriptada { get; set; }
        public string Localizacao_imagem { get; set; }
        public int Idade { get; set; }
        public int telemovel { get; set; }
        public Utilizador() { }
       public Utilizador(string nome1, string email1, string pass,string im, int idad)
        {
            Nome = nome1;
            Email = email1;
            Password = pass;
            Localizacao_imagem = im;
            Idade = idad;
        }

    }
}
