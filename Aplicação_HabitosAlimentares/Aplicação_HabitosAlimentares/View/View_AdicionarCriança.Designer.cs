﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_AdicionarCriança
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_AdicionarCriança));
            this.textBoxNomeCriança = new System.Windows.Forms.TextBox();
            this.pictureBoxFoto_Criança = new System.Windows.Forms.PictureBox();
            this.textBoxAlturaCriança = new System.Windows.Forms.TextBox();
            this.textBoxIdadeCriança = new System.Windows.Forms.TextBox();
            this.textBoxPesoCriança = new System.Windows.Forms.TextBox();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.textBoxAtividade = new System.Windows.Forms.TextBox();
            this.labelCriar = new System.Windows.Forms.Label();
            this.radioButtonMasculino = new System.Windows.Forms.RadioButton();
            this.radioButtonFeminino = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFoto_Criança)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxNomeCriança
            // 
            this.textBoxNomeCriança.Location = new System.Drawing.Point(156, 130);
            this.textBoxNomeCriança.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxNomeCriança.Name = "textBoxNomeCriança";
            this.textBoxNomeCriança.Size = new System.Drawing.Size(170, 20);
            this.textBoxNomeCriança.TabIndex = 1;
            // 
            // pictureBoxFoto_Criança
            // 
            this.pictureBoxFoto_Criança.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxFoto_Criança.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxFoto_Criança.Location = new System.Drawing.Point(417, 104);
            this.pictureBoxFoto_Criança.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBoxFoto_Criança.Name = "pictureBoxFoto_Criança";
            this.pictureBoxFoto_Criança.Size = new System.Drawing.Size(180, 179);
            this.pictureBoxFoto_Criança.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFoto_Criança.TabIndex = 11;
            this.pictureBoxFoto_Criança.TabStop = false;
            this.pictureBoxFoto_Criança.Click += new System.EventHandler(this.pictureBoxFoto_Criança_Click);
            // 
            // textBoxAlturaCriança
            // 
            this.textBoxAlturaCriança.Location = new System.Drawing.Point(223, 184);
            this.textBoxAlturaCriança.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxAlturaCriança.Name = "textBoxAlturaCriança";
            this.textBoxAlturaCriança.Size = new System.Drawing.Size(103, 20);
            this.textBoxAlturaCriança.TabIndex = 12;
            this.textBoxAlturaCriança.TextChanged += new System.EventHandler(this.textBoxAlturaCriança_TextChanged);
            // 
            // textBoxIdadeCriança
            // 
            this.textBoxIdadeCriança.Location = new System.Drawing.Point(223, 154);
            this.textBoxIdadeCriança.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxIdadeCriança.Name = "textBoxIdadeCriança";
            this.textBoxIdadeCriança.Size = new System.Drawing.Size(103, 20);
            this.textBoxIdadeCriança.TabIndex = 16;
            // 
            // textBoxPesoCriança
            // 
            this.textBoxPesoCriança.Location = new System.Drawing.Point(223, 208);
            this.textBoxPesoCriança.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPesoCriança.Name = "textBoxPesoCriança";
            this.textBoxPesoCriança.Size = new System.Drawing.Size(103, 20);
            this.textBoxPesoCriança.TabIndex = 17;
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(564, 360);
            this.buttonVoltar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(92, 37);
            this.buttonVoltar.TabIndex = 24;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBoxAtividade
            // 
            this.textBoxAtividade.Location = new System.Drawing.Point(186, 272);
            this.textBoxAtividade.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxAtividade.Name = "textBoxAtividade";
            this.textBoxAtividade.Size = new System.Drawing.Size(140, 20);
            this.textBoxAtividade.TabIndex = 25;
            // 
            // labelCriar
            // 
            this.labelCriar.BackColor = System.Drawing.Color.Transparent;
            this.labelCriar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelCriar.Location = new System.Drawing.Point(156, 344);
            this.labelCriar.Name = "labelCriar";
            this.labelCriar.Size = new System.Drawing.Size(106, 48);
            this.labelCriar.TabIndex = 29;
            this.labelCriar.Click += new System.EventHandler(this.labelCriar_Click);
            // 
            // radioButtonMasculino
            // 
            this.radioButtonMasculino.AutoSize = true;
            this.radioButtonMasculino.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonMasculino.Checked = true;
            this.radioButtonMasculino.Location = new System.Drawing.Point(183, 245);
            this.radioButtonMasculino.Name = "radioButtonMasculino";
            this.radioButtonMasculino.Size = new System.Drawing.Size(14, 13);
            this.radioButtonMasculino.TabIndex = 35;
            this.radioButtonMasculino.TabStop = true;
            this.radioButtonMasculino.UseVisualStyleBackColor = false;
            this.radioButtonMasculino.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButtonFeminino
            // 
            this.radioButtonFeminino.AutoSize = true;
            this.radioButtonFeminino.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonFeminino.Location = new System.Drawing.Point(245, 245);
            this.radioButtonFeminino.Name = "radioButtonFeminino";
            this.radioButtonFeminino.Size = new System.Drawing.Size(14, 13);
            this.radioButtonFeminino.TabIndex = 36;
            this.radioButtonFeminino.UseVisualStyleBackColor = false;
            this.radioButtonFeminino.CheckedChanged += new System.EventHandler(this.radioButtonFeminino_CheckedChanged);
            // 
            // View_AdicionarCriança
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(658, 401);
            this.Controls.Add(this.radioButtonFeminino);
            this.Controls.Add(this.radioButtonMasculino);
            this.Controls.Add(this.labelCriar);
            this.Controls.Add(this.textBoxAtividade);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.textBoxPesoCriança);
            this.Controls.Add(this.textBoxIdadeCriança);
            this.Controls.Add(this.textBoxAlturaCriança);
            this.Controls.Add(this.pictureBoxFoto_Criança);
            this.Controls.Add(this.textBoxNomeCriança);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "View_AdicionarCriança";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adicionar uma Criança";
            this.Load += new System.EventHandler(this.View_AdicionarCriança_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFoto_Criança)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxNomeCriança;
        public System.Windows.Forms.PictureBox pictureBoxFoto_Criança;
        private System.Windows.Forms.TextBox textBoxAlturaCriança;
        private System.Windows.Forms.TextBox textBoxIdadeCriança;
        private System.Windows.Forms.TextBox textBoxPesoCriança;
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.TextBox textBoxAtividade;
        private System.Windows.Forms.Label labelCriar;
        private System.Windows.Forms.RadioButton radioButtonMasculino;
        private System.Windows.Forms.RadioButton radioButtonFeminino;
    }
}