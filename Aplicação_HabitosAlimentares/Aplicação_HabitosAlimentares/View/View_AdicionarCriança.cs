﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_AdicionarCriança : Form
    {
        public event MetodosComTresIntTresStrings RegistoCriança;
        public event MetodosComTresIntTresStrings AlterarPerfilCriança;
        OpenFileDialog dlg = new OpenFileDialog();
        public bool genero = false;
        public View_AdicionarCriança()
        {
            InitializeComponent();
            Program.M_App.RegistoFalhadoCriança += M_App_RegistoFalhadoCriança;
            Program.M_App.RegistoSucedidoCriança += M_App_RegistoSucedidoCriança;
            Program.M_App.RegistoMesmaCriança += M_App_RegistoMesmaCriança;
            //buttonCriarCriança.Enabled = false;
        }

        private void M_App_RegistoMesmaCriança()
        {
            MessageBox.Show("Já existe uma criança com os dados inseridos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void M_App_RegistoSucedidoCriança()
        {
            MessageBox.Show("O registo da criança foi efetuado com sucesso", "Registado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            Program.V_AltCriança.Show();
        }

        private void M_App_RegistoFalhadoCriança()
        {
            MessageBox.Show("Os campos não estão completos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button1_Click(object sender, EventArgs e)
        {
         
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_AdcCriança = new View_AdicionarCriança();
            Program.V_Menu.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void textBoxAlturaCriança_TextChanged(object sender, EventArgs e)
        {
            //if(double.Parse(textBoxAlturaCriança.Text) <3)
            //{ 
            //buttonCriarCriança.Enabled = textBoxAlturaCriança.Text != "" ? true : false;
            //}
        }

        private void pictureBoxFoto_Criança_Click(object sender, EventArgs e)
        {
            dlg.Filter = "Imagens|*.jpg;*.png;*.bmp|Ficheiros JPG|*.jpg|Ficheiros PNG|*.png|Ficheiros BMP|*.bmp|Todos os Ficheiros|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pictureBoxFoto_Criança.Image = null;
                pictureBoxFoto_Criança.Image = Image.FromFile(dlg.FileName);
            }

        }

        private void labelCriar_Click(object sender, EventArgs e)
        {
            this.Refresh();
            if (Program.V_Menu.adicionarCriança == "Alterar")
            {
                pictureBoxFoto_Criança.Image = null;
                pictureBoxFoto_Criança.ImageLocation = null;
                if (AlterarPerfilCriança != null) AlterarPerfilCriança(textBoxNomeCriança.Text, dlg.FileName, float.Parse(textBoxAlturaCriança.Text), Int32.Parse(textBoxIdadeCriança.Text), Int32.Parse(textBoxPesoCriança.Text), textBoxAtividade.Text);
            }
            else
            {         
                if (RegistoCriança != null)
                {
                    try { RegistoCriança(textBoxNomeCriança.Text, dlg.FileName, float.Parse(textBoxAlturaCriança.Text), Int32.Parse(textBoxIdadeCriança.Text), Int32.Parse(textBoxPesoCriança.Text), textBoxAtividade.Text); }
                    catch (FormatException)
                    {
                        M_App_RegistoFalhadoCriança();
                        //Program.V_AdcCriança.Show();

                    };
                }
                else
                {
                    M_App_RegistoSucedidoCriança();
                    //this.Hide();
                    //Program.V_Menu.Show();
                }
                Program.V_AdcCriança = new View_AdicionarCriança();
                Program.V_AltCriança = new View_AlterarDadosCriança();
            }

            float.TryParse(textBoxAlturaCriança.Text, out float altura);
            if (altura == 0) return;

            double peso_ideal = 0;
            // double idade = 0;
            //deve ser isto
            if (textBoxAlturaCriança.Text != "" ? true : false && double.Parse(textBoxAlturaCriança.Text) < 3)
            {
                if (radioButtonMasculino.Checked)
                {
                    if (textBoxIdadeCriança.Text != "")
                    {
                        double.TryParse(textBoxIdadeCriança.Text, out double idade);

                        if (idade <= 2)
                        {
                            peso_ideal = 18.6 * (altura * altura);
                        }
                        if (idade > 2 && idade <= 4)
                        {
                            peso_ideal = 19.3 * (altura * altura);
                        }
                        if (idade > 4 && idade <= 6)
                        {
                            peso_ideal = 19.7 * (altura * altura);
                        }
                        if (idade > 6 && idade <= 8)
                        {
                            peso_ideal = 20.3 * (altura * altura);
                        }
                        if (idade > 8 && idade <= 10)
                        {
                            peso_ideal = 20.8 * (altura * altura);
                        }
                        if (idade > 10 && idade <= 12)
                        {
                            peso_ideal = 21.4 * (altura * altura);
                        }
                        if (idade > 12 && idade <= 14)
                        {
                            peso_ideal = 21.9 * (altura * altura);
                        }
                        if (idade > 14 && idade <= 16)
                        {
                            peso_ideal = 22.3 * (altura * altura);
                        }
                        if (idade > 16)
                        {
                            peso_ideal = 23 * (altura * altura);
                        }
                        //homem
                        // peso_ideal = (72.7 * altura) - 58;
                        // peso_ideal = 21.9 * (altura * altura) ;

                    }   
                }
                else
                {
                    double.TryParse(textBoxIdadeCriança.Text, out double idade);

                    if (idade <= 2)
                    {
                        peso_ideal = 18.3 * (altura * altura);
                    }
                    if (idade > 2 && idade <= 4)
                    {
                        peso_ideal = 19 * (altura * altura);
                    }
                    if (idade > 4 && idade <= 6)
                    {
                        peso_ideal = 19.4 * (altura * altura);
                    }
                    if (idade > 6 && idade <= 8)
                    {
                        peso_ideal = 20 * (altura * altura);
                    }
                    if (idade > 8 && idade <= 10)
                    {
                        peso_ideal = 20.5 * (altura * altura);
                    }
                    if (idade > 10 && idade <= 12)
                    {
                        peso_ideal = 21.1 * (altura * altura);
                    }
                    if (idade > 12 && idade <= 14)
                    {
                        peso_ideal = 21.6 * (altura * altura);
                    }
                    if (idade > 14 && idade <= 16)
                    {
                        peso_ideal = 22 * (altura * altura);
                    }
                    if (idade > 16)
                    {
                        peso_ideal = 22.7 * (altura * altura);
                    }

                    //mulher
                    // peso_ideal = (62.7 * altura) - 44.7;

                }

                StringBuilder str = new StringBuilder("Peso Ideal: " + peso_ideal.ToString("0.00") + "Kg");

                //comparaçao com o peso atual

                if (textBoxPesoCriança.Text != "")
                {
                    double.TryParse(textBoxPesoCriança.Text, out double peso_atual);
                    if (peso_atual != 0)
                    {
                        if (peso_ideal < peso_atual)
                        {
                            str.Append(Environment.NewLine + "Tem de emagracer " + (peso_atual - peso_ideal).ToString(" 0.00 ") + " Kg");

                        }
                        else if (peso_ideal > peso_atual)
                        {
                            str.Append(Environment.NewLine + "Esta " + (peso_ideal - peso_atual).ToString("0.00") + " Kg abaixo do peso ideal.");
                        }
                        else
                        {
                            str.Append(Environment.NewLine + "Esta com o peso ideal!");
                        }
                    }
                }

                MessageBox.Show(str.ToString());
            }
        }

        private void radioButtonMasculino_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void labelMasculino_Click(object sender, EventArgs e)
        {
                genero = false;

        }

        private void labelFemenino_Click(object sender, EventArgs e)
        {
            genero = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void View_AdicionarCriança_Load(object sender, EventArgs e)
        {

        }

        private void radioButtonFeminino_CheckedChanged(object sender, EventArgs e)
        {

        }
    }

}
