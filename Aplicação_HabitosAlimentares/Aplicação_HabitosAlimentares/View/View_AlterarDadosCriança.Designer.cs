﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_AlterarDadosCriança
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_AlterarDadosCriança));
            this.button1 = new System.Windows.Forms.Button();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.comboBoxNomeCrianca = new System.Windows.Forms.ComboBox();
            this.buttonAdicionarACrianca = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(190, 209);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(215, 30);
            this.button1.TabIndex = 2;
            this.button1.Text = "Pesquisar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(9, 329);
            this.buttonVoltar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(89, 27);
            this.buttonVoltar.TabIndex = 3;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // comboBoxNomeCrianca
            // 
            this.comboBoxNomeCrianca.FormattingEnabled = true;
            this.comboBoxNomeCrianca.Location = new System.Drawing.Point(313, 148);
            this.comboBoxNomeCrianca.Name = "comboBoxNomeCrianca";
            this.comboBoxNomeCrianca.Size = new System.Drawing.Size(217, 21);
            this.comboBoxNomeCrianca.TabIndex = 4;
            this.comboBoxNomeCrianca.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // buttonAdicionarACrianca
            // 
            this.buttonAdicionarACrianca.Location = new System.Drawing.Point(452, 330);
            this.buttonAdicionarACrianca.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAdicionarACrianca.Name = "buttonAdicionarACrianca";
            this.buttonAdicionarACrianca.Size = new System.Drawing.Size(139, 27);
            this.buttonAdicionarACrianca.TabIndex = 6;
            this.buttonAdicionarACrianca.Text = "Adicionar Criança";
            this.buttonAdicionarACrianca.UseVisualStyleBackColor = true;
            this.buttonAdicionarACrianca.Click += new System.EventHandler(this.buttonAdicionarACrianca_Click);
            // 
            // View_AlterarDadosCriança
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.buttonAdicionarACrianca);
            this.Controls.Add(this.comboBoxNomeCrianca);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "View_AlterarDadosCriança";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "9";
            this.Load += new System.EventHandler(this.View_AlterarDadosCriança_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.ComboBox comboBoxNomeCrianca;
        private System.Windows.Forms.Button buttonAdicionarACrianca;
    }
}