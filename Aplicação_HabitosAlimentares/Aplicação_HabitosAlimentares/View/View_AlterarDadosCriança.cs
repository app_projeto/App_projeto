﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aplicação_HabitosAlimentares.Controller;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_AlterarDadosCriança : Form
    {
        public event MetodoComString Preencheracriança;
        public View_AlterarDadosCriança()
        {
            InitializeComponent();
            
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Menu.Show();
        }

        private void View_AlterarDadosCriança_Load(object sender, EventArgs e)
        {
            comboBoxNomeCrianca.Refresh();
            Controller_Crianca criancaController = new Controller_Crianca();
            comboBoxNomeCrianca.DataSource = criancaController.Listar();
            comboBoxNomeCrianca.ValueMember = "nome";
           // comboBoxNomeCrianca.DisplayMember = "";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Preencheracriança(comboBoxNomeCrianca.Text);
            this.Hide();
            Program.V_Pesquisar.Show();
        }

        private void buttonAdicionarACrianca_Click(object sender, EventArgs e)
        {
            this.Hide();
            //Program.V_AdcCriança = new View_AdicionarCriança();
            Program.V_AdcCriança.Show();
        }
    }
}
