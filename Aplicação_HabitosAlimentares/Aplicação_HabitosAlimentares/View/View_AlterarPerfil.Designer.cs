﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_AlterarPerfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_AlterarPerfil));
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.buttonAlterar_Perfil = new System.Windows.Forms.Button();
            this.textBoxNomePerfil = new System.Windows.Forms.TextBox();
            this.textBoxPasswordPerfil = new System.Windows.Forms.TextBox();
            this.textBoxIdadePerfil = new System.Windows.Forms.TextBox();
            this.textBoxtelemovelPerfil = new System.Windows.Forms.TextBox();
            this.textBoxEmailPerfil = new System.Windows.Forms.TextBox();
            this.pictureBoxAlterarPerfil = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlterarPerfil)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(333, 288);
            this.buttonVoltar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(70, 28);
            this.buttonVoltar.TabIndex = 2;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // buttonAlterar_Perfil
            // 
            this.buttonAlterar_Perfil.Location = new System.Drawing.Point(236, 288);
            this.buttonAlterar_Perfil.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAlterar_Perfil.Name = "buttonAlterar_Perfil";
            this.buttonAlterar_Perfil.Size = new System.Drawing.Size(68, 28);
            this.buttonAlterar_Perfil.TabIndex = 3;
            this.buttonAlterar_Perfil.Text = "Alterar";
            this.buttonAlterar_Perfil.UseVisualStyleBackColor = true;
            this.buttonAlterar_Perfil.Click += new System.EventHandler(this.buttonAlterar_Perfil_Click);
            // 
            // textBoxNomePerfil
            // 
            this.textBoxNomePerfil.Location = new System.Drawing.Point(236, 115);
            this.textBoxNomePerfil.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNomePerfil.Name = "textBoxNomePerfil";
            this.textBoxNomePerfil.Size = new System.Drawing.Size(167, 20);
            this.textBoxNomePerfil.TabIndex = 7;
            // 
            // textBoxPasswordPerfil
            // 
            this.textBoxPasswordPerfil.Location = new System.Drawing.Point(236, 177);
            this.textBoxPasswordPerfil.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPasswordPerfil.Name = "textBoxPasswordPerfil";
            this.textBoxPasswordPerfil.Size = new System.Drawing.Size(167, 20);
            this.textBoxPasswordPerfil.TabIndex = 8;
            this.textBoxPasswordPerfil.UseSystemPasswordChar = true;
            // 
            // textBoxIdadePerfil
            // 
            this.textBoxIdadePerfil.Location = new System.Drawing.Point(236, 211);
            this.textBoxIdadePerfil.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxIdadePerfil.Name = "textBoxIdadePerfil";
            this.textBoxIdadePerfil.Size = new System.Drawing.Size(167, 20);
            this.textBoxIdadePerfil.TabIndex = 9;
            // 
            // textBoxtelemovelPerfil
            // 
            this.textBoxtelemovelPerfil.Location = new System.Drawing.Point(236, 249);
            this.textBoxtelemovelPerfil.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxtelemovelPerfil.Name = "textBoxtelemovelPerfil";
            this.textBoxtelemovelPerfil.Size = new System.Drawing.Size(167, 20);
            this.textBoxtelemovelPerfil.TabIndex = 10;
            // 
            // textBoxEmailPerfil
            // 
            this.textBoxEmailPerfil.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxEmailPerfil.Location = new System.Drawing.Point(236, 142);
            this.textBoxEmailPerfil.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxEmailPerfil.Name = "textBoxEmailPerfil";
            this.textBoxEmailPerfil.ReadOnly = true;
            this.textBoxEmailPerfil.Size = new System.Drawing.Size(167, 20);
            this.textBoxEmailPerfil.TabIndex = 11;
            this.textBoxEmailPerfil.TextChanged += new System.EventHandler(this.textBoxEmailPerfil_TextChanged);
            // 
            // pictureBoxAlterarPerfil
            // 
            this.pictureBoxAlterarPerfil.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxAlterarPerfil.Location = new System.Drawing.Point(439, 62);
            this.pictureBoxAlterarPerfil.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxAlterarPerfil.Name = "pictureBoxAlterarPerfil";
            this.pictureBoxAlterarPerfil.Size = new System.Drawing.Size(130, 119);
            this.pictureBoxAlterarPerfil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAlterarPerfil.TabIndex = 14;
            this.pictureBoxAlterarPerfil.TabStop = false;
            this.pictureBoxAlterarPerfil.Click += new System.EventHandler(this.pictureBoxAlterarPerfil_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(148, 118);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(150, 149);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(78, 249);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Telefone/Telemóvel *";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(161, 211);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Idade";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(139, 180);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(494, 344);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "*Campo Opcional";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(436, 39);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Foto";
            // 
            // View_AlterarPerfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo3_3;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBoxAlterarPerfil);
            this.Controls.Add(this.textBoxEmailPerfil);
            this.Controls.Add(this.textBoxtelemovelPerfil);
            this.Controls.Add(this.textBoxIdadePerfil);
            this.Controls.Add(this.textBoxPasswordPerfil);
            this.Controls.Add(this.textBoxNomePerfil);
            this.Controls.Add(this.buttonAlterar_Perfil);
            this.Controls.Add(this.buttonVoltar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_AlterarPerfil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Perfil";
            this.Load += new System.EventHandler(this.View_Perfil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlterarPerfil)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.Button buttonAlterar_Perfil;
        private System.Windows.Forms.TextBox textBoxNomePerfil;
        private System.Windows.Forms.TextBox textBoxPasswordPerfil;
        private System.Windows.Forms.TextBox textBoxIdadePerfil;
        private System.Windows.Forms.TextBox textBoxtelemovelPerfil;
        private System.Windows.Forms.TextBox textBoxEmailPerfil;
        private System.Windows.Forms.PictureBox pictureBoxAlterarPerfil;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}