﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_AlterarPerfil : Form
    {
        public event MetodosComIntStrings alterarperfil;
        OpenFileDialog dlg = new OpenFileDialog();
        int tele = 0;
        public View_AlterarPerfil()
        {
            InitializeComponent();
            
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Menu.Show();
        }

        private void View_Perfil_Load(object sender, EventArgs e)
        {
            textBoxNomePerfil.Text = Program.M_App.Pais.Nome;
            textBoxEmailPerfil.Text = Program.M_App.Pais.Email;
            //textBoxPasswordPerfil.Text = Program.M_App.Pais.Password;
            textBoxIdadePerfil.Text = Program.M_App.Pais.Idade.ToString();
            textBoxtelemovelPerfil.Text = Program.M_App.Pais.telemovel.ToString();
            pictureBoxAlterarPerfil.Image = Image.FromFile("Imagens\\" +Program.M_App.Pais.Localizacao_imagem);

        }

        private void buttonAdicionarImagemPerfil_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonAlterar_Perfil_Click(object sender, EventArgs e)
        {
            if (textBoxtelemovelPerfil.Text != "")
            {
                tele = Int32.Parse(textBoxtelemovelPerfil.Text);
            }
            alterarperfil(textBoxNomePerfil.Text, textBoxPasswordPerfil.Text, dlg.FileName, Program.M_App.Pais.Email, Int32.Parse(textBoxIdadePerfil.Text), tele);
            if (alterarperfil != null)
            {
                try { alterarperfil(textBoxNomePerfil.Text, textBoxPasswordPerfil.Text, dlg.FileName, Program.M_App.Pais.Email, Int32.Parse(textBoxIdadePerfil.Text), tele); }
                catch (FormatException)
                {
                    MessageBox.Show("Alteração de Perfil falhada. Confirme os dados.");
                    Program.V_AlterarPerfil.Show();

                };
            }
            else
            {
                MessageBox.Show("Alteração de Perfil com Sucesso.");
                this.Hide();
                Program.V_Menu.Show();
            }
        }

        private void pictureBoxAlterarPerfil_Click(object sender, EventArgs e)
        {
            dlg.Filter = "Imagens|*.jpg;*.png;*.bmp|Ficheiros JPG|*.jpg|Ficheiros PNG|*.png|Ficheiros BMP|*.bmp|Todos os Ficheiros|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pictureBoxAlterarPerfil.Image = null;
                pictureBoxAlterarPerfil.Image = Image.FromFile(dlg.FileName);
            }
        }

        private void textBoxEmailPerfil_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
