﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_CalcularPesoIdeal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_CalcularPesoIdeal));
            this.textBoxAltura = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxPesoAtual = new System.Windows.Forms.TextBox();
            this.textBoxIdade = new System.Windows.Forms.TextBox();
            this.labelCalcular = new System.Windows.Forms.Label();
            this.radioButtonmasc = new System.Windows.Forms.RadioButton();
            this.radioButtonfem = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // textBoxAltura
            // 
            this.textBoxAltura.Location = new System.Drawing.Point(136, 158);
            this.textBoxAltura.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxAltura.Name = "textBoxAltura";
            this.textBoxAltura.Size = new System.Drawing.Size(59, 20);
            this.textBoxAltura.TabIndex = 3;
            this.textBoxAltura.TextChanged += new System.EventHandler(this.textBoxAltura_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(487, 324);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 31);
            this.button2.TabIndex = 4;
            this.button2.Text = "Voltar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxPesoAtual
            // 
            this.textBoxPesoAtual.Location = new System.Drawing.Point(136, 182);
            this.textBoxPesoAtual.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPesoAtual.Name = "textBoxPesoAtual";
            this.textBoxPesoAtual.Size = new System.Drawing.Size(59, 20);
            this.textBoxPesoAtual.TabIndex = 8;
            // 
            // textBoxIdade
            // 
            this.textBoxIdade.Location = new System.Drawing.Point(136, 134);
            this.textBoxIdade.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxIdade.Name = "textBoxIdade";
            this.textBoxIdade.Size = new System.Drawing.Size(59, 20);
            this.textBoxIdade.TabIndex = 10;
            this.textBoxIdade.TextChanged += new System.EventHandler(this.textBoxIdade_TextChanged);
            // 
            // labelCalcular
            // 
            this.labelCalcular.BackColor = System.Drawing.Color.Transparent;
            this.labelCalcular.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelCalcular.Location = new System.Drawing.Point(12, 282);
            this.labelCalcular.Name = "labelCalcular";
            this.labelCalcular.Size = new System.Drawing.Size(194, 35);
            this.labelCalcular.TabIndex = 33;
            this.labelCalcular.Click += new System.EventHandler(this.labelCalcular_Click);
            // 
            // radioButtonmasc
            // 
            this.radioButtonmasc.AutoSize = true;
            this.radioButtonmasc.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonmasc.Checked = true;
            this.radioButtonmasc.Location = new System.Drawing.Point(99, 215);
            this.radioButtonmasc.Name = "radioButtonmasc";
            this.radioButtonmasc.Size = new System.Drawing.Size(14, 13);
            this.radioButtonmasc.TabIndex = 34;
            this.radioButtonmasc.TabStop = true;
            this.radioButtonmasc.UseVisualStyleBackColor = false;
            // 
            // radioButtonfem
            // 
            this.radioButtonfem.AutoSize = true;
            this.radioButtonfem.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonfem.Location = new System.Drawing.Point(153, 215);
            this.radioButtonfem.Name = "radioButtonfem";
            this.radioButtonfem.Size = new System.Drawing.Size(14, 13);
            this.radioButtonfem.TabIndex = 35;
            this.radioButtonfem.UseVisualStyleBackColor = false;
            // 
            // View_CalcularPesoIdeal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo5_1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.radioButtonfem);
            this.Controls.Add(this.radioButtonmasc);
            this.Controls.Add(this.labelCalcular);
            this.Controls.Add(this.textBoxIdade);
            this.Controls.Add(this.textBoxPesoAtual);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBoxAltura);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_CalcularPesoIdeal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Calculadora do Peso Ideal";
            this.Load += new System.EventHandler(this.View_CalcularPesoIdeal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxAltura;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxPesoAtual;
        private System.Windows.Forms.TextBox textBoxIdade;
        private System.Windows.Forms.Label labelCalcular;
        private System.Windows.Forms.RadioButton radioButtonmasc;
        private System.Windows.Forms.RadioButton radioButtonfem;
    }
}