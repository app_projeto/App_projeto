﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    
    public partial class View_CalcularPesoIdeal : Form
    {
      
        public View_CalcularPesoIdeal()
        {
            InitializeComponent();
            //buttonCalcularPesoIdeal.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_CalcularPesoIdeal = new View_CalcularPesoIdeal();
            Program.V_Menu.Show();
        }

        private void textBoxAltura_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonCalcularPesoIdeal_Click(object sender, EventArgs e)
        {
           
               // this.Hide();
               // Program.V_Menu.Show();
               // str.Clear();
        }

    
        private void labelCalcular_Click(object sender, EventArgs e)
        {
            if (textBoxAltura.Text != "" ? true : false == true)
            {
                double.TryParse(textBoxAltura.Text, out double altura1);
                if (altura1 == 0) return;

                double peso_ideal = 0;
                // double idade = 0;

                if (radioButtonmasc.Checked)
                {
                    if (textBoxIdade.Text != "")
                    {
                        double.TryParse(textBoxIdade.Text, out double idade);

                        if (idade <= 2)
                        {
                            peso_ideal = 18.6 * (altura1 * altura1);
                        }
                        if (idade > 2 && idade <= 4)
                        {
                            peso_ideal = 19.3 * (altura1 * altura1);
                        }
                        if (idade > 4 && idade <= 6)
                        {
                            peso_ideal = 19.7 * (altura1 * altura1);
                        }
                        if (idade > 6 && idade <= 8)
                        {
                            peso_ideal = 20.3 * (altura1 * altura1);
                        }
                        if (idade > 8 && idade <= 10)
                        {
                            peso_ideal = 20.8 * (altura1 * altura1);
                        }
                        if (idade > 10 && idade <= 12)
                        {
                            peso_ideal = 21.4 * (altura1 * altura1);
                        }
                        if (idade > 12 && idade <= 14)
                        {
                            peso_ideal = 21.9 * (altura1 * altura1);
                        }
                        if (idade > 14 && idade <= 16)
                        {
                            peso_ideal = 22.3 * (altura1 * altura1);
                        }
                        if (idade > 16)
                        {
                            peso_ideal = 23 * (altura1 * altura1);
                        }
                        //homem
                        // peso_ideal = (72.7 * altura) - 58;
                        // peso_ideal = 21.9 * (altura * altura) ;

                    }
                }
                else
                {
                    double.TryParse(textBoxIdade.Text, out double idade);

                    if (idade <= 2)
                    {
                        peso_ideal = 18.3 * (altura1 * altura1);
                    }
                    if (idade > 2 && idade <= 4)
                    {
                        peso_ideal = 19 * (altura1 * altura1);
                    }
                    if (idade > 4 && idade <= 6)
                    {
                        peso_ideal = 19.4 * (altura1 * altura1);
                    }
                    if (idade > 6 && idade <= 8)
                    {
                        peso_ideal = 20 * (altura1 * altura1);
                    }
                    if (idade > 8 && idade <= 10)
                    {
                        peso_ideal = 20.5 * (altura1 * altura1);
                    }
                    if (idade > 10 && idade <= 12)
                    {
                        peso_ideal = 21.1 * (altura1 * altura1);
                    }
                    if (idade > 12 && idade <= 14)
                    {
                        peso_ideal = 21.6 * (altura1 * altura1);
                    }
                    if (idade > 14 && idade <= 16)
                    {
                        peso_ideal = 22 * (altura1 * altura1);
                    }
                    if (idade > 16)
                    {
                        peso_ideal = 22.7 * (altura1 * altura1);
                    }
                    //mulher
                    // peso_ideal = (62.7 * altura) - 44.7;

                }

                StringBuilder str = new StringBuilder("Peso Ideal: " + peso_ideal.ToString("0.00") + "Kg");

                //comparaçao com o peso atual

                if (textBoxPesoAtual.Text != "")
                {
                    double.TryParse(textBoxPesoAtual.Text, out double peso_atual);
                    if (peso_atual != 0)
                    {
                        if (peso_ideal < peso_atual)
                        {
                            str.Append(Environment.NewLine + "Tem de emagracer " + (peso_atual - peso_ideal).ToString(" 0.00 ") + " Kg");

                        }
                        else if (peso_ideal > peso_atual)
                        {
                            str.Append(Environment.NewLine + "Esta " + (peso_ideal - peso_atual).ToString("0.00") + " Kg abaixo do peso ideal.");
                        }
                        else
                        {
                            str.Append(Environment.NewLine + "Esta com o peso ideal!");
                        }
                    }
                }
                MessageBox.Show(str.ToString(), "Peso Ideal", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void View_CalcularPesoIdeal_Load(object sender, EventArgs e)
        {

        }

        private void textBoxIdade_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
