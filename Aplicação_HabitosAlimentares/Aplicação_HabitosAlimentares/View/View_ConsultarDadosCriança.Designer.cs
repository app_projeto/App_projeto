﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_ConsultarDadosCriança
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_ConsultarDadosCriança));
            this.dataGridViewDados = new System.Windows.Forms.DataGridView();
            this.textBoxPesquisarDadosCrianca = new System.Windows.Forms.TextBox();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.buttonPlanoDeAlimentacao = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDados)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewDados
            // 
            this.dataGridViewDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDados.Location = new System.Drawing.Point(8, 55);
            this.dataGridViewDados.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewDados.Name = "dataGridViewDados";
            this.dataGridViewDados.RowHeadersWidth = 51;
            this.dataGridViewDados.RowTemplate.Height = 24;
            this.dataGridViewDados.Size = new System.Drawing.Size(582, 301);
            this.dataGridViewDados.TabIndex = 0;
            this.dataGridViewDados.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDados_CellContentClick_1);
            // 
            // textBoxPesquisarDadosCrianca
            // 
            this.textBoxPesquisarDadosCrianca.Location = new System.Drawing.Point(11, 24);
            this.textBoxPesquisarDadosCrianca.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPesquisarDadosCrianca.Name = "textBoxPesquisarDadosCrianca";
            this.textBoxPesquisarDadosCrianca.Size = new System.Drawing.Size(579, 20);
            this.textBoxPesquisarDadosCrianca.TabIndex = 1;
            this.textBoxPesquisarDadosCrianca.TextChanged += new System.EventHandler(this.textBoxPesquisarDadosCrianca_TextChanged);
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(8, 366);
            this.buttonVoltar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(73, 19);
            this.buttonVoltar.TabIndex = 3;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // buttonPlanoDeAlimentacao
            // 
            this.buttonPlanoDeAlimentacao.Location = new System.Drawing.Point(445, 364);
            this.buttonPlanoDeAlimentacao.Name = "buttonPlanoDeAlimentacao";
            this.buttonPlanoDeAlimentacao.Size = new System.Drawing.Size(137, 23);
            this.buttonPlanoDeAlimentacao.TabIndex = 4;
            this.buttonPlanoDeAlimentacao.Text = "Plano De Alimentacao";
            this.buttonPlanoDeAlimentacao.UseVisualStyleBackColor = true;
            this.buttonPlanoDeAlimentacao.Click += new System.EventHandler(this.buttonPlanoDeAlimentacao_Click);
            // 
            // View_ConsultarDadosCriança
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(594, 395);
            this.Controls.Add(this.buttonPlanoDeAlimentacao);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.textBoxPesquisarDadosCrianca);
            this.Controls.Add(this.dataGridViewDados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_ConsultarDadosCriança";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View_ConsultarDadosCriança";
            this.Load += new System.EventHandler(this.View_ConsultarDadosCriança_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDados;
        private System.Windows.Forms.TextBox textBoxPesquisarDadosCrianca;
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.Button buttonPlanoDeAlimentacao;
    }
}