﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aplicação_HabitosAlimentares.Controller;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_ConsultarDadosCriança : Form
    {
        public View_ConsultarDadosCriança()
        {
            InitializeComponent();
        }

        private void View_ConsultarDadosCriança_Load(object sender, EventArgs e)
        {
            this.Refresh();
            Controller_Crianca criancaController = new Controller_Crianca();

            dataGridViewDados.DataSource = criancaController.Listar();

        }
        private void Pesquisar(Crianca crianca)
        {
            crianca.nome = textBoxPesquisarDadosCrianca.Text.Trim();

            Controller_Crianca controller_Crianca = new Controller_Crianca();

            dataGridViewDados.DataSource = controller_Crianca.Pesquisar(crianca);
        }

        private void textBoxPesquisarDadosCrianca_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPesquisarDadosCrianca.Text == "")
            {
                Controller_Crianca controller_Crianca = new Controller_Crianca();

                dataGridViewDados.DataSource = controller_Crianca.Listar();

            }
            else
            {
                Crianca crianca = new Crianca();
                Pesquisar(crianca);

            }
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Menu.Show();
        }

        private void dataGridViewDados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonPlanoDeAlimentacao_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_PlanoDeAlimentacao.Show();
        }

        private void dataGridViewDados_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
