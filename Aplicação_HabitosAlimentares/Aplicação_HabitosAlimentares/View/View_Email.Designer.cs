﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_Email
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnviar = new System.Windows.Forms.Button();
            this.labelEmail = new System.Windows.Forms.Label();
            this.labelTitulo = new System.Windows.Forms.Label();
            this.labelmail = new System.Windows.Forms.Label();
            this.labelsenhaa = new System.Windows.Forms.Label();
            this.labeltextomail = new System.Windows.Forms.Label();
            this.textBoxDestinatário = new System.Windows.Forms.TextBox();
            this.textBoxtitulo = new System.Windows.Forms.TextBox();
            this.textBox_email = new System.Windows.Forms.TextBox();
            this.textBoxsenhaa = new System.Windows.Forms.TextBox();
            this.textBoxtextoemail = new System.Windows.Forms.TextBox();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonEnviar
            // 
            this.buttonEnviar.Location = new System.Drawing.Point(187, 469);
            this.buttonEnviar.Name = "buttonEnviar";
            this.buttonEnviar.Size = new System.Drawing.Size(133, 49);
            this.buttonEnviar.TabIndex = 0;
            this.buttonEnviar.Text = "Enviar";
            this.buttonEnviar.UseVisualStyleBackColor = true;
            this.buttonEnviar.Click += new System.EventHandler(this.buttonEnviar_Click);
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(33, 23);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(84, 17);
            this.labelEmail.TabIndex = 1;
            this.labelEmail.Text = "Destinatário";
            // 
            // labelTitulo
            // 
            this.labelTitulo.AutoSize = true;
            this.labelTitulo.Location = new System.Drawing.Point(33, 93);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(43, 17);
            this.labelTitulo.TabIndex = 2;
            this.labelTitulo.Text = "Titulo";
            // 
            // labelmail
            // 
            this.labelmail.AutoSize = true;
            this.labelmail.Location = new System.Drawing.Point(34, 163);
            this.labelmail.Name = "labelmail";
            this.labelmail.Size = new System.Drawing.Size(71, 17);
            this.labelmail.TabIndex = 3;
            this.labelmail.Text = "Seu Email";
            // 
            // labelsenhaa
            // 
            this.labelsenhaa.AutoSize = true;
            this.labelsenhaa.Location = new System.Drawing.Point(33, 234);
            this.labelsenhaa.Name = "labelsenhaa";
            this.labelsenhaa.Size = new System.Drawing.Size(49, 17);
            this.labelsenhaa.TabIndex = 4;
            this.labelsenhaa.Text = "Senha";
            this.labelsenhaa.Click += new System.EventHandler(this.label4_Click);
            // 
            // labeltextomail
            // 
            this.labeltextomail.AutoSize = true;
            this.labeltextomail.Location = new System.Drawing.Point(33, 314);
            this.labeltextomail.Name = "labeltextomail";
            this.labeltextomail.Size = new System.Drawing.Size(43, 17);
            this.labeltextomail.TabIndex = 5;
            this.labeltextomail.Text = "Texto";
            // 
            // textBoxDestinatário
            // 
            this.textBoxDestinatário.Location = new System.Drawing.Point(36, 53);
            this.textBoxDestinatário.Name = "textBoxDestinatário";
            this.textBoxDestinatário.Size = new System.Drawing.Size(311, 22);
            this.textBoxDestinatário.TabIndex = 6;
            // 
            // textBoxtitulo
            // 
            this.textBoxtitulo.Location = new System.Drawing.Point(37, 126);
            this.textBoxtitulo.Name = "textBoxtitulo";
            this.textBoxtitulo.Size = new System.Drawing.Size(311, 22);
            this.textBoxtitulo.TabIndex = 7;
            // 
            // textBox_email
            // 
            this.textBox_email.Location = new System.Drawing.Point(36, 193);
            this.textBox_email.Name = "textBox_email";
            this.textBox_email.Size = new System.Drawing.Size(311, 22);
            this.textBox_email.TabIndex = 8;
            this.textBox_email.TextChanged += new System.EventHandler(this.textBox_email_TextChanged);
            // 
            // textBoxsenhaa
            // 
            this.textBoxsenhaa.Location = new System.Drawing.Point(36, 263);
            this.textBoxsenhaa.Name = "textBoxsenhaa";
            this.textBoxsenhaa.Size = new System.Drawing.Size(311, 22);
            this.textBoxsenhaa.TabIndex = 9;
            this.textBoxsenhaa.UseSystemPasswordChar = true;
            // 
            // textBoxtextoemail
            // 
            this.textBoxtextoemail.Location = new System.Drawing.Point(36, 343);
            this.textBoxtextoemail.Multiline = true;
            this.textBoxtextoemail.Name = "textBoxtextoemail";
            this.textBoxtextoemail.Size = new System.Drawing.Size(460, 100);
            this.textBoxtextoemail.TabIndex = 10;
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(407, 502);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(89, 30);
            this.buttonVoltar.TabIndex = 11;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // View_Email
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 544);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.textBoxtextoemail);
            this.Controls.Add(this.textBoxsenhaa);
            this.Controls.Add(this.textBox_email);
            this.Controls.Add(this.textBoxtitulo);
            this.Controls.Add(this.textBoxDestinatário);
            this.Controls.Add(this.labeltextomail);
            this.Controls.Add(this.labelsenhaa);
            this.Controls.Add(this.labelmail);
            this.Controls.Add(this.labelTitulo);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.buttonEnviar);
            this.Name = "View_Email";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View_Email";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEnviar;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label labelTitulo;
        private System.Windows.Forms.Label labelmail;
        private System.Windows.Forms.Label labelsenhaa;
        private System.Windows.Forms.Label labeltextomail;
        private System.Windows.Forms.TextBox textBoxDestinatário;
        private System.Windows.Forms.TextBox textBoxtitulo;
        private System.Windows.Forms.TextBox textBox_email;
        private System.Windows.Forms.TextBox textBoxsenhaa;
        private System.Windows.Forms.TextBox textBoxtextoemail;
        private System.Windows.Forms.Button buttonVoltar;
    }
}