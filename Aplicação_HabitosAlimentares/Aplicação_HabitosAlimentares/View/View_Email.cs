﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_Email : Form
    { 

        private MailMessage Email;
        Stopwatch Stop = new Stopwatch();
        public View_Email()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void buttonEnviar_Click(object sender, EventArgs e)
        {
            Email = new MailMessage();
            Email.To.Add(new MailAddress(textBoxDestinatário.Text));
            Email.From = (new MailAddress(textBox_email.Text));
            Email.Subject = textBoxtitulo.Text;
            Email.IsBodyHtml = true;
            Email.Body = textBoxtextoemail.Text;
            SmtpClient cliente = new SmtpClient("smtp.live.com", 587);
            using(cliente)
            {
                cliente.Credentials = new System.Net.NetworkCredential(textBox_email.Text, textBoxsenhaa.Text);
                cliente.EnableSsl = true;
                cliente.Send(Email);
            }
            MessageBox.Show("Email enviado com sucesso");
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Email = new View_Email();
            Program.V_Menu.Show();
        }

        private void textBox_email_TextChanged(object sender, EventArgs e)
        {
            textBox_email.Text = Program.M_App.Pais.Email;
        }
    }
}
