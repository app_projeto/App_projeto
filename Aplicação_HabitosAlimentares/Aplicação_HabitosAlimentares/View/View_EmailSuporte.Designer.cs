﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_EmailSuporte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_EmailSuporte));
            this.textBoxDestinatário = new System.Windows.Forms.TextBox();
            this.textBoxTextoEmail = new System.Windows.Forms.TextBox();
            this.textBoxTitulo = new System.Windows.Forms.TextBox();
            this.buttonEnviarEmail = new System.Windows.Forms.Button();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.textBoxEmailPess = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxDestinatário
            // 
            this.textBoxDestinatário.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBoxDestinatário.Location = new System.Drawing.Point(122, 150);
            this.textBoxDestinatário.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDestinatário.Multiline = true;
            this.textBoxDestinatário.Name = "textBoxDestinatário";
            this.textBoxDestinatário.ReadOnly = true;
            this.textBoxDestinatário.Size = new System.Drawing.Size(288, 35);
            this.textBoxDestinatário.TabIndex = 5;
            this.textBoxDestinatário.Text = "al66518@utad.eu";
            // 
            // textBoxTextoEmail
            // 
            this.textBoxTextoEmail.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxTextoEmail.Location = new System.Drawing.Point(122, 265);
            this.textBoxTextoEmail.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxTextoEmail.Multiline = true;
            this.textBoxTextoEmail.Name = "textBoxTextoEmail";
            this.textBoxTextoEmail.Size = new System.Drawing.Size(288, 101);
            this.textBoxTextoEmail.TabIndex = 6;
            this.textBoxTextoEmail.Text = "Texto:";
            this.textBoxTextoEmail.Click += new System.EventHandler(this.textBoxTextoEmail_Click);
            // 
            // textBoxTitulo
            // 
            this.textBoxTitulo.Location = new System.Drawing.Point(122, 189);
            this.textBoxTitulo.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxTitulo.Multiline = true;
            this.textBoxTitulo.Name = "textBoxTitulo";
            this.textBoxTitulo.Size = new System.Drawing.Size(288, 34);
            this.textBoxTitulo.TabIndex = 7;
            this.textBoxTitulo.Text = "Titulo/Problema:";
            this.textBoxTitulo.Click += new System.EventHandler(this.textBoxTitulo_Click);
            // 
            // buttonEnviarEmail
            // 
            this.buttonEnviarEmail.BackColor = System.Drawing.Color.Transparent;
            this.buttonEnviarEmail.Location = new System.Drawing.Point(274, 376);
            this.buttonEnviarEmail.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEnviarEmail.Name = "buttonEnviarEmail";
            this.buttonEnviarEmail.Size = new System.Drawing.Size(136, 43);
            this.buttonEnviarEmail.TabIndex = 10;
            this.buttonEnviarEmail.Text = "Enviar";
            this.buttonEnviarEmail.UseVisualStyleBackColor = false;
            this.buttonEnviarEmail.Click += new System.EventHandler(this.buttonEnviarEmail_Click);
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.BackColor = System.Drawing.Color.Transparent;
            this.buttonVoltar.Location = new System.Drawing.Point(122, 376);
            this.buttonVoltar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(133, 43);
            this.buttonVoltar.TabIndex = 11;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = false;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // textBoxEmailPess
            // 
            this.textBoxEmailPess.Location = new System.Drawing.Point(122, 227);
            this.textBoxEmailPess.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxEmailPess.Multiline = true;
            this.textBoxEmailPess.Name = "textBoxEmailPess";
            this.textBoxEmailPess.Size = new System.Drawing.Size(288, 34);
            this.textBoxEmailPess.TabIndex = 13;
            this.textBoxEmailPess.Text = "O seu Email:";
            this.textBoxEmailPess.Click += new System.EventHandler(this.textBoxEmailPess_Click);
            this.textBoxEmailPess.TextChanged += new System.EventHandler(this.textBoxEmailPess_TextChanged);
            // 
            // View_EmailSuporte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo11_1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(530, 490);
            this.Controls.Add(this.textBoxEmailPess);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.buttonEnviarEmail);
            this.Controls.Add(this.textBoxTitulo);
            this.Controls.Add(this.textBoxTextoEmail);
            this.Controls.Add(this.textBoxDestinatário);
            this.Cursor = System.Windows.Forms.Cursors.Help;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_EmailSuporte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email de Suporte";
            this.Load += new System.EventHandler(this.View_EmailSuporte_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxDestinatário;
        private System.Windows.Forms.TextBox textBoxTextoEmail;
        private System.Windows.Forms.TextBox textBoxTitulo;
        private System.Windows.Forms.Button buttonEnviarEmail;
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.TextBox textBoxEmailPess;
    }
}