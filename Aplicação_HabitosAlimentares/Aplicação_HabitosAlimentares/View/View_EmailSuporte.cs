﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_EmailSuporte : Form

    {
        private MailMessage Email;
        Stopwatch Stop = new Stopwatch();

        public View_EmailSuporte()
        {
            InitializeComponent();
        }

        private void View_EmailSuporte_Load(object sender, EventArgs e)
        {

        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Email_suporte = new View_EmailSuporte();
            Program.V_Menu.Show();
        }

        private void buttonEnviarEmail_Click(object sender, EventArgs e)
        {
            if (textBoxTextoEmail.ToString() != "")
            {
                Email = new MailMessage();
                Email.To.Add(new MailAddress(textBoxDestinatário.Text));
                Email.From = (new MailAddress(textBoxDestinatário.Text));
                Email.Subject = textBoxTitulo.Text;
                Email.IsBodyHtml = true;
                Email.Body = textBoxEmailPess.Text + "\n \n" + textBoxTextoEmail.Text;
                SmtpClient cliente = new SmtpClient("smtp.live.com", 587);
                using (cliente)
                {
                    cliente.Credentials = new System.Net.NetworkCredential(textBoxDestinatário.Text, "88vv22un");
                    cliente.EnableSsl = true;
                    cliente.Send(Email);
                }
                textBoxTextoEmail.Clear();
                textBoxTitulo.Clear();
                MessageBox.Show("Email enviado com sucesso");
            }
            else
            {
                MessageBox.Show("Email nao enviado, tente novamente");
            }
            
        }

        private void textBoxEmailPess_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxTitulo_Click(object sender, EventArgs e)
        {
            if (textBoxTitulo.Text == "")
            {
                textBoxTitulo.Text = "Titulo/Problema:";
                return;
            }
            if (textBoxTitulo.Text == "Titulo/Problema:")
            {
                textBoxTitulo.Clear();
                return;
            }
        }

        private void textBoxEmailPess_Click(object sender, EventArgs e)
        {
            if (textBoxEmailPess.Text == "")
            {
                textBoxEmailPess.Text = "O seu Email:";
                return;
            }
            if (textBoxEmailPess.Text == "O seu Email:")
            {
                textBoxEmailPess.Clear();
                return;
            }
        }

        private void textBoxTextoEmail_Click(object sender, EventArgs e)
        {
            if (textBoxTextoEmail.Text == "")
            {
                textBoxTextoEmail.Text = "Texto:";
                return;
            }
            if (textBoxTextoEmail.Text == "Texto:")
            {
                textBoxTextoEmail.Clear();
                return;
            }
        }
    }
}
