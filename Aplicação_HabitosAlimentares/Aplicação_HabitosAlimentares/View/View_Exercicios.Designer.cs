﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_Exercicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_Exercicios));
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonBasico = new System.Windows.Forms.RadioButton();
            this.radioButtonBasicoM = new System.Windows.Forms.RadioButton();
            this.radioButtonMedio = new System.Windows.Forms.RadioButton();
            this.radioMedioM = new System.Windows.Forms.RadioButton();
            this.radioDificil = new System.Windows.Forms.RadioButton();
            this.pictureBoxExercicio = new System.Windows.Forms.PictureBox();
            this.buttonvolta = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxExercicio)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(12, 415);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(75, 23);
            this.buttonVoltar.TabIndex = 0;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(362, 368);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dificuldade";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(320, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Plano";
            // 
            // radioButtonBasico
            // 
            this.radioButtonBasico.AutoSize = true;
            this.radioButtonBasico.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButtonBasico.Location = new System.Drawing.Point(269, 640);
            this.radioButtonBasico.Name = "radioButtonBasico";
            this.radioButtonBasico.Size = new System.Drawing.Size(43, 30);
            this.radioButtonBasico.TabIndex = 4;
            this.radioButtonBasico.TabStop = true;
            this.radioButtonBasico.Text = "Basico";
            this.radioButtonBasico.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButtonBasico.UseVisualStyleBackColor = true;
            this.radioButtonBasico.CheckedChanged += new System.EventHandler(this.radioButtonBasico_CheckedChanged);
            // 
            // radioButtonBasicoM
            // 
            this.radioButtonBasicoM.AutoSize = true;
            this.radioButtonBasicoM.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButtonBasicoM.Location = new System.Drawing.Point(539, 640);
            this.radioButtonBasicoM.Name = "radioButtonBasicoM";
            this.radioButtonBasicoM.Size = new System.Drawing.Size(49, 30);
            this.radioButtonBasicoM.TabIndex = 5;
            this.radioButtonBasicoM.TabStop = true;
            this.radioButtonBasicoM.Text = "Basico+";
            this.radioButtonBasicoM.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButtonBasicoM.UseVisualStyleBackColor = true;
            this.radioButtonBasicoM.CheckedChanged += new System.EventHandler(this.radioButtonBasicoM_CheckedChanged);
            // 
            // radioButtonMedio
            // 
            this.radioButtonMedio.AutoSize = true;
            this.radioButtonMedio.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButtonMedio.Location = new System.Drawing.Point(746, 640);
            this.radioButtonMedio.Name = "radioButtonMedio";
            this.radioButtonMedio.Size = new System.Drawing.Size(40, 30);
            this.radioButtonMedio.TabIndex = 6;
            this.radioButtonMedio.TabStop = true;
            this.radioButtonMedio.Text = "Médio";
            this.radioButtonMedio.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButtonMedio.UseVisualStyleBackColor = true;
            this.radioButtonMedio.CheckedChanged += new System.EventHandler(this.radioButtonMedio_CheckedChanged);
            // 
            // radioMedioM
            // 
            this.radioMedioM.AutoSize = true;
            this.radioMedioM.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioMedioM.Location = new System.Drawing.Point(976, 640);
            this.radioMedioM.Name = "radioMedioM";
            this.radioMedioM.Size = new System.Drawing.Size(46, 30);
            this.radioMedioM.TabIndex = 7;
            this.radioMedioM.TabStop = true;
            this.radioMedioM.Text = "Médio+";
            this.radioMedioM.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioMedioM.UseVisualStyleBackColor = true;
            this.radioMedioM.CheckedChanged += new System.EventHandler(this.radioMedioM_CheckedChanged);
            // 
            // radioDificil
            // 
            this.radioDificil.AutoSize = true;
            this.radioDificil.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioDificil.Location = new System.Drawing.Point(1161, 640);
            this.radioDificil.Name = "radioDificil";
            this.radioDificil.Size = new System.Drawing.Size(38, 30);
            this.radioDificil.TabIndex = 8;
            this.radioDificil.TabStop = true;
            this.radioDificil.Text = "Díficil";
            this.radioDificil.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioDificil.UseVisualStyleBackColor = true;
            this.radioDificil.CheckedChanged += new System.EventHandler(this.radioDificil_CheckedChanged);
            // 
            // pictureBoxExercicio
            // 
            this.pictureBoxExercicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxExercicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxExercicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxExercicio.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxExercicio.Name = "pictureBoxExercicio";
            this.pictureBoxExercicio.Size = new System.Drawing.Size(1395, 682);
            this.pictureBoxExercicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxExercicio.TabIndex = 3;
            this.pictureBoxExercicio.TabStop = false;
            // 
            // buttonvolta
            // 
            this.buttonvolta.Location = new System.Drawing.Point(21, 640);
            this.buttonvolta.Name = "buttonvolta";
            this.buttonvolta.Size = new System.Drawing.Size(75, 23);
            this.buttonvolta.TabIndex = 9;
            this.buttonvolta.Text = "Voltar";
            this.buttonvolta.UseVisualStyleBackColor = true;
            this.buttonvolta.Click += new System.EventHandler(this.buttonvolta_Click);
            // 
            // View_Exercicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1395, 682);
            this.Controls.Add(this.buttonvolta);
            this.Controls.Add(this.radioDificil);
            this.Controls.Add(this.radioMedioM);
            this.Controls.Add(this.radioButtonMedio);
            this.Controls.Add(this.radioButtonBasicoM);
            this.Controls.Add(this.radioButtonBasico);
            this.Controls.Add(this.pictureBoxExercicio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonVoltar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "View_Exercicios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Plano de Exercicios";
            this.Load += new System.EventHandler(this.View_Exercicios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxExercicio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBoxExercicio;
        private System.Windows.Forms.RadioButton radioButtonBasico;
        private System.Windows.Forms.RadioButton radioButtonBasicoM;
        private System.Windows.Forms.RadioButton radioButtonMedio;
        private System.Windows.Forms.RadioButton radioMedioM;
        private System.Windows.Forms.RadioButton radioDificil;
        private System.Windows.Forms.Button buttonvolta;
    }
}