﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_Exercicios : Form
    {
        public View_Exercicios()
        {
            InitializeComponent();
        }

        private void radioButtonBasico_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxExercicio.Image = Properties.Resources._1585476055_407149;
        }

        private void radioButtonBasicoM_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxExercicio.Image = Properties.Resources._1585808717_754310;
        }

        private void radioButtonMedio_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxExercicio.Image = Properties.Resources._1587714697_923489;
        }

        private void radioMedioM_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxExercicio.Image = Properties.Resources._1586940885_666586;
        }

        private void radioDificil_CheckedChanged(object sender, EventArgs e)
        {
            pictureBoxExercicio.Image = Properties.Resources._1585039855_131856;
        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void View_Exercicios_Load(object sender, EventArgs e)
        {

        }

        private void buttonvolta_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Exercicio = new View_Exercicios();
            Program.V_Pesquisar.Show();
        }
    }
}
