﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_HistoricoGraficos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.zedGraphControlHistoricoGraficosPeso = new ZedGraph.ZedGraphControl();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.buttonCarregarGraficos = new System.Windows.Forms.Button();
            this.chartPeso = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.checkBoxAtivar3D = new System.Windows.Forms.CheckBox();
            this.timerGrafico = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chartPeso)).BeginInit();
            this.SuspendLayout();
            // 
            // zedGraphControlHistoricoGraficosPeso
            // 
            this.zedGraphControlHistoricoGraficosPeso.Location = new System.Drawing.Point(59, 96);
            this.zedGraphControlHistoricoGraficosPeso.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.zedGraphControlHistoricoGraficosPeso.Name = "zedGraphControlHistoricoGraficosPeso";
            this.zedGraphControlHistoricoGraficosPeso.ScrollGrace = 0D;
            this.zedGraphControlHistoricoGraficosPeso.ScrollMaxX = 0D;
            this.zedGraphControlHistoricoGraficosPeso.ScrollMaxY = 0D;
            this.zedGraphControlHistoricoGraficosPeso.ScrollMaxY2 = 0D;
            this.zedGraphControlHistoricoGraficosPeso.ScrollMinX = 0D;
            this.zedGraphControlHistoricoGraficosPeso.ScrollMinY = 0D;
            this.zedGraphControlHistoricoGraficosPeso.ScrollMinY2 = 0D;
            this.zedGraphControlHistoricoGraficosPeso.Size = new System.Drawing.Size(315, 238);
            this.zedGraphControlHistoricoGraficosPeso.TabIndex = 0;
            this.zedGraphControlHistoricoGraficosPeso.UseExtendedPrintDialog = true;
            this.zedGraphControlHistoricoGraficosPeso.ZoomStepFraction = 1D;
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(13, 409);
            this.buttonVoltar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(96, 30);
            this.buttonVoltar.TabIndex = 2;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // buttonCarregarGraficos
            // 
            this.buttonCarregarGraficos.Location = new System.Drawing.Point(692, 409);
            this.buttonCarregarGraficos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCarregarGraficos.Name = "buttonCarregarGraficos";
            this.buttonCarregarGraficos.Size = new System.Drawing.Size(96, 30);
            this.buttonCarregarGraficos.TabIndex = 3;
            this.buttonCarregarGraficos.Text = "Carregar";
            this.buttonCarregarGraficos.UseVisualStyleBackColor = true;
            this.buttonCarregarGraficos.Click += new System.EventHandler(this.buttonCarregarGraficos_Click);
            // 
            // chartPeso
            // 
            chartArea2.Name = "ChartArea1";
            this.chartPeso.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartPeso.Legends.Add(legend2);
            this.chartPeso.Location = new System.Drawing.Point(400, 96);
            this.chartPeso.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chartPeso.Name = "chartPeso";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartPeso.Series.Add(series2);
            this.chartPeso.Size = new System.Drawing.Size(371, 238);
            this.chartPeso.TabIndex = 4;
            this.chartPeso.Text = "Peso";
            // 
            // checkBoxAtivar3D
            // 
            this.checkBoxAtivar3D.AutoSize = true;
            this.checkBoxAtivar3D.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxAtivar3D.Checked = true;
            this.checkBoxAtivar3D.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAtivar3D.Location = new System.Drawing.Point(677, 337);
            this.checkBoxAtivar3D.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBoxAtivar3D.Name = "checkBoxAtivar3D";
            this.checkBoxAtivar3D.Size = new System.Drawing.Size(88, 21);
            this.checkBoxAtivar3D.TabIndex = 5;
            this.checkBoxAtivar3D.Text = "Ativar 3D";
            this.checkBoxAtivar3D.UseVisualStyleBackColor = false;
            this.checkBoxAtivar3D.CheckedChanged += new System.EventHandler(this.checkBoxAtivar3D_CheckedChanged);
            // 
            // timerGrafico
            // 
            this.timerGrafico.Interval = 500;
            this.timerGrafico.Tick += new System.EventHandler(this.timerGrafico_Tick);
            // 
            // View_HistoricoGraficos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo13;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.checkBoxAtivar3D);
            this.Controls.Add(this.chartPeso);
            this.Controls.Add(this.buttonCarregarGraficos);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.zedGraphControlHistoricoGraficosPeso);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "View_HistoricoGraficos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View_HistoricoGraficos";
            ((System.ComponentModel.ISupportInitialize)(this.chartPeso)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControlHistoricoGraficosPeso;
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.Button buttonCarregarGraficos;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPeso;
        private System.Windows.Forms.CheckBox checkBoxAtivar3D;
        private System.Windows.Forms.Timer timerGrafico;
    }
}