﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_HistoricoGraficos : Form
    {
        //static int Peso = Convert.ToInt32(Program.M_App.Menor.peso);
        static int Altura = Convert.ToInt32(Program.M_App.Menor.altura);
        double[] x = new double[18];
        double[] y = new double[18];
        public View_HistoricoGraficos()
        {
            InitializeComponent();
            chartPeso.Legends.Clear();
            chartPeso.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Range;
        }
        int X= 200;

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_HistoricoGraficos = new View_HistoricoGraficos();
            Program.V_Pesquisar.Show();
        }

        private void buttonCarregarGraficos_Click(object sender, EventArgs e)
        {
            double a=0;
            for (int i = 0; i<18; i++)
            {
                x[i] = i;
                a = x[i] * x[i] * x[i] * x[i] * -0.0018 + x[i] * x[i] * x[i] * 0.0513 - 0.3712 * x[i] * x[i] + 3.5066 * x[i] + 8.8166;
                y[i] = a;
   
            }

            zedGraphControlHistoricoGraficosPeso.GraphPane.AddCurve("Curva", x, y, Color.Orange);
            zedGraphControlHistoricoGraficosPeso.RestoreScale(zedGraphControlHistoricoGraficosPeso.GraphPane);
            zedGraphControlHistoricoGraficosPeso.Refresh();
        }

        private void checkBoxAtivar3D_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxAtivar3D.Checked)
            {
                chartPeso.ChartAreas[0].Area3DStyle.Enable3D = true;
            }
            else
            {
                chartPeso.ChartAreas[0].Area3DStyle.Enable3D = false;
            }
        }

        private void timerGrafico_Tick(object sender, EventArgs e)
        {
            if (chartPeso.Series[0].Points.Count > 5)
            {
                chartPeso.Series[0].Points.RemoveAt(0);
                chartPeso.Update();
            }
            chartPeso.Series[0].Points.AddXY(X, new Random().NextDouble());
        }
    }
}
