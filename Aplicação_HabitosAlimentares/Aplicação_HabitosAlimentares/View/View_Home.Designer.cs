﻿namespace Aplicação_HabitosAlimentares
{
    partial class View_Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_Home));
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonRegistar = new System.Windows.Forms.Button();
            this.buttonTermosCondiçoes = new System.Windows.Forms.Button();
            this.buttonSair = new System.Windows.Forms.Button();
            this.buttonCreditos = new System.Windows.Forms.Button();
            this.buttonJogo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(225, 157);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(260, 33);
            this.buttonLogin.TabIndex = 0;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonRegistar
            // 
            this.buttonRegistar.Location = new System.Drawing.Point(225, 209);
            this.buttonRegistar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonRegistar.Name = "buttonRegistar";
            this.buttonRegistar.Size = new System.Drawing.Size(260, 33);
            this.buttonRegistar.TabIndex = 1;
            this.buttonRegistar.Text = "Registar";
            this.buttonRegistar.UseVisualStyleBackColor = true;
            this.buttonRegistar.Click += new System.EventHandler(this.buttonRegistar_Click);
            // 
            // buttonTermosCondiçoes
            // 
            this.buttonTermosCondiçoes.Location = new System.Drawing.Point(225, 305);
            this.buttonTermosCondiçoes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonTermosCondiçoes.Name = "buttonTermosCondiçoes";
            this.buttonTermosCondiçoes.Size = new System.Drawing.Size(260, 33);
            this.buttonTermosCondiçoes.TabIndex = 2;
            this.buttonTermosCondiçoes.Text = "Termos e Condições";
            this.buttonTermosCondiçoes.UseVisualStyleBackColor = true;
            this.buttonTermosCondiçoes.Click += new System.EventHandler(this.buttonTermosCondiçoes_Click);
            // 
            // buttonSair
            // 
            this.buttonSair.Location = new System.Drawing.Point(259, 401);
            this.buttonSair.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonSair.Name = "buttonSair";
            this.buttonSair.Size = new System.Drawing.Size(199, 33);
            this.buttonSair.TabIndex = 3;
            this.buttonSair.Text = "Sair";
            this.buttonSair.UseVisualStyleBackColor = true;
            this.buttonSair.Click += new System.EventHandler(this.buttonSair_Click);
            // 
            // buttonCreditos
            // 
            this.buttonCreditos.Location = new System.Drawing.Point(225, 353);
            this.buttonCreditos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonCreditos.Name = "buttonCreditos";
            this.buttonCreditos.Size = new System.Drawing.Size(260, 33);
            this.buttonCreditos.TabIndex = 4;
            this.buttonCreditos.Text = "Créditos";
            this.buttonCreditos.UseVisualStyleBackColor = true;
            this.buttonCreditos.Click += new System.EventHandler(this.buttonCreditos_Click);
            // 
            // buttonJogo
            // 
            this.buttonJogo.Location = new System.Drawing.Point(225, 258);
            this.buttonJogo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonJogo.Name = "buttonJogo";
            this.buttonJogo.Size = new System.Drawing.Size(260, 33);
            this.buttonJogo.TabIndex = 5;
            this.buttonJogo.Text = "Jogo";
            this.buttonJogo.UseVisualStyleBackColor = true;
            this.buttonJogo.Click += new System.EventHandler(this.buttonJogo_Click);
            // 
            // View_Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(721, 455);
            this.Controls.Add(this.buttonJogo);
            this.Controls.Add(this.buttonCreditos);
            this.Controls.Add(this.buttonSair);
            this.Controls.Add(this.buttonTermosCondiçoes);
            this.Controls.Add(this.buttonRegistar);
            this.Controls.Add(this.buttonLogin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "View_Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonRegistar;
        private System.Windows.Forms.Button buttonTermosCondiçoes;
        private System.Windows.Forms.Button buttonSair;
        private System.Windows.Forms.Button buttonCreditos;
        private System.Windows.Forms.Button buttonJogo;
    }
}

