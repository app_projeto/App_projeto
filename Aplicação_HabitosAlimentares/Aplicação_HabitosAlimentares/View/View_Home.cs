﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares
{
    public partial class View_Home : Form
    {
        public View_Home()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Login.Show();
        }

        private void buttonRegistar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ao registar-se, concorda com os Termos e Condições", "Obrigatório...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.Hide();
            Program.V_Termos_Condiçoes.Show();
        }

        private void buttonTermosCondiçoes_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ao registar-se, concorda com os Termos e Condições", "Obrigatório...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.Hide();
            Program.V_Termos_Condiçoes.Show();
        }

        private void buttonSair_Click(object sender, EventArgs e)
        {

           // this.Hide();
            MessageBox.Show("Obrigado por usar a aplicação!", "Desconectando...", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.Exit();

        }

        private void buttonCreditos_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Creditos.Show();
        }

        private void buttonJogo_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_JogoPuzzle = new View.View_JogoPuzzle();
            Program.V_JogoPuzzle.Show();
        }
    }
}
