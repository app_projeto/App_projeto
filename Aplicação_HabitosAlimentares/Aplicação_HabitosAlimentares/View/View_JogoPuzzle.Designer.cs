﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_JogoPuzzle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_JogoPuzzle));
            this.groupBoxPuzzle = new System.Windows.Forms.GroupBox();
            this.pictureBoxP1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP6 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP7 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP8 = new System.Windows.Forms.PictureBox();
            this.pictureBoxP9 = new System.Windows.Forms.PictureBox();
            this.labelNumJog = new System.Windows.Forms.Label();
            this.groupBoxImagem = new System.Windows.Forms.GroupBox();
            this.pictureBoxOriginal = new System.Windows.Forms.PictureBox();
            this.labelTempo = new System.Windows.Forms.Label();
            this.buttonShuffle = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonSair = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBoxPuzzle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP9)).BeginInit();
            this.groupBoxImagem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOriginal)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxPuzzle
            // 
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP1);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP2);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP3);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP4);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP5);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP6);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP7);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP8);
            this.groupBoxPuzzle.Controls.Add(this.pictureBoxP9);
            this.groupBoxPuzzle.Location = new System.Drawing.Point(28, 36);
            this.groupBoxPuzzle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxPuzzle.Name = "groupBoxPuzzle";
            this.groupBoxPuzzle.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxPuzzle.Size = new System.Drawing.Size(931, 729);
            this.groupBoxPuzzle.TabIndex = 0;
            this.groupBoxPuzzle.TabStop = false;
            this.groupBoxPuzzle.Text = "Puzzle";
            // 
            // pictureBoxP1
            // 
            this.pictureBoxP1.Location = new System.Drawing.Point(40, 23);
            this.pictureBoxP1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP1.Name = "pictureBoxP1";
            this.pictureBoxP1.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP1.TabIndex = 6;
            this.pictureBoxP1.TabStop = false;
            this.pictureBoxP1.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP2
            // 
            this.pictureBoxP2.Location = new System.Drawing.Point(324, 23);
            this.pictureBoxP2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP2.Name = "pictureBoxP2";
            this.pictureBoxP2.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP2.TabIndex = 9;
            this.pictureBoxP2.TabStop = false;
            this.pictureBoxP2.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP3
            // 
            this.pictureBoxP3.Location = new System.Drawing.Point(608, 23);
            this.pictureBoxP3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP3.Name = "pictureBoxP3";
            this.pictureBoxP3.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP3.TabIndex = 10;
            this.pictureBoxP3.TabStop = false;
            this.pictureBoxP3.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP4
            // 
            this.pictureBoxP4.Location = new System.Drawing.Point(40, 256);
            this.pictureBoxP4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP4.Name = "pictureBoxP4";
            this.pictureBoxP4.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP4.TabIndex = 7;
            this.pictureBoxP4.TabStop = false;
            this.pictureBoxP4.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP5
            // 
            this.pictureBoxP5.Location = new System.Drawing.Point(324, 256);
            this.pictureBoxP5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP5.Name = "pictureBoxP5";
            this.pictureBoxP5.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP5.TabIndex = 11;
            this.pictureBoxP5.TabStop = false;
            this.pictureBoxP5.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP6
            // 
            this.pictureBoxP6.Location = new System.Drawing.Point(608, 256);
            this.pictureBoxP6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP6.Name = "pictureBoxP6";
            this.pictureBoxP6.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP6.TabIndex = 12;
            this.pictureBoxP6.TabStop = false;
            this.pictureBoxP6.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP7
            // 
            this.pictureBoxP7.Location = new System.Drawing.Point(40, 489);
            this.pictureBoxP7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP7.Name = "pictureBoxP7";
            this.pictureBoxP7.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP7.TabIndex = 8;
            this.pictureBoxP7.TabStop = false;
            this.pictureBoxP7.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP8
            // 
            this.pictureBoxP8.Location = new System.Drawing.Point(324, 489);
            this.pictureBoxP8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP8.Name = "pictureBoxP8";
            this.pictureBoxP8.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP8.TabIndex = 13;
            this.pictureBoxP8.TabStop = false;
            this.pictureBoxP8.Click += new System.EventHandler(this.MudarImagem);
            // 
            // pictureBoxP9
            // 
            this.pictureBoxP9.Location = new System.Drawing.Point(608, 489);
            this.pictureBoxP9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxP9.Name = "pictureBoxP9";
            this.pictureBoxP9.Size = new System.Drawing.Size(276, 225);
            this.pictureBoxP9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxP9.TabIndex = 14;
            this.pictureBoxP9.TabStop = false;
            this.pictureBoxP9.Click += new System.EventHandler(this.MudarImagem);
            // 
            // labelNumJog
            // 
            this.labelNumJog.AutoSize = true;
            this.labelNumJog.BackColor = System.Drawing.Color.Transparent;
            this.labelNumJog.Font = new System.Drawing.Font("Perpetua Titling MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumJog.Location = new System.Drawing.Point(1449, 516);
            this.labelNumJog.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNumJog.Name = "labelNumJog";
            this.labelNumJog.Size = new System.Drawing.Size(71, 72);
            this.labelNumJog.TabIndex = 1;
            this.labelNumJog.Text = "0";
            this.labelNumJog.Click += new System.EventHandler(this.labelNumJog_Click);
            // 
            // groupBoxImagem
            // 
            this.groupBoxImagem.Controls.Add(this.pictureBoxOriginal);
            this.groupBoxImagem.Location = new System.Drawing.Point(1180, 171);
            this.groupBoxImagem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxImagem.Name = "groupBoxImagem";
            this.groupBoxImagem.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxImagem.Size = new System.Drawing.Size(331, 249);
            this.groupBoxImagem.TabIndex = 2;
            this.groupBoxImagem.TabStop = false;
            this.groupBoxImagem.Enter += new System.EventHandler(this.groupBoxImagem_Enter);
            // 
            // pictureBoxOriginal
            // 
            this.pictureBoxOriginal.Image = global::Aplicação_HabitosAlimentares.Properties.Resources.Puzzle12;
            this.pictureBoxOriginal.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxOriginal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxOriginal.Name = "pictureBoxOriginal";
            this.pictureBoxOriginal.Size = new System.Drawing.Size(331, 254);
            this.pictureBoxOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOriginal.TabIndex = 15;
            this.pictureBoxOriginal.TabStop = false;
            this.pictureBoxOriginal.Click += new System.EventHandler(this.pictureBoxOriginal_Click);
            // 
            // labelTempo
            // 
            this.labelTempo.AutoSize = true;
            this.labelTempo.BackColor = System.Drawing.Color.Transparent;
            this.labelTempo.Font = new System.Drawing.Font("Perpetua Titling MT", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTempo.Location = new System.Drawing.Point(983, 516);
            this.labelTempo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTempo.Name = "labelTempo";
            this.labelTempo.Size = new System.Drawing.Size(304, 72);
            this.labelTempo.TabIndex = 3;
            this.labelTempo.Text = "00:00:00";
            // 
            // buttonShuffle
            // 
            this.buttonShuffle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonShuffle.Location = new System.Drawing.Point(1092, 614);
            this.buttonShuffle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonShuffle.Name = "buttonShuffle";
            this.buttonShuffle.Size = new System.Drawing.Size(141, 49);
            this.buttonShuffle.TabIndex = 4;
            this.buttonShuffle.Text = "Baralhar";
            this.buttonShuffle.UseVisualStyleBackColor = true;
            this.buttonShuffle.Click += new System.EventHandler(this.buttonShuffle_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStop.Location = new System.Drawing.Point(1253, 614);
            this.buttonStop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(141, 49);
            this.buttonStop.TabIndex = 7;
            this.buttonStop.Text = "Parar";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonSair
            // 
            this.buttonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSair.Location = new System.Drawing.Point(1403, 614);
            this.buttonSair.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonSair.Name = "buttonSair";
            this.buttonSair.Size = new System.Drawing.Size(250, 49);
            this.buttonSair.TabIndex = 8;
            this.buttonSair.Text = "Sair e fazer logout";
            this.buttonSair.UseVisualStyleBackColor = true;
            this.buttonSair.Click += new System.EventHandler(this.buttonSair_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 900;
            this.timer1.Tick += new System.EventHandler(this.tempopassado);
            // 
            // View_JogoPuzzle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo12_1;
            this.ClientSize = new System.Drawing.Size(1731, 832);
            this.Controls.Add(this.buttonSair);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonShuffle);
            this.Controls.Add(this.labelTempo);
            this.Controls.Add(this.groupBoxImagem);
            this.Controls.Add(this.labelNumJog);
            this.Controls.Add(this.groupBoxPuzzle);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "View_JogoPuzzle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JogoPuzzle";
            this.Load += new System.EventHandler(this.View_JogoPuzzle_Load);
            this.groupBoxPuzzle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxP9)).EndInit();
            this.groupBoxImagem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOriginal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxPuzzle;
        private System.Windows.Forms.PictureBox pictureBoxP1;
        private System.Windows.Forms.PictureBox pictureBoxP2;
        private System.Windows.Forms.PictureBox pictureBoxP3;
        private System.Windows.Forms.PictureBox pictureBoxP4;
        private System.Windows.Forms.PictureBox pictureBoxP5;
        private System.Windows.Forms.PictureBox pictureBoxP6;
        private System.Windows.Forms.PictureBox pictureBoxP7;
        private System.Windows.Forms.PictureBox pictureBoxP8;
        private System.Windows.Forms.PictureBox pictureBoxP9;
        private System.Windows.Forms.Label labelNumJog;
        private System.Windows.Forms.GroupBox groupBoxImagem;
        private System.Windows.Forms.Label labelTempo;
        private System.Windows.Forms.PictureBox pictureBoxOriginal;
        private System.Windows.Forms.Button buttonShuffle;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonSair;
        private System.Windows.Forms.Timer timer1;
    }
}