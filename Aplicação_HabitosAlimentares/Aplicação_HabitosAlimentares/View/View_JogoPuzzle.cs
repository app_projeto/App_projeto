﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_JogoPuzzle : Form
    {
        int sliceindex, inmoves = 0;
        List<Bitmap> PictureListOriginal = new List<Bitmap>();
        System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
        public View_JogoPuzzle()
        {
            InitializeComponent();
            PictureListOriginal.AddRange(new Bitmap[] { Properties.Resources._1, Properties.Resources._2, Properties.Resources._3, Properties.Resources._4, Properties.Resources._5,
                Properties.Resources._6, Properties.Resources._7, Properties.Resources._8,Properties.Resources._9,Properties.Resources._null });
            labelNumJog.Text += inmoves;
            labelTempo.Text = "00:00:00";
        }
        bool Verseganhou()
        {
            int i;
            for (i = 0; i < 8; i++)
            {
                if ((groupBoxPuzzle.Controls[i] as PictureBox).Image != PictureListOriginal[i]) break;
            }
            if (i == 8) return true;
            else return false;
        }
        void Shuffle()
        {
            do
            {
                int j;
                List<int> Index = new List<int>(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9});
                Random r = new Random();
                for (int i = 0; i < 9; i++)
                {
                    Index.Remove((j = Index[r.Next(0, Index.Count)]));
                    ((PictureBox)groupBoxPuzzle.Controls[i]).Image = PictureListOriginal[j];
                    if (j == 9) sliceindex = i;
                }
            } while (Verseganhou());
        }

        private void buttonShuffle_Click(object sender, EventArgs e)
        {
            DialogResult SimouNao = new DialogResult();
            if(labelTempo.Text != "00:00:00")
            {
                SimouNao = MessageBox.Show("Tens a certeza que queres recomeçar?", "Puzzle", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }if(SimouNao == DialogResult.Yes || labelTempo.Text == "00:00:00")
            {
                Shuffle();
                timer.Reset();
                labelTempo.Text = "00:00:00";
                inmoves = 0;
                labelNumJog.Text = "0";
               // Program.V_JogoPuzzle = new View_JogoPuzzle();

            }
        }
        private void MudarImagem(object sender, EventArgs e)
        {
            if (labelTempo.Text == "00:00:00") timer.Start();
            int inPictureBoxIndex = groupBoxPuzzle.Controls.IndexOf(sender as Control);
            if (sliceindex != inPictureBoxIndex)
            {
                List<int> FourBrothers = new List<int>(new int[] { ((inPictureBoxIndex % 3 == 0) ? - 1: inPictureBoxIndex - 1), inPictureBoxIndex - 3, (inPictureBoxIndex % 3 == 2) ? -1 : inPictureBoxIndex + 1, inPictureBoxIndex + 3 });
                if (FourBrothers.Contains(sliceindex))
                {
                    ((PictureBox)groupBoxPuzzle.Controls[sliceindex]).Image = ((PictureBox)groupBoxPuzzle.Controls[inPictureBoxIndex]).Image;
                    ((PictureBox)groupBoxPuzzle.Controls[inPictureBoxIndex]).Image = PictureListOriginal[9];
                    sliceindex = inPictureBoxIndex;
                    labelNumJog.Text = "" + (++inmoves);
                    if (Verseganhou())
                    {
                        timer.Stop();
                        (groupBoxPuzzle.Controls[8] as PictureBox).Image = PictureListOriginal[8];
                        MessageBox.Show("Parabéns, completas-te o puzzle!\n Esta é uma refeição saudavel com 800 kcal.\n Tempo : " + timer.Elapsed.ToString().Remove(8) + "Numero de jogadas : " + inmoves, "Puzzle");
                        inmoves = 0;
                        labelNumJog.Text = "0";
                        labelTempo.Text = "00:00:00";
                        timer.Reset();
                        Shuffle();
                    }
                }
            } 
        }
            private void tempopassado(object sender, EventArgs e)
            {
            if (timer.Elapsed.ToString() != "00:00:00") labelTempo.Text = timer.Elapsed.ToString().Remove(8);
            if (timer.Elapsed.ToString() == "00:00:00") buttonStop.Enabled = false;
            else
                buttonStop.Enabled = true;
            if(timer.Elapsed.Minutes.ToString() == "10")
            {
                inmoves = 0;
                labelNumJog.Text = "0";
                labelTempo.Text = "00:00:00";
                timer.Reset();
                buttonStop.Enabled = false;
                MessageBox.Show("Acabou o tempo!", "Puzzle");
                Shuffle();
            }
            }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (buttonStop.Text == "Parar")
            {
                timer.Stop();
                groupBoxPuzzle.Visible = false;
                buttonStop.Text = "Continuar";
            }
            else
            {
                timer.Start();
                groupBoxPuzzle.Visible = true;
                buttonStop.Text = "Parar";
            }
        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            this.Hide();
            timer.Stop();
            Program.V_Home.Show();
        }

        private void groupBoxImagem_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBoxOriginal_Click(object sender, EventArgs e)
        {

        }

        private void labelNumJog_Click(object sender, EventArgs e)
        {

        }

        private void View_JogoPuzzle_Load(object sender, EventArgs e)
        {
            Shuffle();
        }
    }
}
