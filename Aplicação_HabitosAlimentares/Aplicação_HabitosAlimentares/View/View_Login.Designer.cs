﻿namespace Aplicação_HabitosAlimentares
{
    partial class View_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_Login));
            this.buttonSair = new System.Windows.Forms.Button();
            this.buttonEntrar = new System.Windows.Forms.Button();
            this.buttonRegistar = new System.Windows.Forms.Button();
            this.textBoxEntrar = new System.Windows.Forms.TextBox();
            this.textBoxSenha = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxPasswordLogin = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // buttonSair
            // 
            this.buttonSair.Location = new System.Drawing.Point(12, 406);
            this.buttonSair.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSair.Name = "buttonSair";
            this.buttonSair.Size = new System.Drawing.Size(107, 32);
            this.buttonSair.TabIndex = 1;
            this.buttonSair.Text = "Voltar";
            this.buttonSair.UseVisualStyleBackColor = true;
            this.buttonSair.Click += new System.EventHandler(this.buttonSair_Click);
            // 
            // buttonEntrar
            // 
            this.buttonEntrar.Location = new System.Drawing.Point(399, 267);
            this.buttonEntrar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEntrar.Name = "buttonEntrar";
            this.buttonEntrar.Size = new System.Drawing.Size(112, 39);
            this.buttonEntrar.TabIndex = 2;
            this.buttonEntrar.Text = "Entrar";
            this.buttonEntrar.UseVisualStyleBackColor = true;
            this.buttonEntrar.Click += new System.EventHandler(this.buttonEntrar_Click);
            // 
            // buttonRegistar
            // 
            this.buttonRegistar.Location = new System.Drawing.Point(285, 267);
            this.buttonRegistar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonRegistar.Name = "buttonRegistar";
            this.buttonRegistar.Size = new System.Drawing.Size(108, 39);
            this.buttonRegistar.TabIndex = 3;
            this.buttonRegistar.Text = "Registar";
            this.buttonRegistar.UseVisualStyleBackColor = true;
            this.buttonRegistar.Click += new System.EventHandler(this.buttonRegistar_Click);
            // 
            // textBoxEntrar
            // 
            this.textBoxEntrar.AcceptsTab = true;
            this.textBoxEntrar.Location = new System.Drawing.Point(285, 182);
            this.textBoxEntrar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEntrar.Name = "textBoxEntrar";
            this.textBoxEntrar.Size = new System.Drawing.Size(224, 22);
            this.textBoxEntrar.TabIndex = 6;
            this.textBoxEntrar.Text = "Email";
            this.textBoxEntrar.Click += new System.EventHandler(this.textBoxEntrar_Click);
            this.textBoxEntrar.TextChanged += new System.EventHandler(this.textBoxEntrar_TextChanged);
            // 
            // textBoxSenha
            // 
            this.textBoxSenha.AcceptsTab = true;
            this.textBoxSenha.Location = new System.Drawing.Point(285, 224);
            this.textBoxSenha.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxSenha.Name = "textBoxSenha";
            this.textBoxSenha.Size = new System.Drawing.Size(224, 22);
            this.textBoxSenha.TabIndex = 7;
            this.textBoxSenha.Text = "Password";
            this.textBoxSenha.Click += new System.EventHandler(this.textBoxSenha_Click);
            this.textBoxSenha.TextChanged += new System.EventHandler(this.textBoxSenha_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(531, 423);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(258, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nunca revele os seus dados a ninguém";
            // 
            // checkBoxPasswordLogin
            // 
            this.checkBoxPasswordLogin.AutoSize = true;
            this.checkBoxPasswordLogin.Checked = true;
            this.checkBoxPasswordLogin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPasswordLogin.Location = new System.Drawing.Point(515, 228);
            this.checkBoxPasswordLogin.Name = "checkBoxPasswordLogin";
            this.checkBoxPasswordLogin.Size = new System.Drawing.Size(18, 17);
            this.checkBoxPasswordLogin.TabIndex = 9;
            this.checkBoxPasswordLogin.UseVisualStyleBackColor = true;
            this.checkBoxPasswordLogin.CheckedChanged += new System.EventHandler(this.checkBoxPasswordLogin_CheckedChanged);
            // 
            // View_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.checkBoxPasswordLogin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxSenha);
            this.Controls.Add(this.textBoxEntrar);
            this.Controls.Add(this.buttonRegistar);
            this.Controls.Add(this.buttonEntrar);
            this.Controls.Add(this.buttonSair);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "View_Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.View_Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonSair;
        private System.Windows.Forms.Button buttonEntrar;
        private System.Windows.Forms.Button buttonRegistar;
        private System.Windows.Forms.TextBox textBoxEntrar;
        private System.Windows.Forms.TextBox textBoxSenha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxPasswordLogin;
    }
}