﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Aplicação_HabitosAlimentares
{
    public partial class View_Login : Form
    {
        public event MetodosComDuasStrings Login;

        public int teste1 { get; private set; }

        public View_Login()
        {
            InitializeComponent();
            Program.M_App.LoginSucedido += M_App_LoginSucedido;
            Program.M_App.LoginFalhado += M_App_LoginFalhado;

        }

        private void M_App_LoginFalhado()
        {
            MessageBox.Show("O username ou a password inseridos estão incorretos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            textBoxEntrar.Text = string.Empty;
            textBoxSenha.Text = string.Empty;

        }

        private void M_App_LoginSucedido()
        {
            MessageBox.Show("Login efetuado com Sucesso", "Login Efetuado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            Program.V_Menu.Show();

            teste1 = 1;
            textBoxEntrar.Text = string.Empty;
            textBoxSenha.Text = string.Empty;

        }

        private void buttonSair_Click(object sender, EventArgs e)
        {
            this.Hide();
            textBoxEntrar.Text = "Email";
            textBoxSenha.Text = "Password";
            textBoxSenha.UseSystemPasswordChar = false;
            Program.V_Home.Show();
        }

        private void buttonRegistar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ao registar-se, concorda com os Termos e Condições", "Obrigatório...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.Hide();
            Program.V_Registar.Show();
        }

        private void buttonEntrar_Click(object sender, EventArgs e)
        {
            if (Login != null) 
                Login(textBoxEntrar.Text, textBoxSenha.Text);
        }

        private void View_Login_Load(object sender, EventArgs e)
        {
            
        }

        private void textBoxEntrar_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxEntrar_Click(object sender, EventArgs e)
        {
            if (textBoxEntrar.Text == "")
            {
                textBoxEntrar.Text = "Email";
                return;
            }
            if (textBoxEntrar.Text == "Email")
            {
                textBoxEntrar.Clear();
                return;
            }
        }

        private void textBoxSenha_Click(object sender, EventArgs e)
        {
            if (textBoxSenha.Text == "")
            {
                textBoxSenha.UseSystemPasswordChar = false;
                textBoxSenha.Text = "Password";
                return;
            }
            if (textBoxSenha.Text == "Password")
            {
                textBoxSenha.Clear();
                textBoxSenha.UseSystemPasswordChar = true;
                return;
            }
            
        }

        private void textBoxSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxPasswordLogin_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPasswordLogin.Checked)
            {
                textBoxSenha.UseSystemPasswordChar = true;


            }
            else
            {
                textBoxSenha.UseSystemPasswordChar = false;
            }
        }
    }
}
