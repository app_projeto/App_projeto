﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_MenuPrincipal));
            this.buttonLogout = new System.Windows.Forms.Button();
            this.labelNome = new System.Windows.Forms.Label();
            this.pictureBoxPerfil = new System.Windows.Forms.PictureBox();
            this.labelCalcularPesoIdeal = new System.Windows.Forms.Label();
            this.labelConsultarDadosCrianca = new System.Windows.Forms.Label();
            this.labelAlterarDadosCrianca = new System.Windows.Forms.Label();
            this.labelAdicionarCrianca = new System.Windows.Forms.Label();
            this.labelAlterarPerfil = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPerfil)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(16, 317);
            this.buttonLogout.Margin = new System.Windows.Forms.Padding(2);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(87, 39);
            this.buttonLogout.TabIndex = 1;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.BackColor = System.Drawing.Color.Transparent;
            this.labelNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNome.Location = new System.Drawing.Point(219, 173);
            this.labelNome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(47, 15);
            this.labelNome.TabIndex = 7;
            this.labelNome.Text = "label2";
            this.labelNome.Click += new System.EventHandler(this.labelNome_Click);
            // 
            // pictureBoxPerfil
            // 
            this.pictureBoxPerfil.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxPerfil.Location = new System.Drawing.Point(0, 3);
            this.pictureBoxPerfil.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxPerfil.Name = "pictureBoxPerfil";
            this.pictureBoxPerfil.Size = new System.Drawing.Size(86, 77);
            this.pictureBoxPerfil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPerfil.TabIndex = 8;
            this.pictureBoxPerfil.TabStop = false;
            this.pictureBoxPerfil.Click += new System.EventHandler(this.pictureBoxPerfil_Click_1);
            // 
            // labelCalcularPesoIdeal
            // 
            this.labelCalcularPesoIdeal.BackColor = System.Drawing.Color.Transparent;
            this.labelCalcularPesoIdeal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelCalcularPesoIdeal.Location = new System.Drawing.Point(474, 145);
            this.labelCalcularPesoIdeal.Name = "labelCalcularPesoIdeal";
            this.labelCalcularPesoIdeal.Size = new System.Drawing.Size(128, 71);
            this.labelCalcularPesoIdeal.TabIndex = 10;
            this.labelCalcularPesoIdeal.Click += new System.EventHandler(this.labelCalcularPesoIdeal_Click);
            // 
            // labelConsultarDadosCrianca
            // 
            this.labelConsultarDadosCrianca.BackColor = System.Drawing.Color.Transparent;
            this.labelConsultarDadosCrianca.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelConsultarDadosCrianca.Location = new System.Drawing.Point(474, 288);
            this.labelConsultarDadosCrianca.Name = "labelConsultarDadosCrianca";
            this.labelConsultarDadosCrianca.Size = new System.Drawing.Size(128, 78);
            this.labelConsultarDadosCrianca.TabIndex = 11;
            this.labelConsultarDadosCrianca.Click += new System.EventHandler(this.labelConsultarDadosCrianca_Click);
            // 
            // labelAlterarDadosCrianca
            // 
            this.labelAlterarDadosCrianca.BackColor = System.Drawing.Color.Transparent;
            this.labelAlterarDadosCrianca.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelAlterarDadosCrianca.Location = new System.Drawing.Point(474, 73);
            this.labelAlterarDadosCrianca.Name = "labelAlterarDadosCrianca";
            this.labelAlterarDadosCrianca.Size = new System.Drawing.Size(128, 72);
            this.labelAlterarDadosCrianca.TabIndex = 12;
            this.labelAlterarDadosCrianca.Click += new System.EventHandler(this.labelAlterarDadosCrianca_Click);
            // 
            // labelAdicionarCrianca
            // 
            this.labelAdicionarCrianca.BackColor = System.Drawing.Color.Transparent;
            this.labelAdicionarCrianca.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelAdicionarCrianca.Location = new System.Drawing.Point(474, 216);
            this.labelAdicionarCrianca.Name = "labelAdicionarCrianca";
            this.labelAdicionarCrianca.Size = new System.Drawing.Size(128, 72);
            this.labelAdicionarCrianca.TabIndex = 13;
            this.labelAdicionarCrianca.Click += new System.EventHandler(this.labelAdicionarCrianca_Click);
            // 
            // labelAlterarPerfil
            // 
            this.labelAlterarPerfil.BackColor = System.Drawing.Color.Transparent;
            this.labelAlterarPerfil.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelAlterarPerfil.Location = new System.Drawing.Point(474, 3);
            this.labelAlterarPerfil.Name = "labelAlterarPerfil";
            this.labelAlterarPerfil.Size = new System.Drawing.Size(128, 70);
            this.labelAlterarPerfil.TabIndex = 14;
            this.labelAlterarPerfil.Click += new System.EventHandler(this.labelAlterarPerfil_Click);
            // 
            // View_MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo4_2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.labelAlterarPerfil);
            this.Controls.Add(this.labelAdicionarCrianca);
            this.Controls.Add(this.labelAlterarDadosCrianca);
            this.Controls.Add(this.labelConsultarDadosCrianca);
            this.Controls.Add(this.labelCalcularPesoIdeal);
            this.Controls.Add(this.pictureBoxPerfil);
            this.Controls.Add(this.labelNome);
            this.Controls.Add(this.buttonLogout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_MenuPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuPrincipal";
            this.Load += new System.EventHandler(this.View_MenuPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPerfil)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.PictureBox pictureBoxPerfil;
        private System.Windows.Forms.Label labelCalcularPesoIdeal;
        private System.Windows.Forms.Label labelConsultarDadosCrianca;
        private System.Windows.Forms.Label labelAlterarDadosCrianca;
        private System.Windows.Forms.Label labelAdicionarCrianca;
        private System.Windows.Forms.Label labelAlterarPerfil;
    }
}