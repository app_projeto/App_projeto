﻿using Aplicação_HabitosAlimentares.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_MenuPrincipal : Form
    {
        public event MetodosSemParametros PreencherPerfilUtilizador;
        public string escolhaPerfil { get; private set; }

        public string adicionarCriança { get; private set; }
        public View_MenuPrincipal()
        {
            InitializeComponent();
            Program.M_App.Preencher_Utilizador += M_App_Preencher_Utilizador;
            
                
        }

        private void M_App_Preencher_Utilizador()
        {
            //labelNome.Text = Program.M_App.NomePais();
           labelNome.Text = Program.M_App.Pais.Nome;
           pictureBoxPerfil.Image = Image.FromFile("Imagens\\" + Program.M_App.Pais.Localizacao_imagem);
           

        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Obrigado por usar a aplicação!", "Desconectando...", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.Exit();
        }


        private void buttonAlterarPerfil_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonAdicionarCriança_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonAlterarDadosCriança_Click(object sender, EventArgs e)
        {

        }

        private void buttonConsultarDadosCriança_Click(object sender, EventArgs e)
        {
          
        }

   

        private void labelNome_Click(object sender, EventArgs e)
        {
        }

        private void View_MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void pictureBoxPerfil_Click(object sender, EventArgs e)
        {

        }

        private void pictureBoxPerfil_Click_1(object sender, EventArgs e)
        {

        }

        private void labelCalcularPesoIdeal_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_CalcularPesoIdeal.Show();
        }

        private void labelConsultarDadosCrianca_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_FaQ.Show();

        }

        private void labelAlterarDadosCrianca_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Refresh();
            Program.V_AltCriança.Show();
        }

        private void labelAdicionarCrianca_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Email_suporte.Show();
        }

        private void labelAlterarPerfil_Click(object sender, EventArgs e)
        {
            escolhaPerfil = "Alterar";
            this.Hide();
            Program.V_AlterarPerfil = new View_AlterarPerfil();
            Program.V_AlterarPerfil.Show();
        }

        //private void buttonEnviarEmail_Click(object sender, EventArgs e)
        //{
        //    this.Hide();
        //    Program.V_Email.Show();
        //}

        private void buttonEmail_Click(object sender, EventArgs e)
        {
      
        }
    }
}
