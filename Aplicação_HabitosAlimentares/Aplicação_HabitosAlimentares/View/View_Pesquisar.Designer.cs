﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_Pesquisar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_Pesquisar));
            this.labelNomeCrianca = new System.Windows.Forms.Label();
            this.labelIdade = new System.Windows.Forms.Label();
            this.labelPeso = new System.Windows.Forms.Label();
            this.labelPesoIdeal = new System.Windows.Forms.Label();
            this.labelAltura = new System.Windows.Forms.Label();
            this.pictureBoxCriancaCarregar = new System.Windows.Forms.PictureBox();
            this.labelAlterarDados = new System.Windows.Forms.Label();
            this.labelPlanoDeAlimentacao = new System.Windows.Forms.Label();
            this.labelPlanoDeExercicio = new System.Windows.Forms.Label();
            this.labelHistoricoGraficos = new System.Windows.Forms.Label();
            this.labelVoltar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCriancaCarregar)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNomeCrianca
            // 
            this.labelNomeCrianca.AutoSize = true;
            this.labelNomeCrianca.BackColor = System.Drawing.Color.Transparent;
            this.labelNomeCrianca.Location = new System.Drawing.Point(162, 304);
            this.labelNomeCrianca.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNomeCrianca.Name = "labelNomeCrianca";
            this.labelNomeCrianca.Size = new System.Drawing.Size(35, 13);
            this.labelNomeCrianca.TabIndex = 11;
            this.labelNomeCrianca.Text = "label6";
            // 
            // labelIdade
            // 
            this.labelIdade.AutoSize = true;
            this.labelIdade.BackColor = System.Drawing.Color.Transparent;
            this.labelIdade.Location = new System.Drawing.Point(162, 320);
            this.labelIdade.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIdade.Name = "labelIdade";
            this.labelIdade.Size = new System.Drawing.Size(35, 13);
            this.labelIdade.TabIndex = 12;
            this.labelIdade.Text = "label7";
            // 
            // labelPeso
            // 
            this.labelPeso.AutoSize = true;
            this.labelPeso.BackColor = System.Drawing.Color.Transparent;
            this.labelPeso.Location = new System.Drawing.Point(162, 338);
            this.labelPeso.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPeso.Name = "labelPeso";
            this.labelPeso.Size = new System.Drawing.Size(35, 13);
            this.labelPeso.TabIndex = 13;
            this.labelPeso.Text = "label8";
            // 
            // labelPesoIdeal
            // 
            this.labelPesoIdeal.AutoSize = true;
            this.labelPesoIdeal.BackColor = System.Drawing.Color.Transparent;
            this.labelPesoIdeal.Location = new System.Drawing.Point(162, 355);
            this.labelPesoIdeal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPesoIdeal.Name = "labelPesoIdeal";
            this.labelPesoIdeal.Size = new System.Drawing.Size(35, 13);
            this.labelPesoIdeal.TabIndex = 14;
            this.labelPesoIdeal.Text = "label9";
            // 
            // labelAltura
            // 
            this.labelAltura.AutoSize = true;
            this.labelAltura.BackColor = System.Drawing.Color.Transparent;
            this.labelAltura.Location = new System.Drawing.Point(162, 372);
            this.labelAltura.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelAltura.Name = "labelAltura";
            this.labelAltura.Size = new System.Drawing.Size(41, 13);
            this.labelAltura.TabIndex = 15;
            this.labelAltura.Text = "label10";
            // 
            // pictureBoxCriancaCarregar
            // 
            this.pictureBoxCriancaCarregar.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxCriancaCarregar.Location = new System.Drawing.Point(11, 11);
            this.pictureBoxCriancaCarregar.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxCriancaCarregar.Name = "pictureBoxCriancaCarregar";
            this.pictureBoxCriancaCarregar.Size = new System.Drawing.Size(147, 125);
            this.pictureBoxCriancaCarregar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCriancaCarregar.TabIndex = 5;
            this.pictureBoxCriancaCarregar.TabStop = false;
            // 
            // labelAlterarDados
            // 
            this.labelAlterarDados.BackColor = System.Drawing.Color.Transparent;
            this.labelAlterarDados.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelAlterarDados.Location = new System.Drawing.Point(467, -2);
            this.labelAlterarDados.Name = "labelAlterarDados";
            this.labelAlterarDados.Size = new System.Drawing.Size(184, 88);
            this.labelAlterarDados.TabIndex = 16;
            this.labelAlterarDados.Click += new System.EventHandler(this.labelAlterarDados_Click);
            // 
            // labelPlanoDeAlimentacao
            // 
            this.labelPlanoDeAlimentacao.BackColor = System.Drawing.Color.Transparent;
            this.labelPlanoDeAlimentacao.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelPlanoDeAlimentacao.Location = new System.Drawing.Point(470, 88);
            this.labelPlanoDeAlimentacao.Name = "labelPlanoDeAlimentacao";
            this.labelPlanoDeAlimentacao.Size = new System.Drawing.Size(181, 89);
            this.labelPlanoDeAlimentacao.TabIndex = 17;
            this.labelPlanoDeAlimentacao.Click += new System.EventHandler(this.labelPlanoDeAlimentacao_Click);
            // 
            // labelPlanoDeExercicio
            // 
            this.labelPlanoDeExercicio.BackColor = System.Drawing.Color.Transparent;
            this.labelPlanoDeExercicio.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelPlanoDeExercicio.Location = new System.Drawing.Point(470, 177);
            this.labelPlanoDeExercicio.Name = "labelPlanoDeExercicio";
            this.labelPlanoDeExercicio.Size = new System.Drawing.Size(181, 91);
            this.labelPlanoDeExercicio.TabIndex = 18;
            this.labelPlanoDeExercicio.Click += new System.EventHandler(this.labelPlanoDeExercicio_Click);
            // 
            // labelHistoricoGraficos
            // 
            this.labelHistoricoGraficos.BackColor = System.Drawing.Color.Transparent;
            this.labelHistoricoGraficos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelHistoricoGraficos.Location = new System.Drawing.Point(470, 268);
            this.labelHistoricoGraficos.Name = "labelHistoricoGraficos";
            this.labelHistoricoGraficos.Size = new System.Drawing.Size(181, 100);
            this.labelHistoricoGraficos.TabIndex = 19;
            this.labelHistoricoGraficos.Click += new System.EventHandler(this.labelHistoricoGraficos_Click);
            // 
            // labelVoltar
            // 
            this.labelVoltar.BackColor = System.Drawing.Color.Transparent;
            this.labelVoltar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelVoltar.Location = new System.Drawing.Point(473, 372);
            this.labelVoltar.Name = "labelVoltar";
            this.labelVoltar.Size = new System.Drawing.Size(178, 40);
            this.labelVoltar.TabIndex = 20;
            this.labelVoltar.Click += new System.EventHandler(this.labelVoltar_Click);
            // 
            // View_Pesquisar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo9_2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(651, 414);
            this.Controls.Add(this.labelVoltar);
            this.Controls.Add(this.labelHistoricoGraficos);
            this.Controls.Add(this.labelPlanoDeExercicio);
            this.Controls.Add(this.labelPlanoDeAlimentacao);
            this.Controls.Add(this.labelAlterarDados);
            this.Controls.Add(this.labelAltura);
            this.Controls.Add(this.labelPesoIdeal);
            this.Controls.Add(this.labelPeso);
            this.Controls.Add(this.labelIdade);
            this.Controls.Add(this.labelNomeCrianca);
            this.Controls.Add(this.pictureBoxCriancaCarregar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_Pesquisar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Criança";
            this.Load += new System.EventHandler(this.View_Pesquisar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCriancaCarregar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxCriancaCarregar;
        private System.Windows.Forms.Label labelNomeCrianca;
        private System.Windows.Forms.Label labelIdade;
        private System.Windows.Forms.Label labelPeso;
        private System.Windows.Forms.Label labelPesoIdeal;
        private System.Windows.Forms.Label labelAltura;
        private System.Windows.Forms.Label labelAlterarDados;
        private System.Windows.Forms.Label labelPlanoDeAlimentacao;
        private System.Windows.Forms.Label labelPlanoDeExercicio;
        private System.Windows.Forms.Label labelHistoricoGraficos;
        private System.Windows.Forms.Label labelVoltar;
    }
}