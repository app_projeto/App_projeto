﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_Pesquisar : Form
    {
        public event MetodosSemParametros PreencherPerfilCrianca;
        public View_Pesquisar()
        {
            InitializeComponent();
            Program.M_App.Preencher_Crianca += M_App_Preencher_Crianca;
        }

        private void M_App_Preencher_Crianca(string nomecrianca)
        {
            //labelNomeCrianca.Text = Program.M_App.Menor.nome;
            //pictureBoxCriancaCarregar.Image = Image.FromFile("Imagens\\" + Program.M_App.Menor.foto);
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
        }

        private void View_Pesquisar_Load(object sender, EventArgs e)
        {
            double peso_ideal = 0;
            int idade = Program.M_App.Menor.idade;
            float altura1 = Program.M_App.Menor.altura;
            labelNomeCrianca.Text = Program.M_App.Menor.nome;
            labelAltura.Text = Program.M_App.Menor.altura.ToString() + " Metros";
            labelIdade.Text = Program.M_App.Menor.idade.ToString();
            labelPeso.Text = Program.M_App.Menor.peso.ToString() + " Kg";
            pictureBoxCriancaCarregar.Image = Image.FromFile("Imagens_Crianças\\" + Program.M_App.Menor.foto);

            if (idade <= 2)
            {
                peso_ideal = 18.6 * (altura1 * altura1);
            }
            if (idade > 2 && idade <= 4)
            {
                peso_ideal = 19.3 * (altura1 * altura1);
            }
            if (idade > 4 && idade <= 6)
            {
                peso_ideal = 19.7 * (altura1 * altura1);
            }
            if (idade > 6 && idade <= 8)
            {
                peso_ideal = 20.3 * (altura1 * altura1);
            }
            if (idade > 8 && idade <= 10)
            {
                peso_ideal = 20.8 * (altura1 * altura1);
            }
            if (idade > 10 && idade <= 12)
            {
                peso_ideal = 21.4 * (altura1 * altura1);
            }
            if (idade > 12 && idade <= 14)
            {
                peso_ideal = 21.9 * (altura1 * altura1);
            }
            if (idade > 14 && idade <= 16)
            {
                peso_ideal = 22.3 * (altura1 * altura1);
            }
            if (idade > 16)
            {
                peso_ideal = 23 * (altura1 * altura1);
            }
            peso_ideal = Convert.ToInt32(peso_ideal);
            labelPesoIdeal.Text = peso_ideal.ToString()+ " Kg";
            //else
            //{
            //    if (idade <= 2)
            //    {
            //        peso_ideal = 18.3 * (altura1 * altura1);
            //    }
            //    if (idade > 2 && idade <= 4)
            //    {
            //        peso_ideal = 19 * (altura1 * altura1);
            //    }
            //    if (idade > 4 && idade <= 6)
            //    {
            //        peso_ideal = 19.4 * (altura1 * altura1);
            //    }
            //    if (idade > 6 && idade <= 8)
            //    {
            //        peso_ideal = 20 * (altura1 * altura1);
            //    }
            //    if (idade > 8 && idade <= 10)
            //    {
            //        peso_ideal = 20.5 * (altura1 * altura1);
            //    }
            //    if (idade > 10 && idade <= 12)
            //    {
            //        peso_ideal = 21.1 * (altura1 * altura1);
            //    }
            //    if (idade > 12 && idade <= 14)
            //    {
            //        peso_ideal = 21.6 * (altura1 * altura1);
            //    }
            //    if (idade > 14 && idade <= 16)
            //    {
            //        peso_ideal = 22 * (altura1 * altura1);
            //    }
            //    if (idade > 16)
            //    {
            //        peso_ideal = 22.7 * (altura1 * altura1);
            //    }
            //}
        }

        private void buttonPlanoExercicioCrianca_Click(object sender, EventArgs e)
        {
          
        }

        private void buttonHistoricoGraficos_Click(object sender, EventArgs e)
        {
            
        }

        private void labelVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Pesquisar = new View_Pesquisar();
            Program.V_AltCriança.Show();
        }

        private void labelHistoricoGraficos_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_HistoricoGraficos.Show();
        }

        private void labelPlanoDeExercicio_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Exercicio.Show();
        }

        private void labelPlanoDeAlimentacao_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_PlanoDeAlimentacao.Show();
        }

        private void labelAlterarDados_Click(object sender, EventArgs e)
        {

        }
    }
}
