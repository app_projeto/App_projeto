﻿namespace Aplicação_HabitosAlimentares.View
{
    partial class View_Calorias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_Calorias));
            this.textTomate = new System.Windows.Forms.TextBox();
            this.textBoxAlface = new System.Windows.Forms.TextBox();
            this.textBoxBatata = new System.Windows.Forms.TextBox();
            this.textBoxPepino = new System.Windows.Forms.TextBox();
            this.textBoxpimento = new System.Windows.Forms.TextBox();
            this.textBoxespinafre = new System.Windows.Forms.TextBox();
            this.textBoxcenoura = new System.Windows.Forms.TextBox();
            this.textBoxbroculos = new System.Windows.Forms.TextBox();
            this.textBoxervilhas = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.labelcalorias = new System.Windows.Forms.Label();
            this.textBoxArroz = new System.Windows.Forms.TextBox();
            this.textBoxbife = new System.Windows.Forms.TextBox();
            this.textBoxperu = new System.Windows.Forms.TextBox();
            this.textBoxFebra = new System.Windows.Forms.TextBox();
            this.buttonTomateMenos = new System.Windows.Forms.Button();
            this.buttonAlfaceMais = new System.Windows.Forms.Button();
            this.buttonBatataMais = new System.Windows.Forms.Button();
            this.buttonPepinoMais = new System.Windows.Forms.Button();
            this.buttonPimentoMais = new System.Windows.Forms.Button();
            this.buttonespinafreMais = new System.Windows.Forms.Button();
            this.buttonBroculosMais = new System.Windows.Forms.Button();
            this.buttonCenouraMais = new System.Windows.Forms.Button();
            this.buttonArrozMais = new System.Windows.Forms.Button();
            this.buttonErvilhasMais = new System.Windows.Forms.Button();
            this.buttonBifeMais = new System.Windows.Forms.Button();
            this.buttonPeruMais = new System.Windows.Forms.Button();
            this.buttonAlfaceMenos = new System.Windows.Forms.Button();
            this.buttonBatataMenos = new System.Windows.Forms.Button();
            this.buttonPepinoMenos = new System.Windows.Forms.Button();
            this.buttonPimentoMenos = new System.Windows.Forms.Button();
            this.buttonEspinafreMenos = new System.Windows.Forms.Button();
            this.buttonCenouraMenos = new System.Windows.Forms.Button();
            this.buttonBroculosMenos = new System.Windows.Forms.Button();
            this.buttonErvilhasMenos = new System.Windows.Forms.Button();
            this.buttonArrozMenos = new System.Windows.Forms.Button();
            this.buttonBifeMenos = new System.Windows.Forms.Button();
            this.buttonPeruMenos = new System.Windows.Forms.Button();
            this.buttonFebraMenos = new System.Windows.Forms.Button();
            this.buttonFebraMais = new System.Windows.Forms.Button();
            this.textBoxFrango = new System.Windows.Forms.TextBox();
            this.textBoxPorco = new System.Windows.Forms.TextBox();
            this.buttonFrangoMais = new System.Windows.Forms.Button();
            this.buttonPorcoMais = new System.Windows.Forms.Button();
            this.buttonFrangoMenos = new System.Windows.Forms.Button();
            this.buttonPorcoMenos = new System.Windows.Forms.Button();
            this.textBoxlula = new System.Windows.Forms.TextBox();
            this.textBoxsalmao = new System.Windows.Forms.TextBox();
            this.textBoxpolvo = new System.Windows.Forms.TextBox();
            this.textBoxsardinhas = new System.Windows.Forms.TextBox();
            this.textBoxfiletes = new System.Windows.Forms.TextBox();
            this.textBoxcarapau = new System.Windows.Forms.TextBox();
            this.textBoxdourada = new System.Windows.Forms.TextBox();
            this.textBoxlaranja = new System.Windows.Forms.TextBox();
            this.textBoxmaca = new System.Windows.Forms.TextBox();
            this.textBoxpera = new System.Windows.Forms.TextBox();
            this.textBoxbanana = new System.Windows.Forms.TextBox();
            this.textBoxkiwi = new System.Windows.Forms.TextBox();
            this.textBoxpessego = new System.Windows.Forms.TextBox();
            this.textBoxmelao = new System.Windows.Forms.TextBox();
            this.textBoxmelancia = new System.Windows.Forms.TextBox();
            this.textBoxmanga = new System.Windows.Forms.TextBox();
            this.textBoxfigo = new System.Windows.Forms.TextBox();
            this.textBoxananas = new System.Windows.Forms.TextBox();
            this.textBoxabacate = new System.Windows.Forms.TextBox();
            this.textBoxmassa = new System.Windows.Forms.TextBox();
            this.textBoxyogurte = new System.Windows.Forms.TextBox();
            this.textBoxleite = new System.Windows.Forms.TextBox();
            this.textBoxfrutossecos = new System.Windows.Forms.TextBox();
            this.textBoxpao = new System.Windows.Forms.TextBox();
            this.textBoxfrancesinha = new System.Windows.Forms.TextBox();
            this.buttonlulamais = new System.Windows.Forms.Button();
            this.buttonsalmaomais = new System.Windows.Forms.Button();
            this.buttonpolvomais = new System.Windows.Forms.Button();
            this.buttonsardinhasmais = new System.Windows.Forms.Button();
            this.buttonfiletesmais = new System.Windows.Forms.Button();
            this.buttoncarapaumais = new System.Windows.Forms.Button();
            this.buttondouradamais = new System.Windows.Forms.Button();
            this.buttonLaranjaMais = new System.Windows.Forms.Button();
            this.buttonMaçaMais = new System.Windows.Forms.Button();
            this.buttonPeraMais = new System.Windows.Forms.Button();
            this.buttonBananaMais = new System.Windows.Forms.Button();
            this.buttonKiwiMais = new System.Windows.Forms.Button();
            this.buttonPessegoMais = new System.Windows.Forms.Button();
            this.buttonMelaoMais = new System.Windows.Forms.Button();
            this.buttonMelanciaMais = new System.Windows.Forms.Button();
            this.buttonMangaMais = new System.Windows.Forms.Button();
            this.buttonFigoMais = new System.Windows.Forms.Button();
            this.buttonAnanasMais = new System.Windows.Forms.Button();
            this.buttonAbacateMais = new System.Windows.Forms.Button();
            this.buttonMassaMais = new System.Windows.Forms.Button();
            this.buttonYogurteMais = new System.Windows.Forms.Button();
            this.buttonLeiteMais = new System.Windows.Forms.Button();
            this.buttonFrutosSecosMais = new System.Windows.Forms.Button();
            this.buttonPaoMais = new System.Windows.Forms.Button();
            this.buttonFrancesinhMais = new System.Windows.Forms.Button();
            this.buttonlulamenos = new System.Windows.Forms.Button();
            this.buttonsalmaomenos = new System.Windows.Forms.Button();
            this.buttonpolvomenos = new System.Windows.Forms.Button();
            this.buttonsardinhasmenos = new System.Windows.Forms.Button();
            this.buttonfiletesmenos = new System.Windows.Forms.Button();
            this.buttoncarapaumenos = new System.Windows.Forms.Button();
            this.buttondouradamenos = new System.Windows.Forms.Button();
            this.buttonLaranjaMenos = new System.Windows.Forms.Button();
            this.buttonMaçaMenos = new System.Windows.Forms.Button();
            this.buttonPeraMenos = new System.Windows.Forms.Button();
            this.buttonBananaMenos = new System.Windows.Forms.Button();
            this.buttonKiwiMenos = new System.Windows.Forms.Button();
            this.buttonPessegoMenos = new System.Windows.Forms.Button();
            this.buttonMelaoMenos = new System.Windows.Forms.Button();
            this.buttonMelanciaMenos = new System.Windows.Forms.Button();
            this.buttonMangaMenos = new System.Windows.Forms.Button();
            this.buttonFigoMenos = new System.Windows.Forms.Button();
            this.buttonAnanasMenos = new System.Windows.Forms.Button();
            this.buttonAbacateMenos = new System.Windows.Forms.Button();
            this.buttonMassaMenos = new System.Windows.Forms.Button();
            this.buttonYogurteMenos = new System.Windows.Forms.Button();
            this.buttonLeiteMenos = new System.Windows.Forms.Button();
            this.buttonFrutosSecosMenos = new System.Windows.Forms.Button();
            this.buttonPaoMenos = new System.Windows.Forms.Button();
            this.buttonFrancesinhaMenos = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonLasanhaMais = new System.Windows.Forms.Button();
            this.buttonLasanhaMenos = new System.Windows.Forms.Button();
            this.labelVoltar = new System.Windows.Forms.Label();
            this.labelTabelaCalorias = new System.Windows.Forms.Label();
            this.labelCalcular = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textTomate
            // 
            this.textTomate.Location = new System.Drawing.Point(130, 143);
            this.textTomate.Name = "textTomate";
            this.textTomate.Size = new System.Drawing.Size(26, 20);
            this.textTomate.TabIndex = 7;
            this.textTomate.Text = "0";
            this.textTomate.TextChanged += new System.EventHandler(this.textTomate_TextChanged);
            // 
            // textBoxAlface
            // 
            this.textBoxAlface.Location = new System.Drawing.Point(130, 115);
            this.textBoxAlface.Name = "textBoxAlface";
            this.textBoxAlface.Size = new System.Drawing.Size(26, 20);
            this.textBoxAlface.TabIndex = 9;
            this.textBoxAlface.Text = "0";
            this.textBoxAlface.TextChanged += new System.EventHandler(this.textBoxAlface_TextChanged);
            // 
            // textBoxBatata
            // 
            this.textBoxBatata.Location = new System.Drawing.Point(130, 171);
            this.textBoxBatata.Name = "textBoxBatata";
            this.textBoxBatata.Size = new System.Drawing.Size(26, 20);
            this.textBoxBatata.TabIndex = 15;
            this.textBoxBatata.Text = "0";
            this.textBoxBatata.TextChanged += new System.EventHandler(this.textBoxBatata_TextChanged);
            // 
            // textBoxPepino
            // 
            this.textBoxPepino.Location = new System.Drawing.Point(130, 200);
            this.textBoxPepino.Name = "textBoxPepino";
            this.textBoxPepino.Size = new System.Drawing.Size(26, 20);
            this.textBoxPepino.TabIndex = 16;
            this.textBoxPepino.Text = "0";
            this.textBoxPepino.TextChanged += new System.EventHandler(this.textBoxPepino_TextChanged);
            // 
            // textBoxpimento
            // 
            this.textBoxpimento.Location = new System.Drawing.Point(130, 225);
            this.textBoxpimento.Name = "textBoxpimento";
            this.textBoxpimento.Size = new System.Drawing.Size(26, 20);
            this.textBoxpimento.TabIndex = 17;
            this.textBoxpimento.Text = "0";
            this.textBoxpimento.TextChanged += new System.EventHandler(this.textBoxpimento_TextChanged);
            // 
            // textBoxespinafre
            // 
            this.textBoxespinafre.Location = new System.Drawing.Point(130, 258);
            this.textBoxespinafre.Name = "textBoxespinafre";
            this.textBoxespinafre.Size = new System.Drawing.Size(26, 20);
            this.textBoxespinafre.TabIndex = 18;
            this.textBoxespinafre.Text = "0";
            this.textBoxespinafre.TextChanged += new System.EventHandler(this.textBoxespinafre_TextChanged);
            // 
            // textBoxcenoura
            // 
            this.textBoxcenoura.Location = new System.Drawing.Point(130, 287);
            this.textBoxcenoura.Name = "textBoxcenoura";
            this.textBoxcenoura.Size = new System.Drawing.Size(26, 20);
            this.textBoxcenoura.TabIndex = 19;
            this.textBoxcenoura.Text = "0";
            this.textBoxcenoura.TextChanged += new System.EventHandler(this.textBoxcenoura_TextChanged);
            // 
            // textBoxbroculos
            // 
            this.textBoxbroculos.Location = new System.Drawing.Point(130, 313);
            this.textBoxbroculos.Name = "textBoxbroculos";
            this.textBoxbroculos.Size = new System.Drawing.Size(26, 20);
            this.textBoxbroculos.TabIndex = 20;
            this.textBoxbroculos.Text = "0";
            this.textBoxbroculos.TextChanged += new System.EventHandler(this.textBoxbroculos_TextChanged);
            // 
            // textBoxervilhas
            // 
            this.textBoxervilhas.Location = new System.Drawing.Point(130, 339);
            this.textBoxervilhas.Name = "textBoxervilhas";
            this.textBoxervilhas.Size = new System.Drawing.Size(26, 20);
            this.textBoxervilhas.TabIndex = 21;
            this.textBoxervilhas.Text = "0";
            this.textBoxervilhas.TextChanged += new System.EventHandler(this.textBoxervilhas_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(190, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 23);
            this.button1.TabIndex = 62;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // labelcalorias
            // 
            this.labelcalorias.AutoSize = true;
            this.labelcalorias.Location = new System.Drawing.Point(1201, 574);
            this.labelcalorias.Name = "labelcalorias";
            this.labelcalorias.Size = new System.Drawing.Size(16, 13);
            this.labelcalorias.TabIndex = 63;
            this.labelcalorias.Text = "---";
            // 
            // textBoxArroz
            // 
            this.textBoxArroz.Location = new System.Drawing.Point(1182, 289);
            this.textBoxArroz.Name = "textBoxArroz";
            this.textBoxArroz.Size = new System.Drawing.Size(26, 20);
            this.textBoxArroz.TabIndex = 65;
            this.textBoxArroz.Text = "0";
            this.textBoxArroz.TextChanged += new System.EventHandler(this.textBoxArroz_TextChanged);
            // 
            // textBoxbife
            // 
            this.textBoxbife.Location = new System.Drawing.Point(400, 119);
            this.textBoxbife.Name = "textBoxbife";
            this.textBoxbife.Size = new System.Drawing.Size(26, 20);
            this.textBoxbife.TabIndex = 66;
            this.textBoxbife.Text = "0";
            this.textBoxbife.TextChanged += new System.EventHandler(this.textBoxbife_TextChanged);
            // 
            // textBoxperu
            // 
            this.textBoxperu.Location = new System.Drawing.Point(400, 146);
            this.textBoxperu.Name = "textBoxperu";
            this.textBoxperu.Size = new System.Drawing.Size(26, 20);
            this.textBoxperu.TabIndex = 67;
            this.textBoxperu.Text = "0";
            this.textBoxperu.TextChanged += new System.EventHandler(this.textBoxperu_TextChanged);
            // 
            // textBoxFebra
            // 
            this.textBoxFebra.Location = new System.Drawing.Point(400, 176);
            this.textBoxFebra.Name = "textBoxFebra";
            this.textBoxFebra.Size = new System.Drawing.Size(26, 20);
            this.textBoxFebra.TabIndex = 68;
            this.textBoxFebra.Text = "0";
            this.textBoxFebra.TextChanged += new System.EventHandler(this.textBoxfebra_TextChanged);
            // 
            // buttonTomateMenos
            // 
            this.buttonTomateMenos.Location = new System.Drawing.Point(224, 140);
            this.buttonTomateMenos.Name = "buttonTomateMenos";
            this.buttonTomateMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonTomateMenos.TabIndex = 70;
            this.buttonTomateMenos.Text = "-";
            this.buttonTomateMenos.UseVisualStyleBackColor = true;
            this.buttonTomateMenos.Click += new System.EventHandler(this.buttonTomate_Click);
            // 
            // buttonAlfaceMais
            // 
            this.buttonAlfaceMais.Location = new System.Drawing.Point(190, 110);
            this.buttonAlfaceMais.Name = "buttonAlfaceMais";
            this.buttonAlfaceMais.Size = new System.Drawing.Size(28, 23);
            this.buttonAlfaceMais.TabIndex = 71;
            this.buttonAlfaceMais.Text = "+";
            this.buttonAlfaceMais.UseVisualStyleBackColor = true;
            this.buttonAlfaceMais.Click += new System.EventHandler(this.buttonAlfaceMais_Click);
            // 
            // buttonBatataMais
            // 
            this.buttonBatataMais.Location = new System.Drawing.Point(190, 169);
            this.buttonBatataMais.Name = "buttonBatataMais";
            this.buttonBatataMais.Size = new System.Drawing.Size(28, 23);
            this.buttonBatataMais.TabIndex = 72;
            this.buttonBatataMais.Text = "+";
            this.buttonBatataMais.UseVisualStyleBackColor = true;
            this.buttonBatataMais.Click += new System.EventHandler(this.buttonBatataMais_Click);
            // 
            // buttonPepinoMais
            // 
            this.buttonPepinoMais.Location = new System.Drawing.Point(190, 197);
            this.buttonPepinoMais.Name = "buttonPepinoMais";
            this.buttonPepinoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPepinoMais.TabIndex = 73;
            this.buttonPepinoMais.Text = "+";
            this.buttonPepinoMais.UseVisualStyleBackColor = true;
            this.buttonPepinoMais.Click += new System.EventHandler(this.buttonPepinoMais_Click);
            // 
            // buttonPimentoMais
            // 
            this.buttonPimentoMais.Location = new System.Drawing.Point(190, 226);
            this.buttonPimentoMais.Name = "buttonPimentoMais";
            this.buttonPimentoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPimentoMais.TabIndex = 74;
            this.buttonPimentoMais.Text = "+";
            this.buttonPimentoMais.UseVisualStyleBackColor = true;
            this.buttonPimentoMais.Click += new System.EventHandler(this.buttonPimentoMais_Click);
            // 
            // buttonespinafreMais
            // 
            this.buttonespinafreMais.Location = new System.Drawing.Point(190, 255);
            this.buttonespinafreMais.Name = "buttonespinafreMais";
            this.buttonespinafreMais.Size = new System.Drawing.Size(28, 23);
            this.buttonespinafreMais.TabIndex = 75;
            this.buttonespinafreMais.Text = "+";
            this.buttonespinafreMais.UseVisualStyleBackColor = true;
            this.buttonespinafreMais.Click += new System.EventHandler(this.buttonespinafreMais_Click);
            // 
            // buttonBroculosMais
            // 
            this.buttonBroculosMais.Location = new System.Drawing.Point(190, 313);
            this.buttonBroculosMais.Name = "buttonBroculosMais";
            this.buttonBroculosMais.Size = new System.Drawing.Size(28, 23);
            this.buttonBroculosMais.TabIndex = 76;
            this.buttonBroculosMais.Text = "+";
            this.buttonBroculosMais.UseVisualStyleBackColor = true;
            this.buttonBroculosMais.Click += new System.EventHandler(this.buttonBroculosMais_Click);
            // 
            // buttonCenouraMais
            // 
            this.buttonCenouraMais.Location = new System.Drawing.Point(190, 287);
            this.buttonCenouraMais.Name = "buttonCenouraMais";
            this.buttonCenouraMais.Size = new System.Drawing.Size(28, 23);
            this.buttonCenouraMais.TabIndex = 77;
            this.buttonCenouraMais.Text = "+";
            this.buttonCenouraMais.UseVisualStyleBackColor = true;
            this.buttonCenouraMais.Click += new System.EventHandler(this.buttonCenouraMais_Click);
            // 
            // buttonArrozMais
            // 
            this.buttonArrozMais.Location = new System.Drawing.Point(1242, 289);
            this.buttonArrozMais.Name = "buttonArrozMais";
            this.buttonArrozMais.Size = new System.Drawing.Size(28, 23);
            this.buttonArrozMais.TabIndex = 78;
            this.buttonArrozMais.Text = "+";
            this.buttonArrozMais.UseVisualStyleBackColor = true;
            this.buttonArrozMais.Click += new System.EventHandler(this.buttonArrozMais_Click);
            // 
            // buttonErvilhasMais
            // 
            this.buttonErvilhasMais.Location = new System.Drawing.Point(190, 336);
            this.buttonErvilhasMais.Name = "buttonErvilhasMais";
            this.buttonErvilhasMais.Size = new System.Drawing.Size(28, 23);
            this.buttonErvilhasMais.TabIndex = 79;
            this.buttonErvilhasMais.Text = "+";
            this.buttonErvilhasMais.UseVisualStyleBackColor = true;
            this.buttonErvilhasMais.Click += new System.EventHandler(this.buttonErvilhasMais_Click);
            // 
            // buttonBifeMais
            // 
            this.buttonBifeMais.Location = new System.Drawing.Point(464, 116);
            this.buttonBifeMais.Name = "buttonBifeMais";
            this.buttonBifeMais.Size = new System.Drawing.Size(28, 23);
            this.buttonBifeMais.TabIndex = 80;
            this.buttonBifeMais.Text = "+";
            this.buttonBifeMais.UseVisualStyleBackColor = true;
            this.buttonBifeMais.Click += new System.EventHandler(this.buttonBifeMais_Click);
            // 
            // buttonPeruMais
            // 
            this.buttonPeruMais.Location = new System.Drawing.Point(464, 147);
            this.buttonPeruMais.Name = "buttonPeruMais";
            this.buttonPeruMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPeruMais.TabIndex = 81;
            this.buttonPeruMais.Text = "+";
            this.buttonPeruMais.UseVisualStyleBackColor = true;
            this.buttonPeruMais.Click += new System.EventHandler(this.buttonPeruMais_Click);
            // 
            // buttonAlfaceMenos
            // 
            this.buttonAlfaceMenos.Location = new System.Drawing.Point(224, 118);
            this.buttonAlfaceMenos.Name = "buttonAlfaceMenos";
            this.buttonAlfaceMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonAlfaceMenos.TabIndex = 85;
            this.buttonAlfaceMenos.Text = "-";
            this.buttonAlfaceMenos.UseVisualStyleBackColor = true;
            this.buttonAlfaceMenos.Click += new System.EventHandler(this.buttonAlfaceMenos_Click);
            // 
            // buttonBatataMenos
            // 
            this.buttonBatataMenos.Location = new System.Drawing.Point(224, 163);
            this.buttonBatataMenos.Name = "buttonBatataMenos";
            this.buttonBatataMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonBatataMenos.TabIndex = 86;
            this.buttonBatataMenos.Text = "-";
            this.buttonBatataMenos.UseVisualStyleBackColor = true;
            this.buttonBatataMenos.Click += new System.EventHandler(this.buttonBatataMenos_Click);
            // 
            // buttonPepinoMenos
            // 
            this.buttonPepinoMenos.Location = new System.Drawing.Point(224, 195);
            this.buttonPepinoMenos.Name = "buttonPepinoMenos";
            this.buttonPepinoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPepinoMenos.TabIndex = 87;
            this.buttonPepinoMenos.Text = "-";
            this.buttonPepinoMenos.UseVisualStyleBackColor = true;
            this.buttonPepinoMenos.Click += new System.EventHandler(this.buttonPepinoMenos_Click);
            // 
            // buttonPimentoMenos
            // 
            this.buttonPimentoMenos.Location = new System.Drawing.Point(224, 226);
            this.buttonPimentoMenos.Name = "buttonPimentoMenos";
            this.buttonPimentoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPimentoMenos.TabIndex = 88;
            this.buttonPimentoMenos.Text = "-";
            this.buttonPimentoMenos.UseVisualStyleBackColor = true;
            this.buttonPimentoMenos.Click += new System.EventHandler(this.buttonPimentoMenos_Click);
            // 
            // buttonEspinafreMenos
            // 
            this.buttonEspinafreMenos.Location = new System.Drawing.Point(224, 255);
            this.buttonEspinafreMenos.Name = "buttonEspinafreMenos";
            this.buttonEspinafreMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonEspinafreMenos.TabIndex = 89;
            this.buttonEspinafreMenos.Text = "-";
            this.buttonEspinafreMenos.UseVisualStyleBackColor = true;
            this.buttonEspinafreMenos.Click += new System.EventHandler(this.buttonEspinafreMenos_Click);
            // 
            // buttonCenouraMenos
            // 
            this.buttonCenouraMenos.Location = new System.Drawing.Point(224, 287);
            this.buttonCenouraMenos.Name = "buttonCenouraMenos";
            this.buttonCenouraMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonCenouraMenos.TabIndex = 90;
            this.buttonCenouraMenos.Text = "-";
            this.buttonCenouraMenos.UseVisualStyleBackColor = true;
            this.buttonCenouraMenos.Click += new System.EventHandler(this.buttonCenouraMenos_Click);
            // 
            // buttonBroculosMenos
            // 
            this.buttonBroculosMenos.Location = new System.Drawing.Point(224, 313);
            this.buttonBroculosMenos.Name = "buttonBroculosMenos";
            this.buttonBroculosMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonBroculosMenos.TabIndex = 91;
            this.buttonBroculosMenos.Text = "-";
            this.buttonBroculosMenos.UseVisualStyleBackColor = true;
            this.buttonBroculosMenos.Click += new System.EventHandler(this.buttonBroculosMenos_Click);
            // 
            // buttonErvilhasMenos
            // 
            this.buttonErvilhasMenos.Location = new System.Drawing.Point(224, 336);
            this.buttonErvilhasMenos.Name = "buttonErvilhasMenos";
            this.buttonErvilhasMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonErvilhasMenos.TabIndex = 92;
            this.buttonErvilhasMenos.Text = "-";
            this.buttonErvilhasMenos.UseVisualStyleBackColor = true;
            this.buttonErvilhasMenos.Click += new System.EventHandler(this.buttonErvilhasMenos_Click);
            // 
            // buttonArrozMenos
            // 
            this.buttonArrozMenos.Location = new System.Drawing.Point(1276, 289);
            this.buttonArrozMenos.Name = "buttonArrozMenos";
            this.buttonArrozMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonArrozMenos.TabIndex = 93;
            this.buttonArrozMenos.Text = "-";
            this.buttonArrozMenos.UseVisualStyleBackColor = true;
            // 
            // buttonBifeMenos
            // 
            this.buttonBifeMenos.Location = new System.Drawing.Point(498, 117);
            this.buttonBifeMenos.Name = "buttonBifeMenos";
            this.buttonBifeMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonBifeMenos.TabIndex = 94;
            this.buttonBifeMenos.Text = "-";
            this.buttonBifeMenos.UseVisualStyleBackColor = true;
            this.buttonBifeMenos.Click += new System.EventHandler(this.buttonBifeMenos_Click);
            // 
            // buttonPeruMenos
            // 
            this.buttonPeruMenos.Location = new System.Drawing.Point(498, 147);
            this.buttonPeruMenos.Name = "buttonPeruMenos";
            this.buttonPeruMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPeruMenos.TabIndex = 95;
            this.buttonPeruMenos.Text = "-";
            this.buttonPeruMenos.UseVisualStyleBackColor = true;
            this.buttonPeruMenos.Click += new System.EventHandler(this.buttonPeruMenos_Click);
            // 
            // buttonFebraMenos
            // 
            this.buttonFebraMenos.Location = new System.Drawing.Point(498, 179);
            this.buttonFebraMenos.Name = "buttonFebraMenos";
            this.buttonFebraMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonFebraMenos.TabIndex = 96;
            this.buttonFebraMenos.Text = "-";
            this.buttonFebraMenos.UseVisualStyleBackColor = true;
            this.buttonFebraMenos.Click += new System.EventHandler(this.buttonFebraMenos_Click);
            // 
            // buttonFebraMais
            // 
            this.buttonFebraMais.Location = new System.Drawing.Point(464, 177);
            this.buttonFebraMais.Name = "buttonFebraMais";
            this.buttonFebraMais.Size = new System.Drawing.Size(28, 23);
            this.buttonFebraMais.TabIndex = 97;
            this.buttonFebraMais.Text = "+";
            this.buttonFebraMais.UseVisualStyleBackColor = true;
            this.buttonFebraMais.Click += new System.EventHandler(this.buttonFebraMais_Click);
            // 
            // textBoxFrango
            // 
            this.textBoxFrango.Location = new System.Drawing.Point(400, 205);
            this.textBoxFrango.Name = "textBoxFrango";
            this.textBoxFrango.Size = new System.Drawing.Size(26, 20);
            this.textBoxFrango.TabIndex = 98;
            this.textBoxFrango.Text = "0";
            this.textBoxFrango.TextChanged += new System.EventHandler(this.textBoxFrango_TextChanged);
            // 
            // textBoxPorco
            // 
            this.textBoxPorco.Location = new System.Drawing.Point(400, 229);
            this.textBoxPorco.Name = "textBoxPorco";
            this.textBoxPorco.Size = new System.Drawing.Size(26, 20);
            this.textBoxPorco.TabIndex = 99;
            this.textBoxPorco.Text = "0";
            this.textBoxPorco.TextChanged += new System.EventHandler(this.textBoxPorco_TextChanged);
            // 
            // buttonFrangoMais
            // 
            this.buttonFrangoMais.Location = new System.Drawing.Point(464, 203);
            this.buttonFrangoMais.Name = "buttonFrangoMais";
            this.buttonFrangoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonFrangoMais.TabIndex = 100;
            this.buttonFrangoMais.Text = "+";
            this.buttonFrangoMais.UseVisualStyleBackColor = true;
            this.buttonFrangoMais.Click += new System.EventHandler(this.buttonFrangoMais_Click);
            // 
            // buttonPorcoMais
            // 
            this.buttonPorcoMais.Location = new System.Drawing.Point(464, 229);
            this.buttonPorcoMais.Name = "buttonPorcoMais";
            this.buttonPorcoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPorcoMais.TabIndex = 101;
            this.buttonPorcoMais.Text = "+";
            this.buttonPorcoMais.UseVisualStyleBackColor = true;
            this.buttonPorcoMais.Click += new System.EventHandler(this.buttonPorcoMais_Click);
            // 
            // buttonFrangoMenos
            // 
            this.buttonFrangoMenos.Location = new System.Drawing.Point(498, 203);
            this.buttonFrangoMenos.Name = "buttonFrangoMenos";
            this.buttonFrangoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonFrangoMenos.TabIndex = 102;
            this.buttonFrangoMenos.Text = "-";
            this.buttonFrangoMenos.UseVisualStyleBackColor = true;
            this.buttonFrangoMenos.Click += new System.EventHandler(this.buttonFrangoMenos_Click);
            // 
            // buttonPorcoMenos
            // 
            this.buttonPorcoMenos.Location = new System.Drawing.Point(498, 229);
            this.buttonPorcoMenos.Name = "buttonPorcoMenos";
            this.buttonPorcoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPorcoMenos.TabIndex = 103;
            this.buttonPorcoMenos.Text = "-";
            this.buttonPorcoMenos.UseVisualStyleBackColor = true;
            this.buttonPorcoMenos.Click += new System.EventHandler(this.buttonPorcoMenos_Click);
            // 
            // textBoxlula
            // 
            this.textBoxlula.Location = new System.Drawing.Point(661, 123);
            this.textBoxlula.Name = "textBoxlula";
            this.textBoxlula.Size = new System.Drawing.Size(26, 20);
            this.textBoxlula.TabIndex = 107;
            this.textBoxlula.Text = "0";
            this.textBoxlula.TextChanged += new System.EventHandler(this.textBoxlula_TextChanged);
            // 
            // textBoxsalmao
            // 
            this.textBoxsalmao.Location = new System.Drawing.Point(661, 149);
            this.textBoxsalmao.Name = "textBoxsalmao";
            this.textBoxsalmao.Size = new System.Drawing.Size(26, 20);
            this.textBoxsalmao.TabIndex = 108;
            this.textBoxsalmao.Text = "0";
            this.textBoxsalmao.TextChanged += new System.EventHandler(this.textBoxsalmao_TextChanged);
            // 
            // textBoxpolvo
            // 
            this.textBoxpolvo.Location = new System.Drawing.Point(661, 177);
            this.textBoxpolvo.Name = "textBoxpolvo";
            this.textBoxpolvo.Size = new System.Drawing.Size(26, 20);
            this.textBoxpolvo.TabIndex = 109;
            this.textBoxpolvo.Text = "0";
            this.textBoxpolvo.TextChanged += new System.EventHandler(this.textBoxpolvo_TextChanged);
            // 
            // textBoxsardinhas
            // 
            this.textBoxsardinhas.Location = new System.Drawing.Point(661, 205);
            this.textBoxsardinhas.Name = "textBoxsardinhas";
            this.textBoxsardinhas.Size = new System.Drawing.Size(26, 20);
            this.textBoxsardinhas.TabIndex = 110;
            this.textBoxsardinhas.Text = "0";
            this.textBoxsardinhas.TextChanged += new System.EventHandler(this.textBoxsardinhas_TextChanged);
            // 
            // textBoxfiletes
            // 
            this.textBoxfiletes.Location = new System.Drawing.Point(661, 233);
            this.textBoxfiletes.Name = "textBoxfiletes";
            this.textBoxfiletes.Size = new System.Drawing.Size(26, 20);
            this.textBoxfiletes.TabIndex = 111;
            this.textBoxfiletes.Text = "0";
            this.textBoxfiletes.TextChanged += new System.EventHandler(this.textBoxfiletes_TextChanged);
            // 
            // textBoxcarapau
            // 
            this.textBoxcarapau.Location = new System.Drawing.Point(661, 258);
            this.textBoxcarapau.Name = "textBoxcarapau";
            this.textBoxcarapau.Size = new System.Drawing.Size(26, 20);
            this.textBoxcarapau.TabIndex = 112;
            this.textBoxcarapau.Text = "0";
            this.textBoxcarapau.TextChanged += new System.EventHandler(this.textBoxcarapau_TextChanged);
            // 
            // textBoxdourada
            // 
            this.textBoxdourada.Location = new System.Drawing.Point(661, 289);
            this.textBoxdourada.Name = "textBoxdourada";
            this.textBoxdourada.Size = new System.Drawing.Size(26, 20);
            this.textBoxdourada.TabIndex = 113;
            this.textBoxdourada.Text = "0";
            this.textBoxdourada.TextChanged += new System.EventHandler(this.textBoxdourada_TextChanged);
            // 
            // textBoxlaranja
            // 
            this.textBoxlaranja.Location = new System.Drawing.Point(922, 122);
            this.textBoxlaranja.Name = "textBoxlaranja";
            this.textBoxlaranja.Size = new System.Drawing.Size(26, 20);
            this.textBoxlaranja.TabIndex = 114;
            this.textBoxlaranja.Text = "0";
            this.textBoxlaranja.TextChanged += new System.EventHandler(this.textBoxlaranja_TextChanged);
            // 
            // textBoxmaca
            // 
            this.textBoxmaca.Location = new System.Drawing.Point(922, 149);
            this.textBoxmaca.Name = "textBoxmaca";
            this.textBoxmaca.Size = new System.Drawing.Size(26, 20);
            this.textBoxmaca.TabIndex = 115;
            this.textBoxmaca.Text = "0";
            this.textBoxmaca.TextChanged += new System.EventHandler(this.textBoxmaca_TextChanged);
            // 
            // textBoxpera
            // 
            this.textBoxpera.Location = new System.Drawing.Point(922, 175);
            this.textBoxpera.Name = "textBoxpera";
            this.textBoxpera.Size = new System.Drawing.Size(26, 20);
            this.textBoxpera.TabIndex = 116;
            this.textBoxpera.Text = "0";
            this.textBoxpera.TextChanged += new System.EventHandler(this.textBoxpera_TextChanged);
            // 
            // textBoxbanana
            // 
            this.textBoxbanana.Location = new System.Drawing.Point(922, 198);
            this.textBoxbanana.Name = "textBoxbanana";
            this.textBoxbanana.Size = new System.Drawing.Size(26, 20);
            this.textBoxbanana.TabIndex = 117;
            this.textBoxbanana.Text = "0";
            this.textBoxbanana.TextChanged += new System.EventHandler(this.textBoxbanana_TextChanged);
            // 
            // textBoxkiwi
            // 
            this.textBoxkiwi.Location = new System.Drawing.Point(921, 229);
            this.textBoxkiwi.Name = "textBoxkiwi";
            this.textBoxkiwi.Size = new System.Drawing.Size(26, 20);
            this.textBoxkiwi.TabIndex = 118;
            this.textBoxkiwi.Text = "0";
            this.textBoxkiwi.TextChanged += new System.EventHandler(this.textBoxkiwi_TextChanged);
            // 
            // textBoxpessego
            // 
            this.textBoxpessego.Location = new System.Drawing.Point(922, 258);
            this.textBoxpessego.Name = "textBoxpessego";
            this.textBoxpessego.Size = new System.Drawing.Size(26, 20);
            this.textBoxpessego.TabIndex = 119;
            this.textBoxpessego.Text = "0";
            this.textBoxpessego.TextChanged += new System.EventHandler(this.textBoxpessego_TextChanged);
            // 
            // textBoxmelao
            // 
            this.textBoxmelao.Location = new System.Drawing.Point(921, 289);
            this.textBoxmelao.Name = "textBoxmelao";
            this.textBoxmelao.Size = new System.Drawing.Size(26, 20);
            this.textBoxmelao.TabIndex = 121;
            this.textBoxmelao.Text = "0";
            // 
            // textBoxmelancia
            // 
            this.textBoxmelancia.Location = new System.Drawing.Point(921, 319);
            this.textBoxmelancia.Name = "textBoxmelancia";
            this.textBoxmelancia.Size = new System.Drawing.Size(26, 20);
            this.textBoxmelancia.TabIndex = 122;
            this.textBoxmelancia.Text = "0";
            // 
            // textBoxmanga
            // 
            this.textBoxmanga.Location = new System.Drawing.Point(922, 345);
            this.textBoxmanga.Name = "textBoxmanga";
            this.textBoxmanga.Size = new System.Drawing.Size(26, 20);
            this.textBoxmanga.TabIndex = 123;
            this.textBoxmanga.Text = "0";
            this.textBoxmanga.TextChanged += new System.EventHandler(this.textBoxmanga_TextChanged);
            // 
            // textBoxfigo
            // 
            this.textBoxfigo.Location = new System.Drawing.Point(922, 379);
            this.textBoxfigo.Name = "textBoxfigo";
            this.textBoxfigo.Size = new System.Drawing.Size(26, 20);
            this.textBoxfigo.TabIndex = 124;
            this.textBoxfigo.Text = "0";
            // 
            // textBoxananas
            // 
            this.textBoxananas.Location = new System.Drawing.Point(922, 405);
            this.textBoxananas.Name = "textBoxananas";
            this.textBoxananas.Size = new System.Drawing.Size(26, 20);
            this.textBoxananas.TabIndex = 125;
            this.textBoxananas.Text = "0";
            // 
            // textBoxabacate
            // 
            this.textBoxabacate.Location = new System.Drawing.Point(922, 431);
            this.textBoxabacate.Name = "textBoxabacate";
            this.textBoxabacate.Size = new System.Drawing.Size(26, 20);
            this.textBoxabacate.TabIndex = 126;
            this.textBoxabacate.Text = "0";
            // 
            // textBoxmassa
            // 
            this.textBoxmassa.Location = new System.Drawing.Point(1182, 119);
            this.textBoxmassa.Name = "textBoxmassa";
            this.textBoxmassa.Size = new System.Drawing.Size(26, 20);
            this.textBoxmassa.TabIndex = 127;
            this.textBoxmassa.Text = "0";
            this.textBoxmassa.TextChanged += new System.EventHandler(this.textBoxmassa_TextChanged);
            // 
            // textBoxyogurte
            // 
            this.textBoxyogurte.Location = new System.Drawing.Point(1182, 142);
            this.textBoxyogurte.Name = "textBoxyogurte";
            this.textBoxyogurte.Size = new System.Drawing.Size(26, 20);
            this.textBoxyogurte.TabIndex = 128;
            this.textBoxyogurte.Text = "0";
            this.textBoxyogurte.TextChanged += new System.EventHandler(this.textBoxyogurte_TextChanged);
            // 
            // textBoxleite
            // 
            this.textBoxleite.Location = new System.Drawing.Point(1182, 176);
            this.textBoxleite.Name = "textBoxleite";
            this.textBoxleite.Size = new System.Drawing.Size(26, 20);
            this.textBoxleite.TabIndex = 129;
            this.textBoxleite.Text = "0";
            this.textBoxleite.TextChanged += new System.EventHandler(this.textBoxleite_TextChanged);
            // 
            // textBoxfrutossecos
            // 
            this.textBoxfrutossecos.Location = new System.Drawing.Point(1182, 206);
            this.textBoxfrutossecos.Name = "textBoxfrutossecos";
            this.textBoxfrutossecos.Size = new System.Drawing.Size(26, 20);
            this.textBoxfrutossecos.TabIndex = 130;
            this.textBoxfrutossecos.Text = "0";
            // 
            // textBoxpao
            // 
            this.textBoxpao.Location = new System.Drawing.Point(1182, 232);
            this.textBoxpao.Name = "textBoxpao";
            this.textBoxpao.Size = new System.Drawing.Size(26, 20);
            this.textBoxpao.TabIndex = 131;
            this.textBoxpao.Text = "0";
            this.textBoxpao.TextChanged += new System.EventHandler(this.textBoxpao_TextChanged);
            // 
            // textBoxfrancesinha
            // 
            this.textBoxfrancesinha.Location = new System.Drawing.Point(1182, 262);
            this.textBoxfrancesinha.Name = "textBoxfrancesinha";
            this.textBoxfrancesinha.Size = new System.Drawing.Size(26, 20);
            this.textBoxfrancesinha.TabIndex = 132;
            this.textBoxfrancesinha.Text = "0";
            this.textBoxfrancesinha.TextChanged += new System.EventHandler(this.textBoxfrancesinha_TextChanged);
            // 
            // buttonlulamais
            // 
            this.buttonlulamais.Location = new System.Drawing.Point(727, 121);
            this.buttonlulamais.Name = "buttonlulamais";
            this.buttonlulamais.Size = new System.Drawing.Size(28, 23);
            this.buttonlulamais.TabIndex = 133;
            this.buttonlulamais.Text = "+";
            this.buttonlulamais.UseVisualStyleBackColor = true;
            this.buttonlulamais.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonsalmaomais
            // 
            this.buttonsalmaomais.Location = new System.Drawing.Point(727, 149);
            this.buttonsalmaomais.Name = "buttonsalmaomais";
            this.buttonsalmaomais.Size = new System.Drawing.Size(28, 23);
            this.buttonsalmaomais.TabIndex = 134;
            this.buttonsalmaomais.Text = "+";
            this.buttonsalmaomais.UseVisualStyleBackColor = true;
            this.buttonsalmaomais.Click += new System.EventHandler(this.buttonsalmaomais_Click);
            // 
            // buttonpolvomais
            // 
            this.buttonpolvomais.Location = new System.Drawing.Point(727, 176);
            this.buttonpolvomais.Name = "buttonpolvomais";
            this.buttonpolvomais.Size = new System.Drawing.Size(28, 23);
            this.buttonpolvomais.TabIndex = 135;
            this.buttonpolvomais.Text = "+";
            this.buttonpolvomais.UseVisualStyleBackColor = true;
            this.buttonpolvomais.Click += new System.EventHandler(this.buttonpolvomais_Click);
            // 
            // buttonsardinhasmais
            // 
            this.buttonsardinhasmais.Location = new System.Drawing.Point(727, 202);
            this.buttonsardinhasmais.Name = "buttonsardinhasmais";
            this.buttonsardinhasmais.Size = new System.Drawing.Size(28, 23);
            this.buttonsardinhasmais.TabIndex = 136;
            this.buttonsardinhasmais.Text = "+";
            this.buttonsardinhasmais.UseVisualStyleBackColor = true;
            this.buttonsardinhasmais.Click += new System.EventHandler(this.buttonsardinhasmais_Click);
            // 
            // buttonfiletesmais
            // 
            this.buttonfiletesmais.Location = new System.Drawing.Point(727, 231);
            this.buttonfiletesmais.Name = "buttonfiletesmais";
            this.buttonfiletesmais.Size = new System.Drawing.Size(28, 23);
            this.buttonfiletesmais.TabIndex = 137;
            this.buttonfiletesmais.Text = "+";
            this.buttonfiletesmais.UseVisualStyleBackColor = true;
            this.buttonfiletesmais.Click += new System.EventHandler(this.buttonfiletesmais_Click);
            // 
            // buttoncarapaumais
            // 
            this.buttoncarapaumais.Location = new System.Drawing.Point(727, 260);
            this.buttoncarapaumais.Name = "buttoncarapaumais";
            this.buttoncarapaumais.Size = new System.Drawing.Size(28, 23);
            this.buttoncarapaumais.TabIndex = 138;
            this.buttoncarapaumais.Text = "+";
            this.buttoncarapaumais.UseVisualStyleBackColor = true;
            this.buttoncarapaumais.Click += new System.EventHandler(this.buttoncarapaumais_Click);
            // 
            // buttondouradamais
            // 
            this.buttondouradamais.Location = new System.Drawing.Point(727, 287);
            this.buttondouradamais.Name = "buttondouradamais";
            this.buttondouradamais.Size = new System.Drawing.Size(28, 23);
            this.buttondouradamais.TabIndex = 139;
            this.buttondouradamais.Text = "+";
            this.buttondouradamais.UseVisualStyleBackColor = true;
            this.buttondouradamais.Click += new System.EventHandler(this.buttondouradamais_Click);
            // 
            // buttonLaranjaMais
            // 
            this.buttonLaranjaMais.Location = new System.Drawing.Point(984, 116);
            this.buttonLaranjaMais.Name = "buttonLaranjaMais";
            this.buttonLaranjaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonLaranjaMais.TabIndex = 140;
            this.buttonLaranjaMais.Text = "+";
            this.buttonLaranjaMais.UseVisualStyleBackColor = true;
            this.buttonLaranjaMais.Click += new System.EventHandler(this.buttonLaranjaMais_Click);
            // 
            // buttonMaçaMais
            // 
            this.buttonMaçaMais.Location = new System.Drawing.Point(984, 147);
            this.buttonMaçaMais.Name = "buttonMaçaMais";
            this.buttonMaçaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonMaçaMais.TabIndex = 141;
            this.buttonMaçaMais.Text = "+";
            this.buttonMaçaMais.UseVisualStyleBackColor = true;
            this.buttonMaçaMais.Click += new System.EventHandler(this.buttonMaçaMais_Click);
            // 
            // buttonPeraMais
            // 
            this.buttonPeraMais.Location = new System.Drawing.Point(984, 173);
            this.buttonPeraMais.Name = "buttonPeraMais";
            this.buttonPeraMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPeraMais.TabIndex = 142;
            this.buttonPeraMais.Text = "+";
            this.buttonPeraMais.UseVisualStyleBackColor = true;
            this.buttonPeraMais.Click += new System.EventHandler(this.buttonPeraMais_Click);
            // 
            // buttonBananaMais
            // 
            this.buttonBananaMais.Location = new System.Drawing.Point(984, 203);
            this.buttonBananaMais.Name = "buttonBananaMais";
            this.buttonBananaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonBananaMais.TabIndex = 143;
            this.buttonBananaMais.Text = "+";
            this.buttonBananaMais.UseVisualStyleBackColor = true;
            this.buttonBananaMais.Click += new System.EventHandler(this.buttonBananaMais_Click);
            // 
            // buttonKiwiMais
            // 
            this.buttonKiwiMais.Location = new System.Drawing.Point(984, 227);
            this.buttonKiwiMais.Name = "buttonKiwiMais";
            this.buttonKiwiMais.Size = new System.Drawing.Size(28, 23);
            this.buttonKiwiMais.TabIndex = 144;
            this.buttonKiwiMais.Text = "+";
            this.buttonKiwiMais.UseVisualStyleBackColor = true;
            this.buttonKiwiMais.Click += new System.EventHandler(this.buttonKiwiMais_Click);
            // 
            // buttonPessegoMais
            // 
            this.buttonPessegoMais.Location = new System.Drawing.Point(984, 256);
            this.buttonPessegoMais.Name = "buttonPessegoMais";
            this.buttonPessegoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPessegoMais.TabIndex = 145;
            this.buttonPessegoMais.Text = "+";
            this.buttonPessegoMais.UseVisualStyleBackColor = true;
            this.buttonPessegoMais.Click += new System.EventHandler(this.buttonPessegoMais_Click);
            // 
            // buttonMelaoMais
            // 
            this.buttonMelaoMais.Location = new System.Drawing.Point(984, 285);
            this.buttonMelaoMais.Name = "buttonMelaoMais";
            this.buttonMelaoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonMelaoMais.TabIndex = 147;
            this.buttonMelaoMais.Text = "+";
            this.buttonMelaoMais.UseVisualStyleBackColor = true;
            // 
            // buttonMelanciaMais
            // 
            this.buttonMelanciaMais.Location = new System.Drawing.Point(984, 320);
            this.buttonMelanciaMais.Name = "buttonMelanciaMais";
            this.buttonMelanciaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonMelanciaMais.TabIndex = 148;
            this.buttonMelanciaMais.Text = "+";
            this.buttonMelanciaMais.UseVisualStyleBackColor = true;
            // 
            // buttonMangaMais
            // 
            this.buttonMangaMais.Location = new System.Drawing.Point(984, 349);
            this.buttonMangaMais.Name = "buttonMangaMais";
            this.buttonMangaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonMangaMais.TabIndex = 149;
            this.buttonMangaMais.Text = "+";
            this.buttonMangaMais.UseVisualStyleBackColor = true;
            // 
            // buttonFigoMais
            // 
            this.buttonFigoMais.Location = new System.Drawing.Point(984, 375);
            this.buttonFigoMais.Name = "buttonFigoMais";
            this.buttonFigoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonFigoMais.TabIndex = 150;
            this.buttonFigoMais.Text = "+";
            this.buttonFigoMais.UseVisualStyleBackColor = true;
            // 
            // buttonAnanasMais
            // 
            this.buttonAnanasMais.Location = new System.Drawing.Point(984, 399);
            this.buttonAnanasMais.Name = "buttonAnanasMais";
            this.buttonAnanasMais.Size = new System.Drawing.Size(28, 23);
            this.buttonAnanasMais.TabIndex = 151;
            this.buttonAnanasMais.Text = "+";
            this.buttonAnanasMais.UseVisualStyleBackColor = true;
            // 
            // buttonAbacateMais
            // 
            this.buttonAbacateMais.Location = new System.Drawing.Point(984, 425);
            this.buttonAbacateMais.Name = "buttonAbacateMais";
            this.buttonAbacateMais.Size = new System.Drawing.Size(28, 23);
            this.buttonAbacateMais.TabIndex = 152;
            this.buttonAbacateMais.Text = "+";
            this.buttonAbacateMais.UseVisualStyleBackColor = true;
            // 
            // buttonMassaMais
            // 
            this.buttonMassaMais.Location = new System.Drawing.Point(1242, 116);
            this.buttonMassaMais.Name = "buttonMassaMais";
            this.buttonMassaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonMassaMais.TabIndex = 153;
            this.buttonMassaMais.Text = "+";
            this.buttonMassaMais.UseVisualStyleBackColor = true;
            // 
            // buttonYogurteMais
            // 
            this.buttonYogurteMais.Location = new System.Drawing.Point(1242, 144);
            this.buttonYogurteMais.Name = "buttonYogurteMais";
            this.buttonYogurteMais.Size = new System.Drawing.Size(28, 23);
            this.buttonYogurteMais.TabIndex = 154;
            this.buttonYogurteMais.Text = "+";
            this.buttonYogurteMais.UseVisualStyleBackColor = true;
            // 
            // buttonLeiteMais
            // 
            this.buttonLeiteMais.Location = new System.Drawing.Point(1242, 177);
            this.buttonLeiteMais.Name = "buttonLeiteMais";
            this.buttonLeiteMais.Size = new System.Drawing.Size(28, 23);
            this.buttonLeiteMais.TabIndex = 155;
            this.buttonLeiteMais.Text = "+";
            this.buttonLeiteMais.UseVisualStyleBackColor = true;
            // 
            // buttonFrutosSecosMais
            // 
            this.buttonFrutosSecosMais.Location = new System.Drawing.Point(1242, 206);
            this.buttonFrutosSecosMais.Name = "buttonFrutosSecosMais";
            this.buttonFrutosSecosMais.Size = new System.Drawing.Size(28, 23);
            this.buttonFrutosSecosMais.TabIndex = 156;
            this.buttonFrutosSecosMais.Text = "+";
            this.buttonFrutosSecosMais.UseVisualStyleBackColor = true;
            // 
            // buttonPaoMais
            // 
            this.buttonPaoMais.Location = new System.Drawing.Point(1242, 232);
            this.buttonPaoMais.Name = "buttonPaoMais";
            this.buttonPaoMais.Size = new System.Drawing.Size(28, 23);
            this.buttonPaoMais.TabIndex = 157;
            this.buttonPaoMais.Text = "+";
            this.buttonPaoMais.UseVisualStyleBackColor = true;
            this.buttonPaoMais.Click += new System.EventHandler(this.buttonPaoMais_Click);
            // 
            // buttonFrancesinhMais
            // 
            this.buttonFrancesinhMais.Location = new System.Drawing.Point(1242, 257);
            this.buttonFrancesinhMais.Name = "buttonFrancesinhMais";
            this.buttonFrancesinhMais.Size = new System.Drawing.Size(28, 23);
            this.buttonFrancesinhMais.TabIndex = 158;
            this.buttonFrancesinhMais.Text = "+";
            this.buttonFrancesinhMais.UseVisualStyleBackColor = true;
            this.buttonFrancesinhMais.Click += new System.EventHandler(this.buttonFrancesinhMais_Click);
            // 
            // buttonlulamenos
            // 
            this.buttonlulamenos.Location = new System.Drawing.Point(761, 121);
            this.buttonlulamenos.Name = "buttonlulamenos";
            this.buttonlulamenos.Size = new System.Drawing.Size(28, 23);
            this.buttonlulamenos.TabIndex = 159;
            this.buttonlulamenos.Text = "-";
            this.buttonlulamenos.UseVisualStyleBackColor = true;
            this.buttonlulamenos.Click += new System.EventHandler(this.buttonlulamenos_Click);
            // 
            // buttonsalmaomenos
            // 
            this.buttonsalmaomenos.Location = new System.Drawing.Point(761, 150);
            this.buttonsalmaomenos.Name = "buttonsalmaomenos";
            this.buttonsalmaomenos.Size = new System.Drawing.Size(28, 23);
            this.buttonsalmaomenos.TabIndex = 160;
            this.buttonsalmaomenos.Text = "-";
            this.buttonsalmaomenos.UseVisualStyleBackColor = true;
            this.buttonsalmaomenos.Click += new System.EventHandler(this.buttonsalmaomenos_Click);
            // 
            // buttonpolvomenos
            // 
            this.buttonpolvomenos.Location = new System.Drawing.Point(761, 176);
            this.buttonpolvomenos.Name = "buttonpolvomenos";
            this.buttonpolvomenos.Size = new System.Drawing.Size(28, 23);
            this.buttonpolvomenos.TabIndex = 161;
            this.buttonpolvomenos.Text = "-";
            this.buttonpolvomenos.UseVisualStyleBackColor = true;
            this.buttonpolvomenos.Click += new System.EventHandler(this.buttonpolvomenos_Click);
            // 
            // buttonsardinhasmenos
            // 
            this.buttonsardinhasmenos.Location = new System.Drawing.Point(761, 202);
            this.buttonsardinhasmenos.Name = "buttonsardinhasmenos";
            this.buttonsardinhasmenos.Size = new System.Drawing.Size(28, 23);
            this.buttonsardinhasmenos.TabIndex = 162;
            this.buttonsardinhasmenos.Text = "-";
            this.buttonsardinhasmenos.UseVisualStyleBackColor = true;
            this.buttonsardinhasmenos.Click += new System.EventHandler(this.buttonsardinhasmenos_Click);
            // 
            // buttonfiletesmenos
            // 
            this.buttonfiletesmenos.Location = new System.Drawing.Point(761, 231);
            this.buttonfiletesmenos.Name = "buttonfiletesmenos";
            this.buttonfiletesmenos.Size = new System.Drawing.Size(28, 23);
            this.buttonfiletesmenos.TabIndex = 163;
            this.buttonfiletesmenos.Text = "-";
            this.buttonfiletesmenos.UseVisualStyleBackColor = true;
            this.buttonfiletesmenos.Click += new System.EventHandler(this.buttonfiletesmenos_Click);
            // 
            // buttoncarapaumenos
            // 
            this.buttoncarapaumenos.Location = new System.Drawing.Point(761, 260);
            this.buttoncarapaumenos.Name = "buttoncarapaumenos";
            this.buttoncarapaumenos.Size = new System.Drawing.Size(28, 23);
            this.buttoncarapaumenos.TabIndex = 164;
            this.buttoncarapaumenos.Text = "-";
            this.buttoncarapaumenos.UseVisualStyleBackColor = true;
            this.buttoncarapaumenos.Click += new System.EventHandler(this.buttoncarapaumenos_Click);
            // 
            // buttondouradamenos
            // 
            this.buttondouradamenos.Location = new System.Drawing.Point(761, 289);
            this.buttondouradamenos.Name = "buttondouradamenos";
            this.buttondouradamenos.Size = new System.Drawing.Size(28, 23);
            this.buttondouradamenos.TabIndex = 165;
            this.buttondouradamenos.Text = "-";
            this.buttondouradamenos.UseVisualStyleBackColor = true;
            this.buttondouradamenos.Click += new System.EventHandler(this.buttondouradamenos_Click);
            // 
            // buttonLaranjaMenos
            // 
            this.buttonLaranjaMenos.Location = new System.Drawing.Point(1018, 116);
            this.buttonLaranjaMenos.Name = "buttonLaranjaMenos";
            this.buttonLaranjaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonLaranjaMenos.TabIndex = 166;
            this.buttonLaranjaMenos.Text = "-";
            this.buttonLaranjaMenos.UseVisualStyleBackColor = true;
            this.buttonLaranjaMenos.Click += new System.EventHandler(this.buttonLaranjaMenos_Click);
            // 
            // buttonMaçaMenos
            // 
            this.buttonMaçaMenos.Location = new System.Drawing.Point(1018, 146);
            this.buttonMaçaMenos.Name = "buttonMaçaMenos";
            this.buttonMaçaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonMaçaMenos.TabIndex = 167;
            this.buttonMaçaMenos.Text = "-";
            this.buttonMaçaMenos.UseVisualStyleBackColor = true;
            this.buttonMaçaMenos.Click += new System.EventHandler(this.buttonMaçaMenos_Click);
            // 
            // buttonPeraMenos
            // 
            this.buttonPeraMenos.Location = new System.Drawing.Point(1018, 175);
            this.buttonPeraMenos.Name = "buttonPeraMenos";
            this.buttonPeraMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPeraMenos.TabIndex = 168;
            this.buttonPeraMenos.Text = "-";
            this.buttonPeraMenos.UseVisualStyleBackColor = true;
            this.buttonPeraMenos.Click += new System.EventHandler(this.buttonPeraMenos_Click);
            // 
            // buttonBananaMenos
            // 
            this.buttonBananaMenos.Location = new System.Drawing.Point(1018, 200);
            this.buttonBananaMenos.Name = "buttonBananaMenos";
            this.buttonBananaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonBananaMenos.TabIndex = 169;
            this.buttonBananaMenos.Text = "-";
            this.buttonBananaMenos.UseVisualStyleBackColor = true;
            this.buttonBananaMenos.Click += new System.EventHandler(this.buttonBananaMenos_Click);
            // 
            // buttonKiwiMenos
            // 
            this.buttonKiwiMenos.Location = new System.Drawing.Point(1018, 223);
            this.buttonKiwiMenos.Name = "buttonKiwiMenos";
            this.buttonKiwiMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonKiwiMenos.TabIndex = 170;
            this.buttonKiwiMenos.Text = "-";
            this.buttonKiwiMenos.UseVisualStyleBackColor = true;
            this.buttonKiwiMenos.Click += new System.EventHandler(this.buttonKiwiMenos_Click);
            // 
            // buttonPessegoMenos
            // 
            this.buttonPessegoMenos.Location = new System.Drawing.Point(1018, 256);
            this.buttonPessegoMenos.Name = "buttonPessegoMenos";
            this.buttonPessegoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPessegoMenos.TabIndex = 171;
            this.buttonPessegoMenos.Text = "-";
            this.buttonPessegoMenos.UseVisualStyleBackColor = true;
            this.buttonPessegoMenos.Click += new System.EventHandler(this.buttonPessegoMenos_Click);
            // 
            // buttonMelaoMenos
            // 
            this.buttonMelaoMenos.Location = new System.Drawing.Point(1018, 285);
            this.buttonMelaoMenos.Name = "buttonMelaoMenos";
            this.buttonMelaoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonMelaoMenos.TabIndex = 173;
            this.buttonMelaoMenos.Text = "-";
            this.buttonMelaoMenos.UseVisualStyleBackColor = true;
            // 
            // buttonMelanciaMenos
            // 
            this.buttonMelanciaMenos.Location = new System.Drawing.Point(1018, 319);
            this.buttonMelanciaMenos.Name = "buttonMelanciaMenos";
            this.buttonMelanciaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonMelanciaMenos.TabIndex = 174;
            this.buttonMelanciaMenos.Text = "-";
            this.buttonMelanciaMenos.UseVisualStyleBackColor = true;
            // 
            // buttonMangaMenos
            // 
            this.buttonMangaMenos.Location = new System.Drawing.Point(1018, 349);
            this.buttonMangaMenos.Name = "buttonMangaMenos";
            this.buttonMangaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonMangaMenos.TabIndex = 175;
            this.buttonMangaMenos.Text = "-";
            this.buttonMangaMenos.UseVisualStyleBackColor = true;
            // 
            // buttonFigoMenos
            // 
            this.buttonFigoMenos.Location = new System.Drawing.Point(1018, 373);
            this.buttonFigoMenos.Name = "buttonFigoMenos";
            this.buttonFigoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonFigoMenos.TabIndex = 176;
            this.buttonFigoMenos.Text = "-";
            this.buttonFigoMenos.UseVisualStyleBackColor = true;
            // 
            // buttonAnanasMenos
            // 
            this.buttonAnanasMenos.Location = new System.Drawing.Point(1018, 401);
            this.buttonAnanasMenos.Name = "buttonAnanasMenos";
            this.buttonAnanasMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonAnanasMenos.TabIndex = 177;
            this.buttonAnanasMenos.Text = "-";
            this.buttonAnanasMenos.UseVisualStyleBackColor = true;
            // 
            // buttonAbacateMenos
            // 
            this.buttonAbacateMenos.Location = new System.Drawing.Point(1018, 425);
            this.buttonAbacateMenos.Name = "buttonAbacateMenos";
            this.buttonAbacateMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonAbacateMenos.TabIndex = 178;
            this.buttonAbacateMenos.Text = "-";
            this.buttonAbacateMenos.UseVisualStyleBackColor = true;
            // 
            // buttonMassaMenos
            // 
            this.buttonMassaMenos.Location = new System.Drawing.Point(1276, 116);
            this.buttonMassaMenos.Name = "buttonMassaMenos";
            this.buttonMassaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonMassaMenos.TabIndex = 179;
            this.buttonMassaMenos.Text = "-";
            this.buttonMassaMenos.UseVisualStyleBackColor = true;
            // 
            // buttonYogurteMenos
            // 
            this.buttonYogurteMenos.Location = new System.Drawing.Point(1276, 141);
            this.buttonYogurteMenos.Name = "buttonYogurteMenos";
            this.buttonYogurteMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonYogurteMenos.TabIndex = 180;
            this.buttonYogurteMenos.Text = "-";
            this.buttonYogurteMenos.UseVisualStyleBackColor = true;
            // 
            // buttonLeiteMenos
            // 
            this.buttonLeiteMenos.Location = new System.Drawing.Point(1276, 177);
            this.buttonLeiteMenos.Name = "buttonLeiteMenos";
            this.buttonLeiteMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonLeiteMenos.TabIndex = 181;
            this.buttonLeiteMenos.Text = "-";
            this.buttonLeiteMenos.UseVisualStyleBackColor = true;
            // 
            // buttonFrutosSecosMenos
            // 
            this.buttonFrutosSecosMenos.Location = new System.Drawing.Point(1276, 206);
            this.buttonFrutosSecosMenos.Name = "buttonFrutosSecosMenos";
            this.buttonFrutosSecosMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonFrutosSecosMenos.TabIndex = 182;
            this.buttonFrutosSecosMenos.Text = "-";
            this.buttonFrutosSecosMenos.UseVisualStyleBackColor = true;
            // 
            // buttonPaoMenos
            // 
            this.buttonPaoMenos.Location = new System.Drawing.Point(1276, 233);
            this.buttonPaoMenos.Name = "buttonPaoMenos";
            this.buttonPaoMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonPaoMenos.TabIndex = 183;
            this.buttonPaoMenos.Text = "-";
            this.buttonPaoMenos.UseVisualStyleBackColor = true;
            this.buttonPaoMenos.Click += new System.EventHandler(this.buttonPaoMenos_Click);
            // 
            // buttonFrancesinhaMenos
            // 
            this.buttonFrancesinhaMenos.Location = new System.Drawing.Point(1276, 257);
            this.buttonFrancesinhaMenos.Name = "buttonFrancesinhaMenos";
            this.buttonFrancesinhaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonFrancesinhaMenos.TabIndex = 184;
            this.buttonFrancesinhaMenos.Text = "-";
            this.buttonFrancesinhaMenos.UseVisualStyleBackColor = true;
            this.buttonFrancesinhaMenos.Click += new System.EventHandler(this.buttonFrancesinhaMenos_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1182, 316);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(26, 20);
            this.textBox1.TabIndex = 186;
            this.textBox1.Text = "0";
            // 
            // buttonLasanhaMais
            // 
            this.buttonLasanhaMais.Location = new System.Drawing.Point(1242, 314);
            this.buttonLasanhaMais.Name = "buttonLasanhaMais";
            this.buttonLasanhaMais.Size = new System.Drawing.Size(28, 23);
            this.buttonLasanhaMais.TabIndex = 187;
            this.buttonLasanhaMais.Text = "+";
            this.buttonLasanhaMais.UseVisualStyleBackColor = true;
            // 
            // buttonLasanhaMenos
            // 
            this.buttonLasanhaMenos.Location = new System.Drawing.Point(1276, 313);
            this.buttonLasanhaMenos.Name = "buttonLasanhaMenos";
            this.buttonLasanhaMenos.Size = new System.Drawing.Size(28, 23);
            this.buttonLasanhaMenos.TabIndex = 188;
            this.buttonLasanhaMenos.Text = "-";
            this.buttonLasanhaMenos.UseVisualStyleBackColor = true;
            // 
            // labelVoltar
            // 
            this.labelVoltar.BackColor = System.Drawing.Color.Transparent;
            this.labelVoltar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelVoltar.Location = new System.Drawing.Point(-1, 544);
            this.labelVoltar.Name = "labelVoltar";
            this.labelVoltar.Size = new System.Drawing.Size(219, 81);
            this.labelVoltar.TabIndex = 189;
            this.labelVoltar.Click += new System.EventHandler(this.labelVoltar_Click);
            // 
            // labelTabelaCalorias
            // 
            this.labelTabelaCalorias.BackColor = System.Drawing.Color.Transparent;
            this.labelTabelaCalorias.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelTabelaCalorias.Location = new System.Drawing.Point(224, 544);
            this.labelTabelaCalorias.Name = "labelTabelaCalorias";
            this.labelTabelaCalorias.Size = new System.Drawing.Size(301, 81);
            this.labelTabelaCalorias.TabIndex = 190;
            this.labelTabelaCalorias.Click += new System.EventHandler(this.labelTabelaCalorias_Click);
            // 
            // labelCalcular
            // 
            this.labelCalcular.BackColor = System.Drawing.Color.Transparent;
            this.labelCalcular.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelCalcular.Location = new System.Drawing.Point(805, 516);
            this.labelCalcular.Name = "labelCalcular";
            this.labelCalcular.Size = new System.Drawing.Size(151, 55);
            this.labelCalcular.TabIndex = 191;
            this.labelCalcular.Click += new System.EventHandler(this.labelCalcular_Click);
            // 
            // View_Calorias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Aplicação_HabitosAlimentares.Properties.Resources.paulo10_1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1307, 623);
            this.Controls.Add(this.labelCalcular);
            this.Controls.Add(this.labelTabelaCalorias);
            this.Controls.Add(this.labelVoltar);
            this.Controls.Add(this.buttonLasanhaMenos);
            this.Controls.Add(this.buttonLasanhaMais);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonFrancesinhaMenos);
            this.Controls.Add(this.buttonPaoMenos);
            this.Controls.Add(this.buttonFrutosSecosMenos);
            this.Controls.Add(this.buttonLeiteMenos);
            this.Controls.Add(this.buttonYogurteMenos);
            this.Controls.Add(this.buttonMassaMenos);
            this.Controls.Add(this.buttonAbacateMenos);
            this.Controls.Add(this.buttonAnanasMenos);
            this.Controls.Add(this.buttonFigoMenos);
            this.Controls.Add(this.buttonMangaMenos);
            this.Controls.Add(this.buttonMelanciaMenos);
            this.Controls.Add(this.buttonMelaoMenos);
            this.Controls.Add(this.buttonPessegoMenos);
            this.Controls.Add(this.buttonKiwiMenos);
            this.Controls.Add(this.buttonBananaMenos);
            this.Controls.Add(this.buttonPeraMenos);
            this.Controls.Add(this.buttonMaçaMenos);
            this.Controls.Add(this.buttonLaranjaMenos);
            this.Controls.Add(this.buttondouradamenos);
            this.Controls.Add(this.buttoncarapaumenos);
            this.Controls.Add(this.buttonfiletesmenos);
            this.Controls.Add(this.buttonsardinhasmenos);
            this.Controls.Add(this.buttonpolvomenos);
            this.Controls.Add(this.buttonsalmaomenos);
            this.Controls.Add(this.buttonlulamenos);
            this.Controls.Add(this.buttonFrancesinhMais);
            this.Controls.Add(this.buttonPaoMais);
            this.Controls.Add(this.buttonFrutosSecosMais);
            this.Controls.Add(this.buttonLeiteMais);
            this.Controls.Add(this.buttonYogurteMais);
            this.Controls.Add(this.buttonMassaMais);
            this.Controls.Add(this.buttonAbacateMais);
            this.Controls.Add(this.buttonAnanasMais);
            this.Controls.Add(this.buttonFigoMais);
            this.Controls.Add(this.buttonMangaMais);
            this.Controls.Add(this.buttonMelanciaMais);
            this.Controls.Add(this.buttonMelaoMais);
            this.Controls.Add(this.buttonPessegoMais);
            this.Controls.Add(this.buttonKiwiMais);
            this.Controls.Add(this.buttonBananaMais);
            this.Controls.Add(this.buttonPeraMais);
            this.Controls.Add(this.buttonMaçaMais);
            this.Controls.Add(this.buttonLaranjaMais);
            this.Controls.Add(this.buttondouradamais);
            this.Controls.Add(this.buttoncarapaumais);
            this.Controls.Add(this.buttonfiletesmais);
            this.Controls.Add(this.buttonsardinhasmais);
            this.Controls.Add(this.buttonpolvomais);
            this.Controls.Add(this.buttonsalmaomais);
            this.Controls.Add(this.buttonlulamais);
            this.Controls.Add(this.textBoxfrancesinha);
            this.Controls.Add(this.textBoxpao);
            this.Controls.Add(this.textBoxfrutossecos);
            this.Controls.Add(this.textBoxleite);
            this.Controls.Add(this.textBoxyogurte);
            this.Controls.Add(this.textBoxmassa);
            this.Controls.Add(this.textBoxabacate);
            this.Controls.Add(this.textBoxananas);
            this.Controls.Add(this.textBoxfigo);
            this.Controls.Add(this.textBoxmanga);
            this.Controls.Add(this.textBoxmelancia);
            this.Controls.Add(this.textBoxmelao);
            this.Controls.Add(this.textBoxpessego);
            this.Controls.Add(this.textBoxkiwi);
            this.Controls.Add(this.textBoxbanana);
            this.Controls.Add(this.textBoxpera);
            this.Controls.Add(this.textBoxmaca);
            this.Controls.Add(this.textBoxlaranja);
            this.Controls.Add(this.textBoxdourada);
            this.Controls.Add(this.textBoxcarapau);
            this.Controls.Add(this.textBoxfiletes);
            this.Controls.Add(this.textBoxsardinhas);
            this.Controls.Add(this.textBoxpolvo);
            this.Controls.Add(this.textBoxsalmao);
            this.Controls.Add(this.textBoxlula);
            this.Controls.Add(this.buttonPorcoMenos);
            this.Controls.Add(this.buttonFrangoMenos);
            this.Controls.Add(this.buttonPorcoMais);
            this.Controls.Add(this.buttonFrangoMais);
            this.Controls.Add(this.textBoxPorco);
            this.Controls.Add(this.textBoxFrango);
            this.Controls.Add(this.buttonFebraMais);
            this.Controls.Add(this.buttonFebraMenos);
            this.Controls.Add(this.buttonPeruMenos);
            this.Controls.Add(this.buttonBifeMenos);
            this.Controls.Add(this.buttonArrozMenos);
            this.Controls.Add(this.buttonErvilhasMenos);
            this.Controls.Add(this.buttonBroculosMenos);
            this.Controls.Add(this.buttonCenouraMenos);
            this.Controls.Add(this.buttonEspinafreMenos);
            this.Controls.Add(this.buttonPimentoMenos);
            this.Controls.Add(this.buttonPepinoMenos);
            this.Controls.Add(this.buttonBatataMenos);
            this.Controls.Add(this.buttonAlfaceMenos);
            this.Controls.Add(this.buttonPeruMais);
            this.Controls.Add(this.buttonBifeMais);
            this.Controls.Add(this.buttonErvilhasMais);
            this.Controls.Add(this.buttonArrozMais);
            this.Controls.Add(this.buttonCenouraMais);
            this.Controls.Add(this.buttonBroculosMais);
            this.Controls.Add(this.buttonespinafreMais);
            this.Controls.Add(this.buttonPimentoMais);
            this.Controls.Add(this.buttonPepinoMais);
            this.Controls.Add(this.buttonBatataMais);
            this.Controls.Add(this.buttonAlfaceMais);
            this.Controls.Add(this.buttonTomateMenos);
            this.Controls.Add(this.textBoxFebra);
            this.Controls.Add(this.textBoxperu);
            this.Controls.Add(this.textBoxbife);
            this.Controls.Add(this.textBoxArroz);
            this.Controls.Add(this.labelcalorias);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxervilhas);
            this.Controls.Add(this.textBoxbroculos);
            this.Controls.Add(this.textBoxcenoura);
            this.Controls.Add(this.textBoxespinafre);
            this.Controls.Add(this.textBoxpimento);
            this.Controls.Add(this.textBoxPepino);
            this.Controls.Add(this.textBoxBatata);
            this.Controls.Add(this.textBoxAlface);
            this.Controls.Add(this.textTomate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "View_Calorias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View_Calorias";
            this.Load += new System.EventHandler(this.View_Calorias_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textTomate;
        private System.Windows.Forms.TextBox textBoxAlface;
        private System.Windows.Forms.TextBox textBoxBatata;
        private System.Windows.Forms.TextBox textBoxPepino;
        private System.Windows.Forms.TextBox textBoxpimento;
        private System.Windows.Forms.TextBox textBoxespinafre;
        private System.Windows.Forms.TextBox textBoxcenoura;
        private System.Windows.Forms.TextBox textBoxbroculos;
        private System.Windows.Forms.TextBox textBoxervilhas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelcalorias;
        private System.Windows.Forms.TextBox textBoxArroz;
        private System.Windows.Forms.TextBox textBoxbife;
        private System.Windows.Forms.TextBox textBoxperu;
        private System.Windows.Forms.TextBox textBoxFebra;
        private System.Windows.Forms.Button buttonTomateMenos;
        private System.Windows.Forms.Button buttonAlfaceMais;
        private System.Windows.Forms.Button buttonBatataMais;
        private System.Windows.Forms.Button buttonPepinoMais;
        private System.Windows.Forms.Button buttonPimentoMais;
        private System.Windows.Forms.Button buttonespinafreMais;
        private System.Windows.Forms.Button buttonBroculosMais;
        private System.Windows.Forms.Button buttonCenouraMais;
        private System.Windows.Forms.Button buttonArrozMais;
        private System.Windows.Forms.Button buttonErvilhasMais;
        private System.Windows.Forms.Button buttonBifeMais;
        private System.Windows.Forms.Button buttonPeruMais;
        private System.Windows.Forms.Button buttonAlfaceMenos;
        private System.Windows.Forms.Button buttonBatataMenos;
        private System.Windows.Forms.Button buttonPepinoMenos;
        private System.Windows.Forms.Button buttonPimentoMenos;
        private System.Windows.Forms.Button buttonEspinafreMenos;
        private System.Windows.Forms.Button buttonCenouraMenos;
        private System.Windows.Forms.Button buttonBroculosMenos;
        private System.Windows.Forms.Button buttonErvilhasMenos;
        private System.Windows.Forms.Button buttonArrozMenos;
        private System.Windows.Forms.Button buttonBifeMenos;
        private System.Windows.Forms.Button buttonPeruMenos;
        private System.Windows.Forms.Button buttonFebraMenos;
        private System.Windows.Forms.Button buttonFebraMais;
        private System.Windows.Forms.TextBox textBoxFrango;
        private System.Windows.Forms.TextBox textBoxPorco;
        private System.Windows.Forms.Button buttonFrangoMais;
        private System.Windows.Forms.Button buttonPorcoMais;
        private System.Windows.Forms.Button buttonFrangoMenos;
        private System.Windows.Forms.Button buttonPorcoMenos;
        private System.Windows.Forms.TextBox textBoxlula;
        private System.Windows.Forms.TextBox textBoxsalmao;
        private System.Windows.Forms.TextBox textBoxpolvo;
        private System.Windows.Forms.TextBox textBoxsardinhas;
        private System.Windows.Forms.TextBox textBoxfiletes;
        private System.Windows.Forms.TextBox textBoxcarapau;
        private System.Windows.Forms.TextBox textBoxdourada;
        private System.Windows.Forms.TextBox textBoxlaranja;
        private System.Windows.Forms.TextBox textBoxmaca;
        private System.Windows.Forms.TextBox textBoxpera;
        private System.Windows.Forms.TextBox textBoxbanana;
        private System.Windows.Forms.TextBox textBoxkiwi;
        private System.Windows.Forms.TextBox textBoxpessego;
        private System.Windows.Forms.TextBox textBoxmelao;
        private System.Windows.Forms.TextBox textBoxmelancia;
        private System.Windows.Forms.TextBox textBoxmanga;
        private System.Windows.Forms.TextBox textBoxfigo;
        private System.Windows.Forms.TextBox textBoxananas;
        private System.Windows.Forms.TextBox textBoxabacate;
        private System.Windows.Forms.TextBox textBoxmassa;
        private System.Windows.Forms.TextBox textBoxyogurte;
        private System.Windows.Forms.TextBox textBoxleite;
        private System.Windows.Forms.TextBox textBoxfrutossecos;
        private System.Windows.Forms.TextBox textBoxpao;
        private System.Windows.Forms.TextBox textBoxfrancesinha;
        private System.Windows.Forms.Button buttonlulamais;
        private System.Windows.Forms.Button buttonsalmaomais;
        private System.Windows.Forms.Button buttonpolvomais;
        private System.Windows.Forms.Button buttonsardinhasmais;
        private System.Windows.Forms.Button buttonfiletesmais;
        private System.Windows.Forms.Button buttoncarapaumais;
        private System.Windows.Forms.Button buttondouradamais;
        private System.Windows.Forms.Button buttonLaranjaMais;
        private System.Windows.Forms.Button buttonMaçaMais;
        private System.Windows.Forms.Button buttonPeraMais;
        private System.Windows.Forms.Button buttonBananaMais;
        private System.Windows.Forms.Button buttonKiwiMais;
        private System.Windows.Forms.Button buttonPessegoMais;
        private System.Windows.Forms.Button buttonMelaoMais;
        private System.Windows.Forms.Button buttonMelanciaMais;
        private System.Windows.Forms.Button buttonMangaMais;
        private System.Windows.Forms.Button buttonFigoMais;
        private System.Windows.Forms.Button buttonAnanasMais;
        private System.Windows.Forms.Button buttonAbacateMais;
        private System.Windows.Forms.Button buttonMassaMais;
        private System.Windows.Forms.Button buttonYogurteMais;
        private System.Windows.Forms.Button buttonLeiteMais;
        private System.Windows.Forms.Button buttonFrutosSecosMais;
        private System.Windows.Forms.Button buttonPaoMais;
        private System.Windows.Forms.Button buttonFrancesinhMais;
        private System.Windows.Forms.Button buttonlulamenos;
        private System.Windows.Forms.Button buttonsalmaomenos;
        private System.Windows.Forms.Button buttonpolvomenos;
        private System.Windows.Forms.Button buttonsardinhasmenos;
        private System.Windows.Forms.Button buttonfiletesmenos;
        private System.Windows.Forms.Button buttoncarapaumenos;
        private System.Windows.Forms.Button buttondouradamenos;
        private System.Windows.Forms.Button buttonLaranjaMenos;
        private System.Windows.Forms.Button buttonMaçaMenos;
        private System.Windows.Forms.Button buttonPeraMenos;
        private System.Windows.Forms.Button buttonBananaMenos;
        private System.Windows.Forms.Button buttonKiwiMenos;
        private System.Windows.Forms.Button buttonPessegoMenos;
        private System.Windows.Forms.Button buttonMelaoMenos;
        private System.Windows.Forms.Button buttonMelanciaMenos;
        private System.Windows.Forms.Button buttonMangaMenos;
        private System.Windows.Forms.Button buttonFigoMenos;
        private System.Windows.Forms.Button buttonAnanasMenos;
        private System.Windows.Forms.Button buttonAbacateMenos;
        private System.Windows.Forms.Button buttonMassaMenos;
        private System.Windows.Forms.Button buttonYogurteMenos;
        private System.Windows.Forms.Button buttonLeiteMenos;
        private System.Windows.Forms.Button buttonFrutosSecosMenos;
        private System.Windows.Forms.Button buttonPaoMenos;
        private System.Windows.Forms.Button buttonFrancesinhaMenos;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonLasanhaMais;
        private System.Windows.Forms.Button buttonLasanhaMenos;
        private System.Windows.Forms.Label labelVoltar;
        private System.Windows.Forms.Label labelTabelaCalorias;
        private System.Windows.Forms.Label labelCalcular;
    }
}