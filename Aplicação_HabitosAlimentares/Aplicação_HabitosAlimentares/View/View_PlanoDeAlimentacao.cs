﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_Calorias : Form
    {
        public double calorias=0;
        public double caloriasAlvo = 0;
        public View_Calorias()
        {
            InitializeComponent();
        }

        private void View_Calorias_Load(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.Hide();
            //Program.V_Plano();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
           
        }

        private void textTomate_TextChanged(object sender, EventArgs e)
        {
           if((textTomate.Text) == "")
            {
                textTomate.Text = "0";
       
            }
        }

        private void buttonCalcular_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            textTomate.Text = Int32.Parse(textTomate.Text) + 100 + "";
        }

        private void textBoxAlface_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxAlface.Text) == "")
            {
                textBoxAlface.Text = "0";

            }
        }

        private void textBoxBatata_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxBatata.Text) == "")
            {
                textBoxBatata.Text = "0";

            }
        }

        private void textBoxPepino_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxPepino.Text) == "")
            {
                textBoxPepino.Text = "0";

            }
        }

        private void textBoxpimento_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxpimento.Text) == "")
            {
                textBoxpimento.Text = "0";

            }
        }

        private void textBoxespinafre_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxespinafre.Text) == "")
            {
                textBoxespinafre.Text = "0";

            }
        }

        private void textBoxcenoura_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxcenoura.Text) == "")
            {
                textBoxcenoura.Text = "0";

            }
        }

        private void textBoxbroculos_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxbroculos.Text) == "")
            {
                textBoxbroculos.Text = "0";

            }
        }

        private void textBoxervilhas_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxervilhas.Text) == "")
            {
                textBoxervilhas.Text = "0";

            }
        }

        private void textBoxArroz_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxArroz.Text) == "")
            {
                textBoxArroz.Text = "0";

            }
        }

        private void textBoxbife_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxbife.Text) == "")
            {
                textBoxbife.Text = "0";

            }
        }

        private void textBoxperu_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxperu.Text) == "")
            {
                textBoxperu.Text = "0";

            }
        }

        private void textBoxfebra_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxFebra.Text) == "")
            {
                textBoxFebra.Text = "0";

            }
        }

        private void buttonTomate_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textTomate.Text) >= 100)
            {
                textTomate.Text = Int32.Parse(textTomate.Text) - 100 + "";
            }
            else
            {
                textTomate.Text = "0";
            }
        }

        private void buttonAlfaceMais_Click(object sender, EventArgs e)
        {
                textBoxAlface.Text = Int32.Parse(textBoxAlface.Text) + 200 + "";

        }

        private void buttonAlfaceMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxAlface.Text) >= 200)
            {
                textBoxAlface.Text = Int32.Parse(textBoxAlface.Text) - 200 + "";
            }
            else
            {
                textBoxAlface.Text = "0";
            }
        }

        private void buttonBatataMais_Click(object sender, EventArgs e)
        {
            textBoxBatata.Text = Int32.Parse(textBoxBatata.Text) + 355 + "";
        }

        private void buttonBatataMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxBatata.Text) >= 355)
            {
                textBoxBatata.Text = Int32.Parse(textBoxBatata.Text) - 355 + "";
            }
            else
            {
                textBoxBatata.Text = "0";
            }
        }

        private void buttonPepinoMais_Click(object sender, EventArgs e)
        {
            textBoxPepino.Text = Int32.Parse(textBoxPepino.Text) + 100 + "";
        }

        private void buttonPepinoMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxPepino.Text) >= 100)
            {
                textBoxPepino.Text = Int32.Parse(textBoxPepino.Text) - 100 + "";
            }
            else
            {
                textBoxPepino.Text = "0";
            }
        }

        private void buttonPimentoMais_Click(object sender, EventArgs e)
        {
            textBoxpimento.Text = Int32.Parse(textBoxpimento.Text) + 100 + "";
        }

        private void buttonPimentoMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxpimento.Text) >= 100)
            {
                textBoxpimento.Text = Int32.Parse(textBoxpimento.Text) - 100 + "";
            }
            else
            {
                textBoxpimento.Text = "0";
            }
        }

        private void buttonespinafreMais_Click(object sender, EventArgs e)
        {
            textBoxespinafre.Text = Int32.Parse(textBoxespinafre.Text) + 285 + "";
        }

        private void buttonEspinafreMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxespinafre.Text) >= 285)
            {
                textBoxespinafre.Text = Int32.Parse(textBoxespinafre.Text) - 285 + "";
            }
            else
            {
                textBoxespinafre.Text = "0";
            }
        }

        private void buttonCenouraMais_Click(object sender, EventArgs e)
        {
            textBoxcenoura.Text = Int32.Parse(textBoxcenoura.Text) + 120 + "";
        }

        private void buttonCenouraMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxcenoura.Text) >= 120)
            {
                textBoxcenoura.Text = Int32.Parse(textBoxcenoura.Text) - 120 + "";
            }
            else
            {
                textBoxcenoura.Text = "0";
            }
        }

        private void buttonBroculosMais_Click(object sender, EventArgs e)
        {
            textBoxbroculos.Text = Int32.Parse(textBoxbroculos.Text) + 100 + "";
        }

        private void buttonBroculosMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxbroculos.Text) >= 100)
            {
                textBoxbroculos.Text = Int32.Parse(textBoxbroculos.Text) - 100 + "";
            }
            else
            {
                textBoxbroculos.Text = "0";
            }
        }

        private void buttonErvilhasMais_Click(object sender, EventArgs e)
        {
            textBoxervilhas.Text = Int32.Parse(textBoxervilhas.Text) + 100 + "";
        }

        private void buttonErvilhasMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxervilhas.Text) >= 100)
            {
                textBoxervilhas.Text = Int32.Parse(textBoxervilhas.Text) - 100 + "";
            }
            else
            {
                textBoxervilhas.Text = "0";
            }
        }

        private void buttonBifeMais_Click(object sender, EventArgs e)
        {
            textBoxbife.Text = Int32.Parse(textBoxbife.Text) + 85 + "";
        }

        private void buttonBifeMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxbife.Text) >= 85)
            {
                textBoxbife.Text = Int32.Parse(textBoxbife.Text) - 85 + "";
            }
            else
            {
                textBoxbife.Text = "0";
            }
        }

        private void buttonPeruMais_Click(object sender, EventArgs e)
        {
            textBoxperu.Text = Int32.Parse(textBoxperu.Text) + 450 + "";
        }

        private void buttonPeruMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxperu.Text) >= 450)
            {
                textBoxperu.Text = Int32.Parse(textBoxperu.Text) - 450 + "";
            }
            else
            {
                textBoxperu.Text = "0";
            }
        }

        private void buttonFebraMais_Click(object sender, EventArgs e)
        {
            textBoxFebra.Text = Int32.Parse(textBoxFebra.Text) + 150 + "";
        }

        private void buttonFebraMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxFebra.Text) >= 450)
            {
                textBoxFebra.Text = Int32.Parse(textBoxFebra.Text) - 150 + "";
            }
            else
            {
                textBoxFebra.Text = "0";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBoxlula.Text = Int32.Parse(textBoxlula.Text) + 57 + "";
        }

        private void buttonFrangoMais_Click(object sender, EventArgs e)
        {
            textBoxFrango.Text = Int32.Parse(textBoxFrango.Text) + 239 + "";
        }

        private void buttonFrangoMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxFrango.Text) >= 239)
            {
                textBoxFrango.Text = Int32.Parse(textBoxFrango.Text) - 239 + "";
            }
            else
            {
                textBoxFrango.Text = "0";
            }
        }

        private void buttonPorcoMais_Click(object sender, EventArgs e)
        {
            textBoxPorco.Text = Int32.Parse(textBoxPorco.Text) + 242 + "";
        }

        private void buttonPorcoMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxPorco.Text) >= 239)
            {
                textBoxPorco.Text = Int32.Parse(textBoxPorco.Text) - 239 + "";
            }
            else
            {
                textBoxPorco.Text = "0";
            }
        }

        private void textBoxFrango_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxFrango.Text) == "")
            {
                textBoxFrango.Text = "0";

            }
        }

        private void textBoxPorco_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxPorco.Text) == "")
            {
                textBoxPorco.Text = "0";

            }
        }

        private void textBoxlula_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxlula.Text) == "")
            {
                textBoxlula.Text = "0";

            }
        }

        private void textBoxsalmao_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxsalmao.Text) == "")
            {
                textBoxsalmao.Text = "0";

            }
        }

        private void textBoxpolvo_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxpolvo.Text) == "")
            {
                textBoxpolvo.Text = "0";

            }
        }

        private void textBoxsardinhas_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxsardinhas.Text) == "")
            {
                textBoxsardinhas.Text = "0";

            }
        }

        private void textBoxfiletes_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxfiletes.Text) == "")
            {
                textBoxfiletes.Text = "0";

            }
        }

        private void textBoxcarapau_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxcarapau.Text) == "")
            {
                textBoxcarapau.Text = "0";

            }
        }

        private void textBoxdourada_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxdourada.Text) == "")
            {
                textBoxdourada.Text = "0";

            }
        }

        private void textBoxlaranja_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxlaranja.Text) == "")
            {
                textBoxlaranja.Text = "0";

            }
        }

        private void textBoxmaca_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxmaca.Text) == "")
            {
                textBoxmaca.Text = "0";

            }
        }

        private void textBoxpera_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxpera.Text) == "")
            {
                textBoxpera.Text = "0";

            }
        }

        private void textBoxbanana_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxbanana.Text) == "")
            {
                textBoxbanana.Text = "0";

            }
        }

        private void textBoxkiwi_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxkiwi.Text) == "")
            {
                textBoxkiwi.Text = "0";

            }
        }

        private void textBoxpessego_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxpessego.Text) == "")
            {
                textBoxpessego.Text = "0";

            }
        }

        private void textBoxmanga_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxmanga.Text) == "")
            {
                textBoxmanga.Text = "0";

            }
        }

        private void textBoxmassa_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxmassa.Text) == "")
            {
                textBoxmassa.Text = "0";

            }
        }

        private void textBoxyogurte_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxyogurte.Text) == "")
            {
                textBoxyogurte.Text = "0";

            }
        }

        private void textBoxleite_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxleite.Text) == "")
            {
                textBoxleite.Text = "0";

            }
        }

        private void textBoxpao_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxpao.Text) == "")
            {
                textBoxpao.Text = "0";

            }
        }

        private void textBoxfrancesinha_TextChanged(object sender, EventArgs e)
        {
            if ((textBoxfrancesinha.Text) == "")
            {
                textBoxfrancesinha.Text = "0";

            }
        }

        private void buttonFrancesinhMais_Click(object sender, EventArgs e)
        {
            textBoxfrancesinha.Text = Int32.Parse(textBoxfrancesinha.Text) + 752 + "";
        }

        private void buttonFrancesinhaMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxfrancesinha.Text) >= 752)
            {
                textBoxfrancesinha.Text = Int32.Parse(textBoxfrancesinha.Text) - 752 + "";
            }
            else
            {
                textBoxfrancesinha.Text = "0";
            }
        }

        private void buttonLaranjaMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxlula.Text) >= 57)
            {
                textBoxlula.Text = Int32.Parse(textBoxlula.Text) - 57 + "";
            }
            else
            {
                textBoxlula.Text = "0";
            }
        }

        private void buttonsalmaomais_Click(object sender, EventArgs e)
        {
            textBoxsalmao.Text = Int32.Parse(textBoxsalmao.Text) + 85 + "";
        }

        private void buttonsalmaomenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxsalmao.Text) >= 85)
            {
                textBoxsalmao.Text = Int32.Parse(textBoxsalmao.Text) - 85 + "";
            }
            else
            {
                textBoxsalmao.Text = "0";
            }
        }

        private void buttonpolvomais_Click(object sender, EventArgs e)
        {
            textBoxsalmao.Text = Int32.Parse(textBoxsalmao.Text) + 300 + "";
        }

        private void buttonpolvomenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxpolvo.Text) >= 57)
            {
                textBoxpolvo.Text = Int32.Parse(textBoxpolvo.Text) - 57 + "";
            }
            else
            {
                textBoxpolvo.Text = "0";
            }
        }

        private void buttonLaranjaMais_Click(object sender, EventArgs e)
        {
            textBoxsalmao.Text = Int32.Parse(textBoxsalmao.Text) + 57 + "";
        }

        private void buttonlulamenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxlula.Text) >= 57)
            {
                textBoxlula.Text = Int32.Parse(textBoxlula.Text) - 57 + "";
            }
            else
            {
                textBoxlula.Text = "0";
            }
        }

        private void buttondouradamais_Click(object sender, EventArgs e)
        {
            textBoxdourada.Text = Int32.Parse(textBoxdourada.Text) + 92 + "";
        }

        private void buttonsardinhasmais_Click(object sender, EventArgs e)
        {
            textBoxsardinhas.Text = Int32.Parse(textBoxsardinhas.Text) + 92 + "";
        }

        private void buttonsardinhasmenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxsardinhas.Text) >= 92)
            {
                textBoxsardinhas.Text = Int32.Parse(textBoxsardinhas.Text) - 92 + "";
            }
            else
            {
                textBoxsardinhas.Text = "0";
            }
        }

        private void buttonfiletesmais_Click(object sender, EventArgs e)
        {
            textBoxfiletes.Text = Int32.Parse(textBoxfiletes.Text) + 85 + "";
        }

        private void buttonfiletesmenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxfiletes.Text) >= 85)
            {
                textBoxfiletes.Text = Int32.Parse(textBoxfiletes.Text) - 85 + "";
            }
            else
            {
                textBoxfiletes.Text = "0";
            }
        }

        private void buttoncarapaumais_Click(object sender, EventArgs e)
        {
            textBoxcarapau.Text = Int32.Parse(textBoxcarapau.Text) + 88 + "";
        }

        private void buttoncarapaumenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxcarapau.Text) >= 88)
            {
                textBoxcarapau.Text = Int32.Parse(textBoxcarapau.Text) - 88 + "";
            }
            else
            {
                textBoxcarapau.Text = "0";
            }
        }

        private void buttondouradamenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxdourada.Text) >= 92)
            {
                textBoxdourada.Text = Int32.Parse(textBoxdourada.Text) - 92 + "";
            }
            else
            {
                textBoxdourada.Text = "0";
            }
        }

        private void buttonMaçaMais_Click(object sender, EventArgs e)
        {
            textBoxmaca.Text = Int32.Parse(textBoxmaca.Text) + 182 + "";
        }

        private void buttonMaçaMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxmaca.Text) >= 182)
            {
                textBoxmaca.Text = Int32.Parse(textBoxmaca.Text) - 182 + "";
            }
            else
            {
                textBoxmaca.Text = "0";
            }
        }

        private void buttonPeraMais_Click(object sender, EventArgs e)
        {
            textBoxpera.Text = Int32.Parse(textBoxpera.Text) + 178 + "";
        }

        private void buttonPeraMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxpera.Text) >= 178)
            {
                textBoxpera.Text = Int32.Parse(textBoxpera.Text) - 178 + "";
            }
            else
            {
                textBoxpera.Text = "0";
            }
        }

        private void buttonBananaMais_Click(object sender, EventArgs e)
        {
            textBoxbanana.Text = Int32.Parse(textBoxbanana.Text) + 118 + "";
        }

        private void buttonBananaMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxbanana.Text) >= 118)
            {
                textBoxbanana.Text = Int32.Parse(textBoxbanana.Text) - 118 + "";
            }
            else
            {
                textBoxbanana.Text = "0";
            }
        }

        private void buttonKiwiMais_Click(object sender, EventArgs e)
        {
            textBoxkiwi.Text = Int32.Parse(textBoxkiwi.Text) + 69 + "";
        }

        private void buttonKiwiMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxkiwi.Text) >= 69)
            {
                textBoxkiwi.Text = Int32.Parse(textBoxkiwi.Text) - 69 + "";
            }
            else
            {
                textBoxkiwi.Text = "0";
            }
        }

        private void buttonPessegoMais_Click(object sender, EventArgs e)
        {
            textBoxpessego.Text = Int32.Parse(textBoxpessego.Text) + 150 + "";
        }

        private void buttonPessegoMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxpessego.Text) >= 69)
            {
                textBoxpessego.Text = Int32.Parse(textBoxpessego.Text) - 69 + "";
            }
            else
            {
                textBoxpessego.Text = "0";
            }
        }

        private void buttonPaoMais_Click(object sender, EventArgs e)
        {
            textBoxpao.Text = Int32.Parse(textBoxpao.Text) + 50 + "";
        }

        private void buttonPaoMenos_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBoxpao.Text) >= 50)
            {
                textBoxpao.Text = Int32.Parse(textBoxpao.Text) - 50 + "";
            }
            else
            {
                textBoxpao.Text = "0";
            }
        }

        private void buttonArrozMais_Click(object sender, EventArgs e)
        {

        }

        private void labelCalcular_Click(object sender, EventArgs e)
        {
            calorias = 0;
            caloriasAlvo = 0;
            calorias += Int32.Parse(textTomate.Text) * 0.2;
            calorias += Int32.Parse(textBoxAlface.Text) * 0.15;
            calorias += Int32.Parse(textBoxBatata.Text) * 0.86;
            calorias += Int32.Parse(textBoxPepino.Text) * 0.16;
            calorias += Int32.Parse(textBoxpimento.Text) * 0.26;
            calorias += Int32.Parse(textBoxespinafre.Text) * 0.23;
            calorias += Int32.Parse(textBoxcenoura.Text) * 0.41;
            calorias += Int32.Parse(textBoxbroculos.Text) * 0.54;
            calorias += Int32.Parse(textBoxervilhas.Text) * 0.81;
            calorias += Int32.Parse(textBoxArroz.Text) * 1.30;
            calorias += Int32.Parse(textBoxbife.Text) * 2.71;
            calorias += Int32.Parse(textBoxperu.Text) * 1.89;
            calorias += Int32.Parse(textBoxFebra.Text) * 2.39;
            calorias += Int32.Parse(textBoxFrango.Text) * 2.39;
            calorias += Int32.Parse(textBoxPorco.Text) * 2.42;
            calorias += Int32.Parse(textBoxlula.Text) * 0.92;
            calorias += Int32.Parse(textBoxsalmao.Text) * 1.71;
            calorias += Int32.Parse(textBoxpolvo.Text) * 0.82;
            calorias += Int32.Parse(textBoxsardinhas.Text) * 2.08;
            calorias += Int32.Parse(textBoxfiletes.Text) * 2.71;
            calorias += Int32.Parse(textBoxcarapau.Text) * 2.62;
            calorias += Int32.Parse(textBoxdourada.Text) * 1.85;
            calorias += Int32.Parse(textBoxmassa.Text) * 1.35;
            calorias += Int32.Parse(textBoxyogurte.Text) * 0.59;
            calorias += Int32.Parse(textBoxleite.Text) * 0.42;
            calorias += Int32.Parse(textBoxfrutossecos.Text) * 3.55;
            calorias += Int32.Parse(textBoxpao.Text) * 2.65;
            calorias += Int32.Parse(textBoxfrancesinha.Text) * 1.73;
            calorias += Int32.Parse(textBoxlaranja.Text) * 0.62;
            calorias += Int32.Parse(textBoxmaca.Text) * 0.52;
            calorias += Int32.Parse(textBoxpera.Text) * 0.57;
            calorias += Int32.Parse(textBoxbanana.Text) * 0.89;
            calorias += Int32.Parse(textBoxkiwi.Text) * 0.61;
            calorias += Int32.Parse(textBoxpessego.Text) * 0.39;
            calorias += Int32.Parse(textBoxmelao.Text) * 0.34;
            calorias += Int32.Parse(textBoxmelancia.Text) * 0.30;
            calorias += Int32.Parse(textBoxmanga.Text) * 0.60;
            calorias += Int32.Parse(textBoxfigo.Text) * 0.37;
            calorias += Int32.Parse(textBoxananas.Text) * 0.50;
            calorias += Int32.Parse(textBoxabacate.Text) * 1.60;
            calorias += Int32.Parse(textBoxpao.Text) * 1.75;
            labelcalorias.Text = calorias + " kcal";
            //labelCaloriasAlvo.Text = caloriasAlvo + "kcal";
        }

        private void labelTabelaCalorias_Click(object sender, EventArgs e)
        {
            Program.V_Tabela = new View_TabCal();
            Program.V_Tabela.Show();
        }

        private void labelVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_PlanoDeAlimentacao = new View_Calorias();
            Program.V_Pesquisar.Show();
        }
    }
}
