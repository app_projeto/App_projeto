﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;



namespace Aplicação_HabitosAlimentares
{
   
    public partial class View_Registar : Form
    { 
        public event MetodosComIntStrings Registo;
        public event MetodosComIntStrings alterarPerfil;
        OpenFileDialog dlg = new OpenFileDialog();
        private MailMessage Email;
        Stopwatch Stop = new Stopwatch();

        public View_Registar()
        {
            InitializeComponent();
            Program.M_App.RegistoSucedido += M_App_RegistoSucedido;
            Program.M_App.RegistoFalhado += M_App_RegistoFalhado;
            Program.M_App.RegistoMesmoEmail += M_App_RegistoMesmoEmail;
            Program.M_App.PerfilAlteradoComSucesso += M_App_PerfilAlteradoComSucesso;
            Program.M_App.PerfilAlteradoFalhado += M_App_PerfilAlteradoFalhado;
        }

        private void M_App_PerfilAlteradoFalhado()
        {
            MessageBox.Show("Perfil não foi alterado", "Perfil Não Alterado", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void M_App_PerfilAlteradoComSucesso()
        {
            MessageBox.Show("Perfil alterado com Sucesso", "Perfil Alterado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            Program.V_Menu.Show();
        }

        private void M_App_RegistoFalhado()
        {
            MessageBox.Show("Os campos não estão completos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void M_App_RegistoMesmoEmail()
        {
            MessageBox.Show("Já existe um utilizador com o email inserido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void M_App_RegistoSucedido()
        {
            MessageBox.Show("O seu registo foi efetuado com sucesso", "Registado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            Program.V_Login.Show();

        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_Home.Show();
            textBoxNome.Clear();
            textBoxEmail.Clear();
            textBoxIdade.Clear();
            textBoxPassword.Clear();
            textBoxTelefone.Clear();
            pictureBoxFoto.Image=null;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            int tele;
            try { tele = Int32.Parse(textBoxTelefone.Text); }
            catch (FormatException) { tele = 0; };
            if (textBoxConfEmail.Text != textBoxEmail.Text)
            {
                MessageBox.Show("Confirmação de Email errada.");
                Program.V_Registar.Show();
                return;
            }
            if (Program.V_Menu.escolhaPerfil == "Alterar")
            {
                pictureBoxFoto.Image = null;
                pictureBoxFoto.ImageLocation = null;
                if (alterarPerfil != null) alterarPerfil(textBoxNome.Text, textBoxPassword.Text, dlg.FileName, textBoxEmail.Text, Int32.Parse(textBoxIdade.Text), Int32.Parse(textBoxTelefone.Text));
            }
            else
            {
                if (Registo != null)
                {
                    try { Registo(textBoxNome.Text, textBoxPassword.Text, dlg.FileName, textBoxEmail.Text, Int32.Parse(textBoxIdade.Text), tele); }
                    catch (FormatException) {
                        M_App_RegistoFalhado();
                        Program.V_Registar.Show();
                           
                            };
                }
                else
                {
                    this.Hide();
                    Program.V_Login.Show();
                }
            }
           // MailMessage mailMsg = new MailMessage();
            Email = new MailMessage();
            Email.To.Add(new MailAddress(textBoxEmail.Text));
            Email.From = (new MailAddress("al66518@utad.eu"));
            //mailMsg.From = "ricardojcr1999@hotmail.com";
            Email.Subject = "Registo Na Aplicação Controlo de Habitos Infantis";
            Email.IsBodyHtml = true;
            Email.Body = "Obrigado por se registar na nossa aplicacao! "+ "\n" + "\n" + "Qualquer dúvida(s) ou erro(s) que encontre, pedimos que encaminhe um Email ao suporte." + "\n" + "Cumprimentos, A equipa do suporte. ";
            
            SmtpClient cliente = new SmtpClient("smtp.live.com", 587);
            //mtpClient.UseDefaultCredentials = false;
            using (cliente)
            {
                //SmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //SmtpClient.UseDefaultCredentials = false;
                cliente.Credentials = new System.Net.NetworkCredential("al66518@utad.eu", "88vv22un");
                cliente.EnableSsl = true;
                cliente.Send(Email);
            }


        }

        private void buttonAdicionarFoto_Click(object sender, EventArgs e)
        {

            dlg.Filter = "Imagens|*.jpg;*.png;*.bmp|Ficheiros JPG|*.jpg|Ficheiros PNG|*.png|Ficheiros BMP|*.bmp|Todos os Ficheiros|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pictureBoxFoto.Image = null;
                pictureBoxFoto.Image = Image.FromFile(dlg.FileName);
            }
        }

        private void checkBoxPassword_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxPassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = true;


            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = false;
            }
        }

        private void View_Registar_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBoxFoto_Click(object sender, EventArgs e)
        {
            dlg.Filter = "Imagens|*.jpg;*.png;*.bmp|Ficheiros JPG|*.jpg|Ficheiros PNG|*.png|Ficheiros BMP|*.bmp|Todos os Ficheiros|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pictureBoxFoto.Image = null;
                pictureBoxFoto.Image = Image.FromFile(dlg.FileName);
            }
        }

        private void textBoxNome_Click(object sender, EventArgs e)
        {
            if (textBoxNome.Text == "")
            {
                textBoxNome.Text = "Nome";
                return;
            }
            if (textBoxNome.Text == "Nome")
            {
                textBoxNome.Clear();
            }
        }

        private void textBoxNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxIdade_Click(object sender, EventArgs e)
        {
            if (textBoxIdade.Text == "")
            {
                textBoxIdade.Text = "Idade";
                return;
            }
            if (textBoxIdade.Text == "Idade")
            {
                textBoxIdade.Clear();
            }
           
        }

        private void textBoxTelefone_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxTelefone_Click(object sender, EventArgs e)
        {
            if (textBoxTelefone.Text == "")
            {
                textBoxTelefone.Text = "Telemovel*";
                return;
            }
            if (textBoxTelefone.Text == "Telemovel*")
            {
                textBoxTelefone.Clear();
            }
            
        }

        private void textBoxEmail_Click(object sender, EventArgs e)
        {
            if (textBoxEmail.Text == "")
            {
                textBoxEmail.Text = "Email";
                return;
            }
            if (textBoxEmail.Text == "Email")
            {
                textBoxEmail.Clear();
            }
           
        }

        private void textBoxConfEmail_Click(object sender, EventArgs e)
        {
            if (textBoxConfEmail.Text == "")
            {
                textBoxConfEmail.Text = "Confirmar Email";
                return;
            }
            if (textBoxConfEmail.Text == "Confirmar Email")
            {
                textBoxConfEmail.Clear();
            }
          
        }

        private void textBoxPassword_Click(object sender, EventArgs e)
        {
            if (textBoxPassword.Text == "")
            {
                textBoxPassword.UseSystemPasswordChar = true;
                textBoxPassword.Text = "Password";
                return;
            }
            if (textBoxPassword.Text == "Password")
            {
                textBoxPassword.Clear();
                textBoxPassword.UseSystemPasswordChar = false;
            }

        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
