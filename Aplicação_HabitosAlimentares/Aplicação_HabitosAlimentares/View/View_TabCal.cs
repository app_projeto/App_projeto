﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicação_HabitosAlimentares.View
{
    public partial class View_TabCal : Form
    {
        public View_TabCal()
        {
            InitializeComponent();
        }

        private void button_fechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Program.V_PlanoDeAlimentacao.Show();
        }
    }
}
