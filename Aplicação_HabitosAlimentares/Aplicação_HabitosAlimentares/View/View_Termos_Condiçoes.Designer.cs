﻿namespace Aplicação_HabitosAlimentares
{
    partial class View_Termos_Condiçoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View_Termos_Condiçoes));
            this.textBoxTermosCondiçoes = new System.Windows.Forms.TextBox();
            this.buttonVoltar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAceitar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxTermosCondiçoes
            // 
            this.textBoxTermosCondiçoes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTermosCondiçoes.Cursor = System.Windows.Forms.Cursors.Help;
            this.textBoxTermosCondiçoes.Location = new System.Drawing.Point(12, 43);
            this.textBoxTermosCondiçoes.Multiline = true;
            this.textBoxTermosCondiçoes.Name = "textBoxTermosCondiçoes";
            this.textBoxTermosCondiçoes.ReadOnly = true;
            this.textBoxTermosCondiçoes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxTermosCondiçoes.Size = new System.Drawing.Size(776, 352);
            this.textBoxTermosCondiçoes.TabIndex = 0;
            this.textBoxTermosCondiçoes.Text = resources.GetString("textBoxTermosCondiçoes.Text");
            // 
            // buttonVoltar
            // 
            this.buttonVoltar.Location = new System.Drawing.Point(12, 408);
            this.buttonVoltar.Name = "buttonVoltar";
            this.buttonVoltar.Size = new System.Drawing.Size(98, 30);
            this.buttonVoltar.TabIndex = 1;
            this.buttonVoltar.Text = "Voltar";
            this.buttonVoltar.UseVisualStyleBackColor = true;
            this.buttonVoltar.Click += new System.EventHandler(this.buttonVoltar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(274, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Termos e Condições";
            // 
            // buttonAceitar
            // 
            this.buttonAceitar.Location = new System.Drawing.Point(657, 408);
            this.buttonAceitar.Name = "buttonAceitar";
            this.buttonAceitar.Size = new System.Drawing.Size(131, 30);
            this.buttonAceitar.TabIndex = 3;
            this.buttonAceitar.Text = "Aceitar e Registar";
            this.buttonAceitar.UseVisualStyleBackColor = true;
            this.buttonAceitar.Click += new System.EventHandler(this.buttonAceitar_Click);
            // 
            // View_Termos_Condiçoes
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Gray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonAceitar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonVoltar);
            this.Controls.Add(this.textBoxTermosCondiçoes);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "View_Termos_Condiçoes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Termos e Condiçoes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonVoltar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAceitar;
        protected System.Windows.Forms.TextBox textBoxTermosCondiçoes;
    }
}